<?php

namespace API;

use Slim\Slim;

class Application extends Slim {

    public function validateToken($user = array(), $action = 'create') {
        $errors = array();
        if (empty($user['email'])) {
            $errors['user'][] = array(
                'field' => 'email',
                'message' => 'Email address cannot be empty'
            );
        } elseif (false === filter_var(
                        $user['email'], FILTER_VALIDATE_EMAIL
                )) {
            $errors['user'][] = array(
                'field' => 'email',
                'message' => 'Email address is invalid'
            );
        }
        if (empty($user['password'])) {
            $errors['user'][] = array(
                'field' => 'password',
                'message' => 'Password cannot be empty'
            );
        }
        if (empty($user['softwareID'])) {
            $errors['user'][] = array(
                'field' => 'softwareID',
                'message' => 'softwareID cannot be empty'
            );
        }
        return $errors;
    }

    public function validateContact($contact = array()) {
        $errors = array();
        if (!isset($contact['taglist']) || empty($contact['taglist'])) {
            $errors['contact'][] = array(
                'field' => 'taglist',
                'message' => 'taglist cannot be empty'
            );
        }
        return $errors;
    }
    public function validateMessage($message = array()) {
        $errors = array();
//        if (isset($message['bodyHTML']) && isset($message['bodyText'])) {
//            $errors['message'][] = array(
//                'field' => 'bodyHTML OR bodyText',
//                'message' => 'Contains both bodyHTML and bodyText.  Can only have one.'
//            );
//        }
//        if (empty($message['title'])) {
//            $errors['message'][] = array(
//                'field' => 'title',
//                'message' => 'Title is required.'
//            );
//        }
        return $errors;
    }
}
