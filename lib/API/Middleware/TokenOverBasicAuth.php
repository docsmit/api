<?php

/**
 * Token Over HTTP Basic Authentication
 *
 * Use this middleware with your Slim Framework application
 * to require a user name or API key via HTTP basic auth
 * for all routes. No need for password.
 *
 * NOTE: the verify() protected method requires an ORM object,
 * Idiorm is the default.
 *
 *
 * @author Vito Tardia <vito@tardia.me>
 * @version 1.0
 * @copyright 2014 Vito Tardia
 *
 * USAGE
 *
 * $app = new \Slim\Slim();
 * $app->add(new API\Middleware\TokenOverBasicAuth());
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace API\Middleware;

class TokenOverBasicAuth extends \Slim\Middleware {

    /**
     * @var array
     */
    protected $settings = array(
        'realm' => 'Protected Area',
        'root' => '/'
    );

    /**
     * Constructor
     *
     * @param   array  $config   Configuration and Login Details
     * @return  void
     */
    public function __construct(array $config = array()) {
        if (!isset($this->app)) {
            $this->app = \Slim\Slim::getInstance();
        }
        $this->config = array_merge($this->settings, $config);
    }

    /**
     * Call
     *
     * This method will check the HTTP request headers for
     * previous authentication. If the request has already authenticated,
     * the next middleware is called. Otherwise,
     * a 401 Authentication Required response is returned to the client.
     *
     * @return  void
     */
    public function call() {

        $req = $this->app->request();

DEBLOG ("Middleware.TOBA received call from " . $_SERVER['HTTP_X_FORWARDED_FOR'] . " URI = " . $req->getResourceUri() . " and token = " . $req->headers('PHP_AUTH_USER'));

        $res = $this->app->response();

        // URI can advance to next middleware without authentication if:
        // - inside /v1/testing, or
        // - the URI is in public.routes with that public.route's method.
        $isTestingURI = preg_match("/v1\/testing.*/", $req->getResourceUri()) ? 1 : 0;
        if ((isset($this->app->settings["public.routes"][$req->getResourceUri()]) &&
                $this->app->settings["public.routes"][$req->getResourceUri()] == $req->getMethod())
                || $isTestingURI)
        {
            DEBLOG("TOBA Bypassed for public route.");
            $this->next->call();
            return;
        }
        if (!preg_match('|^' . $this->config['root'] . '.*|', $req->getResourceUri())) {
            $errmsg = "Version not matching: " .  $this->config['root'] . " != " . $req->getResourceUri();
            DEBLOG ($errmsg);
            $res->body(json_encode(array("code"=>401, "message" => $errmsg), JSON_PRETTY_PRINT));
            return;
        }

        // The token is stored in the USER header
        $authToken = $req->headers('PHP_AUTH_USER');

        if (!$this->verify($authToken)) {
            DEBLOG("TOBA Failed to Verify token = $authToken");
            $res->status(401);
            $res->header('WWW-Authenticate', sprintf('Basic realm="%s"', $this->config['realm']));
            $res->header('Content-Type', "application/json; charset=utf-8");
            $res->body(json_encode(array("code"=>401,"message" => "Authentication failed on token."),JSON_PRETTY_PRINT));
            return;
        }
        DEBLOG("TOBA Accepted with token = $authToken");
        $this->next->call();
    }

    /**
    * Check passed auth token
    *
    * @param string $authToken
    * @return boolean
    **/
    protected function verify($token) {
        $APIToken = new \Docsmit\APIToken ($this->app->mysqli);
        if ($APIToken->validate($token)) {
            $this->app->user = new \Docsmit\User($this->app->mysqli,$APIToken->userID());
            $this->app->APIToken = $APIToken;
            $APIToken->resetExpiration();
            return true;
        }
        return false;
    }

}
