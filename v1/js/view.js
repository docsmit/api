$(function () {
    var path = window.location.pathname;
    var pathArr = path.split("/");
    var owner = pathArr[1];
    var urlName = pathArr[2];
    if (pathArr.length > 2)
        var curVersion = pathArr[3];

    var viewer = new Viewer(owner, urlName, curVersion);

    $.get("/api/context", {owner: owner, urlName: urlName}, function (context, statusStr, response) {
        var spec = context.specInfo;

        if (context.account) {
            $("#username").css("display", "block");
            $("#mySpecsButton").css("display", "block");
            $("#usernamePlaceholder").text(context.account.username);
            $("#avatar").attr("src", context.account.imageUrl);
            if (context.account.orgs && context.account.orgs.length > 0) {
                var menuItem = $("#account-menu-item");
                var items = [];
                items.push($("<li class=\"divider\"></li>"));
                items.push($("<li class=\"dropdown-header\">Organizations</li>"));

                context.account.orgs.forEach(function (o) {
                    items.push($("<li><a href=\"/orgs/" + o.name + "/settings\">" + o.name + "</a></li>"));
                });
                menuItem.after(items);
            }


            if (spec.accessLevel == 'WRITE' || spec.accessLevel == 'OWNER' || spec.accessLevel == 'ADMIN') {
                $(".main-dropdown-menu .edit-menu-item").css("display", "block");
                $(".main-dropdown-menu .edit-menu-item.e a").attr("href", path + "/_edit/methods");
                $(".main-dropdown-menu .edit-menu-item.h a").attr("href", path + "/_edit/history");
                $(".main-dropdown-menu .edit-menu-item.s a").attr("href", '/' + owner + "/" + urlName + "/_settings/");
                var enableEdit = true;
            }

        } else {
            $("#loginButtons").css("display", "block");
            $("#username").css("display", "none");
        }


        var consoleSettings = spec.settings.consoleSettings;
        if (consoleSettings.enabled) {
            if (
                consoleSettings.accessibility == 'all'
                ||
                (consoleSettings.accessibility == 'readers' && context.account )
                ||
                (consoleSettings.accessibility == 'writers' && (context.account && (spec.accessLevel == 'WRITE' || spec.accessLevel == 'OWNER' || spec.accessLevel == 'ADMIN')))
            ) {
                viewer.initAngular();


            }
        }

        viewer.enableButtons(consoleSettings.enabled, enableEdit, path);

        var versions = spec.versions || [];
        if (versions.length > 1) {
            if (!curVersion)
                curVersion = spec.defaultVersion;

            versions.forEach(function (v) {
                if (v.default)
                    v.url = '/' + owner + '/' + urlName;
                else
                    v.url = '/' + owner + '/' + urlName + '/' + v.name;

            });

            var versionsMenu = $('<span class="pull-right versions"><div class="dropdown" dropdown><a class="dropdown-toggle" title="Select version" data-toggle="dropdown" href=""></a><ul class="dropdown-menu versions-menu dropdown-menu-right"></ul></div></span>');
            $(".speca-brand .under-title").append(versionsMenu);
            versionsMenu.find("a").html(curVersion + ' <i class="caret"></i>');
            var versionsDropdown = versionsMenu.find('ul.dropdown-menu');
            versions.forEach(function (v) {
                var str = '<li><a target="_self" href="' + v.url + '">';

                if (v.name == curVersion)
                    str += '<span class="current">';
                else
                    str += '<span>';

                str += v.name;
                str += '</span>';

                if (v.default)
                    str += '<span class="default">default</span>';
                if (!v.published)
                    str += '<span class="not-published">not published</span>';

                str += '</a></li>';
                versionsDropdown.append($(str));
            });
        }


    });

    $("#logoutBtn").click(function () {
        $.post("/api/logout", function () {
            $("#username").css("display", "none");
            $("#usernamePlaceholder").text('');
            $("#loginButtons").css("display", "block");
        });
    });

});

var Viewer = function (owner, urlName, version) {
    this.owner = owner;
    this.urlName = urlName;
    this.version = version;
    this.STORAGE_KEY = "spec_menustate_" + owner + "_" + urlName;
    this.Viewer(owner, urlName);
};

Viewer.prototype.Viewer = function (owner, urlName) {
    var w = $(window);
    var d = $(document);

    var menu = $("#sidebar-wrapper");
    var menuItems = menu.find("a");
    var menuState = localStorage.getObject(this.STORAGE_KEY) || [];

    var currentScroll = d.scrollTop();
    if (currentScroll > 80) {
        if (!menu.hasClass("fixed"))menu.addClass('fixed');
    } else if (menu.hasClass("fixed"))menu.removeClass('fixed');

//    this.initCopyToClipboardButtons();
    this.addGroupActions(menuState);
    // TODO this.updateLastUpdatedFormat();

    var scrollItems = menuItems.map(function (i) {
        var href = this.getAttribute("href");
        var a = $(this);
        if (menuState.indexOf(href) != -1) {
            var div = a.parent();
            var ul = div.next('ul');
            var icon = a.children('i');
            icon.removeClass('fa-chevron-right').addClass('fa-chevron-down');
            ul.addClass('expanded').removeClass('collapsed');
            div.addClass('_active');
        }

        if (href) {
            href = href.replace(/'/g, "\\'").replace("#", "");
            try {
                var item = $("[id='" + href + "']");
                if (item.length) {
                    return item;
                }
            } catch (e) {
                console.error("invalid expression: " + href);
            }
        } else {
            console.log('!!!', a, href);
        }

    });

    this.initScrollHandler(menu, menuItems, scrollItems);
    var w = $(window);
    var h = this._handle;
    menuItems.click(function () {
        w.unbind('scroll', h);
        menu.find('.active').removeClass('active');
        $(this).parent().addClass("active");


        setTimeout(function () {
            var currentScroll = d.scrollTop();
            if (currentScroll > 80) {
                if (!menu.hasClass("fixed"))menu.addClass('fixed');
            } else if (menu.hasClass("fixed"))menu.removeClass('fixed');
        }, 10);

        setTimeout(function () {
            w.scroll(h);
        }, 500);
    });

//    setTimeout(function () {

//    }.bind(this), 1500);
    //   setTimeout(function(){
    if (window.location.hash) {
        this.navigateToItemInHash();
    }

    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("active");
    });


};

Viewer.prototype.destroy = function () {
    this.stop = true;
    $(window).unbind('scroll', this._handle);
};


Viewer.prototype.addGroupActions = function (menuState) {

    $('.doc-item').click(function () {
        var a = $(this);
        var ul = a.next('ul');
        ul.toggleClass('collapsed');
    });

    var key = this.STORAGE_KEY;

    $('.group>a').click(function () {
        var a = $(this);
        var div = a.parent();
        var ul = div.next('ul');
        var icon = a.children('i');
        if (icon.hasClass('fa-chevron-right')) {
            icon.removeClass('fa-chevron-right').addClass('fa-chevron-down');
            ul.addClass('expanded').removeClass('collapsed');
            menuState.push(a.attr("href"));
            localStorage.setObject(key, menuState)
        } else {
            icon.removeClass('fa-chevron-down').addClass('fa-chevron-right');
            ul.removeClass('expanded').addClass('collapsed');
            menuState.splice(menuState.indexOf(a.attr("href")), 1);
            localStorage.setObject(key, menuState)
        }

    });

    $('.sidebar-h>a').click(function () {
        var a = $(this);
        var div = a.parent();
        var ul = div.next('ul');
        var icon = a.children('i');
        if (icon.hasClass('fa-chevron-right')) {
            icon.removeClass('fa-chevron-right').addClass('fa-chevron-down');
            ul.addClass('expanded').removeClass('collapsed');
            div.addClass('_active');
            menuState.push(a.attr("href"));
            localStorage.setObject(key, menuState)
        } else {
            icon.removeClass('fa-chevron-down').addClass('fa-chevron-right');
            ul.removeClass('expanded').addClass('collapsed');
            div.removeClass('_active');
            menuState.splice(menuState.indexOf(a.attr("href")), 1);
            localStorage.setObject(key, menuState)
        }
    });
};

Viewer.prototype.navigateToItemInHash = function () {
    var targetHash = decodeURI(window.location.hash).substr(1);
    targetHash = encodeURIComponent(targetHash);
    //targetHash = targetHash.replace("\'","\\'");
    //console.log(targetHash);
    var activeMenuItem = $('#sidebar a[href="' + targetHash + '"]');
    var expand = function (div) {
        var ul = div.next('ul');
        var icon = div.children('i');
        icon.removeClass('fa-chevron-right').addClass('fa-chevron-down');
        ul.addClass('expanded').removeClass('collapsed');
        div.addClass('_active');
    };

    if (activeMenuItem.length) {
        if (activeMenuItem.hasClass('method-item')) {
            expand($('#methodsHeader'));
            var g = activeMenuItem.parent().parent().parent();
            var groupHeader = g.children('.group');
            if (groupHeader) {
                groupHeader.find('i').removeClass('fa-chevron-right').addClass('fa-chevron-down');
                g.children('ul').addClass('expanded').removeClass('collapsed');
            }
        } else if (activeMenuItem.hasClass('group-item')) {
            expand($('#methodsHeader'));
            var g = activeMenuItem.parent().parent();
            var groupHeader = g.children('.group');
            groupHeader.find('i').removeClass('fa-chevron-right').addClass('fa-chevron-down');
            g.children('ul').addClass('expanded').removeClass('collapsed');
        } else if (activeMenuItem.hasClass('model-item')) {
            expand($('#modelsHeader'));
        } else if (activeMenuItem.hasClass('doc-item')) {
            expand($('#docsHeader'));
        } else if (activeMenuItem.hasClass('common-item')) {
            expand($('#commonHeader'));
        } else if (activeMenuItem.hasClass('doc-item-sub')) {
            var ul = activeMenuItem.parent().parent();
            ul.removeClass('collapsed');
        }


        var el = $("[id='" + targetHash + "']");
        if (el && el.length > 0) {
            var p = el.position();
            if (p) {
                window.scrollTo(0, el.position().top + 40);
//                window.scrollTo(0, el.position().top + 41);
                // $('#sidebar-wrapper').scrollTo(activeMenuItem);
            }
        }
    }

};


Viewer.prototype.highlightCodeBlocks = function () {
    $('pre code').each(function (i, e) {
        hljs.highlightBlock(e)
    });
};

Viewer.prototype.updateLastUpdatedFormat = function () {
    var lastUpdated = $("#lastUpdated");
    lastUpdated.text(moment(lastUpdated.text(), "MMM DD, YYYY HH:mm:SS A").fromNow());
    lastUpdated.show();

};

Viewer.prototype.initWindowResizeHandler = function (menu) {
//    var parentHeight = $("#sidebar").height();
//    menu.height(parentHeight);
//    $(window).resize(function () {
//        var parentHeight = $("#sidebar").height();
//        menu.height(parentHeight);
//
//    });
};

Viewer.prototype.initCopyToClipboardButtons = function () {
    var copyPostmanUrlBtn = $('#copyPostmanUrlBtn');
    var clip = new ZeroClipboard(copyPostmanUrlBtn);
    clip.on('complete', function (client, args) {
        var orig = copyPostmanUrlBtn.html();
        copyPostmanUrlBtn.html("<span class='text-success'><i class='fa fa-check'></i> Copied</span>");
        setTimeout(function (e) {
            copyPostmanUrlBtn.html(orig);
        }, 1000)
    });
};

Viewer.prototype.tryIt = function (methodId) {
    var injector = angular.element('body').injector();
    var $scope = injector.get("$rootScope");
    $scope.$broadcast("tryit", this.owner, this.urlName, this.version, methodId);
};

Viewer.prototype.enableButtons = function (enableTryIt, enableEdit, path) {
    if (!enableEdit && !enableTryIt)
        return;
    var s = this;
    $('.tryIt').css("display", "").on("click", function () {
        var methodId = this.getAttribute("data-edit-try-method-id");
        s.tryIt(methodId);
    });

    $('div[data-entity]').each(function () {
        var type = $(this).attr("data-entity");
        var name = $(this).attr("data-entity-name");
        var group = $(this).attr("data-entity-group");
        var buttons = $('<div class="pull-right"></div>');
        if (enableTryIt && type == "method") {
            var tryItBtn = $('<button class="btn btn-default btn-sm"><i class="fa fa-caret-square-o-right"></i> Try it</button>');
            tryItBtn.on("click", function () {
                s.tryIt(name);
            });
            buttons.append(tryItBtn);
        }
        if (enableEdit) {
            var editButton = $('<button class="btn btn-default btn-sm"><i class="fa fa-edit"></i> Edit</button>');
            editButton.on("click", function () {
                var url;
                switch(type){
                    case "method": url = "/m/" + group + "/" + name; break;
                    case "group": url = "/m/" + name; break;
                    case "model": url = "/models/" + name; break;
                    case "enum": url = "/enums/" + name; break;
                    case "doc": url = "/docs/" + name; break;
                    case "params": url = "/params"; break;
                    case "headers": url = "/headers"; break;
                    case "responses": url = "/responses"; break;
                }
                window.location = path + "/_edit" + url;
            });

            buttons.append(editButton);
        }
        $(this).append(buttons);
    });

};


Viewer.prototype.initScrollHandler = function (menu, menuItems, scrollItems) {
    var reduced = false;
    // Cache selectors
    var lastId;
    var lastGroup;
    var topMenuHeight = 30;
    // All list items
    // Anchors corresponding to menu items

    var w = $(window);
    var d = $(document);

    var navbar = $(".navbar-speca");
    var spacer = $(".navbar-speca-spacer");

    var latestKnownScrollY = 0;
    this._handle = function () {
        //  if(window.scrollY > 600 && (Math.abs(window.scrollY - latestKnownScrollY) < 200))return;
        var currentScroll = d.scrollTop();
        // Height visible
        var visibleHeight = document.documentElement.clientHeight;
        // Height of page
        var totalHeight = d.height();

        //if (currentScroll > 65 && totalHeight > visibleHeight + 100) {
        //}else{
        //}
        if (currentScroll >= 65 && totalHeight > visibleHeight + 100) {
            menu.addClass('fixed');
            navbar.addClass('fixed');
            spacer.addClass('fixed');
            reduced = true;
            var out = true;
            navbar.hover(
                function () {
                    out = false;
                    setTimeout(function () {
                        if (!out)
                            navbar.addClass('slide_down');
                    }, 100)
                },
                function () {
                    out = true;
                    setTimeout(function () {
                        if (out)
                            navbar.removeClass('slide_down');
                    }, 1000)
                }
            );
        } else if (reduced) {
            menu.removeClass('fixed');
            navbar.removeClass('fixed');
            spacer.removeClass('fixed');
            menu.hover(
                function () {

                },
                function () {

                }
            );
            reduced = false;
        }


        if (window.scrollY < 500)return;
        if (window.scrollY > 600 && (Math.abs(window.scrollY - latestKnownScrollY) < 200))return;
        latestKnownScrollY = window.scrollY;
        // Get container scroll position
        var fromTop = currentScroll + topMenuHeight;

        // Get id of current scroll item

        var cur = scrollItems.map(function () {
            if ($(this).offset().top < fromTop + 30)
                return this;
        });


        // Get the id of the current element
        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";

        if (lastId !== id) {
            lastId = id;
            // Set/remove active class
            var newActive = menuItems
                .parent().removeClass("active")
                .end().filter('[href="#' + id + '"]').parent();
            newActive.addClass("active");

            var activeMenuItem = newActive.children('a');
            var expand = function (div) {
                var ul = div.next('ul');
                var icon = div.find('i');
                icon.removeClass('fa-chevron-right').addClass('fa-chevron-down');
                ul.addClass('expanded').removeClass('collapsed');
                div.addClass('_active');

            };
            if (activeMenuItem) {
                if (activeMenuItem.hasClass('method-item')) {
                    expand($('#methodsHeader'));
                    var g = activeMenuItem.parent().parent().parent();
                    var groupHeader = g.children('.group');
                    if (groupHeader) {
                        groupHeader.children('i').removeClass('fa-chevron-right').addClass('fa-chevron-down');
                        g.children('ul').addClass('expanded').removeClass('collapsed');
                    }
                } else if (activeMenuItem.hasClass('group-item')) {
                    expand($('#methodsHeader'));
                    var g = activeMenuItem.parent().parent();
                    var groupHeader = g.children('.group');
                    groupHeader.children('i').removeClass('fa-chevron-right').addClass('fa-chevron-down');
                    g.children('ul').addClass('expanded').removeClass('collapsed');
                } else if (activeMenuItem.hasClass('model-item')) {
                    expand($('#modelsHeader'));
                } else if (activeMenuItem.hasClass('doc-item')) {
                    expand($('#docsHeader'));
                } else if (activeMenuItem.hasClass('common-item')) {
                    expand($('#commonHeader'));
                } else if (activeMenuItem.hasClass('doc-item-sub')) {
                    var ul = activeMenuItem.parent().parent();
                    ul.removeClass('collapsed');
                }
            }


        }
    };
    var scrollIntervalID = setInterval(this._handle, 10);

//    var r = function () {
//        w.unbind('scroll');
//        if (!this.stop) {
//            _handle();
//            var t = setTimeout(function () {
//                w.scroll(r);
//            }, 40);
//        }
//    }.bind(this);

//    r();

};


Viewer.prototype.initAngular = function () {
    var d1 = $.getScript("https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js");
    var d2 = $.getScript("/view/js/tryit-ctrl.js");
    var d3 = $.getScript("/lib/jquery-jsonview.js");

    $.when(d1, d2, d3).done(function () {
        $.getScript("/lib/ui-bootstrap-custom-tpls-0.13.0.js", function () {

            angular.module('TryIt', [
                'ui.bootstrap',
                'ui.bootstrap.modal'
            ])
                .config(
                ["$locationProvider", "$httpProvider", "$controllerProvider",
                    function ($locationProvider, $httpProvider, $controllerProvider) {
                        $locationProvider.html5Mode({
                            enabled: true,
                            requireBase: false
                        });
                        $controllerProvider.allowGlobals();
                        $httpProvider.defaults.timeout = 3000;
                    }])


            ;
            angular.bootstrap(document.body, ["TryIt"]);
            var injector = angular.element('body').injector();
            var $rootScope = injector.get("$rootScope");
            var $modal = injector.get("$modal");
            var $http = injector.get("$http");

            $rootScope.$on("tryit", function (e, owner, urlName, version, methodId) {
                var modal = $modal.open({
                    backdrop: 'static',
                    keyboard: true,
                    animation: false,
                    templateUrl: '/view/tpl/tryIt-dialog.tpl.html',
                    controller: TryItCtrl,
                    windowClass: 'tryit-modal-window',
                    resolve: {
                        AppModel: function () {
                            return {
                                currentSpec: {
                                    owner: {
                                        username: owner
                                    },
                                    urlName: urlName
                                }
                            }
                        },
                        tryItData: function () {
                            return $http({
                                method: 'GET',
                                headers: {"Speca-Version": version},
                                url: '/api/specs/' + owner + '/' + urlName + '/methods/' + methodId + '/tryitdata'
                            }).then(function (response) {
                                return response.data
                            });
                        }
                    }
                });
                modal.opened.then(function () {
                    $rootScope.$broadcast("modal_opened");
                });
                return modal.result.then(function (model) {
                    $rootScope.$broadcast("modal_closed");
                    return model;
                }, function (reason) {
                    $rootScope.$broadcast("modal_closed");
                });
            });
        });
    });

    // todo init angular
    // 1. load angular.min
    // 2. load angular + modal
    // 3. load try-it ctrl
    // 4. load try-it tmpl
    // 5. load methods try it data
};

Storage.prototype.setObject = function (key, value) {
    this.setItem(key, JSON.stringify(value));
};

Storage.prototype.getObject = function (key) {
    try {
        var value = this.getItem(key);
        return value && JSON.parse(value);
    } catch (e) {
        return null;
    }
};


//** jQuery Scroll to Top Control script- (c) Dynamic Drive DHTML code library: http://www.dynamicdrive.com.
//** Available/ usage terms at http://www.dynamicdrive.com (March 30th, 09')
//** v1.1 (April 7th, 09'):
//** 1) Adds ability to scroll to an absolute position (from top of page) or specific element on the page instead.
//** 2) Fixes scroll animation not working in Opera.


var scrolltotop = {
    //startline: Integer. Number of pixels from top of doc scrollbar is scrolled before showing control
    //scrollto: Keyword (Integer, or "Scroll_to_Element_ID"). How far to scroll document up when control is clicked on (0=top).
    setting: {startline: 100, scrollto: 0, scrollduration: 500, fadeduration: [500, 100]},
    controlHTML: '<div class="back-top"><i class="fa fa-angle-up"></i></div>', //HTML for control, which is auto wrapped in DIV w/ ID="topcontrol"
    controlattrs: {offsetx: 5, offsety: 5}, //offset of control relative to right/ bottom of window corner
    anchorkeyword: '#top', //Enter href value of HTML anchors on the page that should also act as "Scroll Up" links

    state: {isvisible: false, shouldvisible: false},

    scrollup: function () {
        if (!this.cssfixedsupport) //if control is positioned using JavaScript
            this.$control.css({opacity: 0}) //hide control immediately after clicking it
        var dest = isNaN(this.setting.scrollto) ? this.setting.scrollto : parseInt(this.setting.scrollto)
        if (typeof dest == "string" && jQuery('#' + dest).length == 1) //check element set by string exists
            dest = jQuery('#' + dest).offset().top
        else
            dest = 0
        this.$body.animate({scrollTop: dest}, this.setting.scrollduration);
    },

    keepfixed: function () {
        var $window = jQuery(window)
        var controlx = $window.scrollLeft() + $window.width() - this.$control.width() - this.controlattrs.offsetx
        var controly = $window.scrollTop() + $window.height() - this.$control.height() - this.controlattrs.offsety
        this.$control.css({left: controlx + 'px', top: controly + 'px'})
    },

    togglecontrol: function () {
        var scrolltop = jQuery(window).scrollTop()
        if (!this.cssfixedsupport)
            this.keepfixed()
        this.state.shouldvisible = (scrolltop >= this.setting.startline) ? true : false
        if (this.state.shouldvisible && !this.state.isvisible) {
            this.$control.stop().animate({opacity: 1}, this.setting.fadeduration[0])
            this.state.isvisible = true
        }
        else if (this.state.shouldvisible == false && this.state.isvisible) {
            this.$control.stop().animate({opacity: 0}, this.setting.fadeduration[1])
            this.state.isvisible = false
        }
    },

    init: function () {
        jQuery(document).ready(function ($) {
            var mainobj = scrolltotop
            var iebrws = document.all
            mainobj.cssfixedsupport = !iebrws || iebrws && document.compatMode == "CSS1Compat" && window.XMLHttpRequest //not IE or IE7+ browsers in standards mode
            mainobj.$body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body')
            mainobj.$control = $('<div id="topcontrol">' + mainobj.controlHTML + '</div>')
                .css({
                    position: mainobj.cssfixedsupport ? 'fixed' : 'absolute',
                    bottom: mainobj.controlattrs.offsety,
                    right: mainobj.controlattrs.offsetx,
                    opacity: 0,
                    cursor: 'pointer'
                })
                .attr({title: 'Scroll Back to Top'})
                .click(function () {
                    mainobj.scrollup();
                    return false
                })
                .appendTo('body')
            if (document.all && !window.XMLHttpRequest && mainobj.$control.text() != '') //loose check for IE6 and below, plus whether control contains any text
                mainobj.$control.css({width: mainobj.$control.width()}) //IE6- seems to require an explicit width on a DIV containing text
            mainobj.togglecontrol()
            $('a[href="' + mainobj.anchorkeyword + '"]').click(function () {
                mainobj.scrollup()
                return false
            });
            $(window).bind('scroll resize', function (e) {
                mainobj.togglecontrol()
            })
        })
    }
};

scrolltotop.init()
