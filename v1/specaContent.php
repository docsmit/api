<div class="sidebar">
			<div class="sidebar-header">
				<div class="sidebar-header__brand">
					<span class="title">Docsmit.com</span>
</div>
				<div class="sidebar-header__brand-secondary">
					<div class="sidebar-header__by">by 
						<a href="/mkasson" title="mkasson">mkasson</a>
</div>
</div>
</div>
			<ul class="sidebar-items sidebar-items__group">
<li item-type="group">
					<a href="#overview" title="Overview">
						<span class="group-name">Overview</span>
						<i class="fa fa-fw fa-angle-right" style="float: right"></i></a>
					<ul class="sidebar-items__group">
<li item-type="doc">
							<a href="#getting-started" title="Getting Started" style="display:flex">
								<span class="group-name">Getting Started</span>
								<span class="item-type-icon" data-item-type="doc"></span></a>
</li>
						<li item-type="doc">
							<a href="#testing-demo" title="Testing/Demo" style="display:flex">
								<span class="group-name">Testing/Demo</span>
								<span class="item-type-icon" data-item-type="doc"></span></a>
</li>
						<li item-type="doc">
							<a href="#how-to-use-it" title="How To Use It" style="display:flex">
								<span class="group-name">How To Use It</span>
								<span class="item-type-icon" data-item-type="doc"></span></a>
</li>
</ul>
</li>
				<li item-type="group">
					<a href="#docs_group" title="API Details">
						<span class="group-name">API Details</span>
						<i class="fa fa-fw fa-angle-right" style="float: right"></i></a>
					<ul class="sidebar-items__group">
<li item-type="doc">
							<a href="#authentication-and-access-tokens" title="Auth: Tokens, Passwords and SoftwareIDs" style="display:flex">
								<span class="group-name">Auth: Tokens, Passwords and SoftwareIDs</span>
								<span class="item-type-icon" data-item-type="doc"></span></a>
</li>
						<li item-type="doc">
							<a href="#messageid-userid-and-partyid" title="MessageID, UserID, and PartyID" style="display:flex">
								<span class="group-name">MessageID, UserID, and PartyID</span>
								<span class="item-type-icon" data-item-type="doc"></span></a>
</li>
						<li item-type="doc">
							<a href="#client-message-id" title="ClientMessageID" style="display:flex">
								<span class="group-name">ClientMessageID</span>
								<span class="item-type-icon" data-item-type="doc"></span></a>
</li>
						<li item-type="doc">
							<a href="#physical-mail-print-and-mail-parameters" title="Physical Mail (Print and Mail) Parameters" style="display:flex">
								<span class="group-name">Physical Mail (Print and Mail) Parameters</span>
								<span class="item-type-icon" data-item-type="doc"></span></a>
</li>
						<li item-type="doc">
							<a href="#callback-notices-vs-email-notices" title="Callback notices (vs email notices)" style="display:flex">
								<span class="group-name">Callback notices (vs email notices)</span>
								<span class="item-type-icon" data-item-type="doc"></span></a>
</li>
						<li item-type="doc">
							<a href="#misc-notes" title="Misc Notes" style="display:flex">
								<span class="group-name">Misc Notes</span>
								<span class="item-type-icon" data-item-type="doc"></span></a>
</li>
						<li item-type="doc-with-subheaders">
							<a href="#php-sdk" title="PHP SDK" style="display:flex">
								<span class="group-name">PHP SDK</span>
								<span class="item-type-icon" data-item-type="doc"></span></a>
							<ul class="sidebar-items__group">
<li>
									<a title="HTTP calls" class="doc-item-sub" href="#php-sdk__HTTP%20calls">HTTP calls</a>
</li>
								<li>
									<a title="Uploading Files" class="doc-item-sub" href="#php-sdk__Uploading%20Files">Uploading Files</a>
</li>
</ul>
</li>
						<li item-type="doc">
							<a href="#ex-1-authentication-check-sent-messages-read-a-message" title="Ex 1 - Authentication, check sent messages, read a message" style="display:flex">
								<span class="group-name">Ex 1 - Authentication, check sent messages, read a message</span>
								<span class="item-type-icon" data-item-type="doc"></span></a>
</li>
						<li item-type="doc">
							<a href="#ex-2-send-mail-merge-with-callback-notice" title="Ex 2 - Send Mail Merge with callback notice" style="display:flex">
								<span class="group-name">Ex 2 - Send Mail Merge with callback notice</span>
								<span class="item-type-icon" data-item-type="doc"></span></a>
</li>
						<li item-type="doc">
							<a href="#ex-3-blast-messages-clientmsgids-and-message-status-check" title="Ex 3 - Blast messages, clientMsgIDs and message status check" style="display:flex">
								<span class="group-name">Ex 3 - Blast messages, clientMsgIDs and message status check</span>
								<span class="item-type-icon" data-item-type="doc"></span></a>
</li>
</ul>
</li>
				<li item-type="group">
					<a href="#methods_group" title="API Methods">
						<span class="group-name">API Methods</span>
						<i class="fa fa-fw fa-angle-right" style="float: right"></i></a>
					<ul class="sidebar-items__group">
<li item-type="group">
							<a href="#authentication-tokens_group" title="Authentication Tokens">
								<span class="group-name">Authentication Tokens</span>
								<i class="fa fa-fw fa-angle-right" style="float: right"></i></a>
							<ul class="sidebar-items__group">
<li>
									<a href="#get-token" title="Get Token" style="display:flex">
										<span class="group-name">Get Token</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="POST"></span></a>
</li>
								<li>
									<a href="#delete-token" title="Delete Token" style="display:flex">
										<span class="group-name">Delete Token</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="DELETE"></span></a>
</li>
</ul>
</li>
						<li item-type="group">
							<a href="#messages-before-sending_group" title="Messages - Before Sending">
								<span class="group-name">Messages - Before Sending</span>
								<i class="fa fa-fw fa-angle-right" style="float: right"></i></a>
							<ul class="sidebar-items__group">
<li>
									<a href="#new-message" title="New Message" style="display:flex">
										<span class="group-name">New Message</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="POST"></span></a>
</li>
								<li>
									<a href="#upload-a-file" title="Upload a file" style="display:flex">
										<span class="group-name">Upload a file</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="POST"></span></a>
</li>
								<li>
									<a href="#send-message" title="Send Message" style="display:flex">
										<span class="group-name">Send Message</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="POST"></span></a>
</li>
								<li>
									<a href="#add-party" title="Add Party" style="display:flex">
										<span class="group-name">Add Party</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="POST"></span></a>
</li>
								<li>
									<a href="#delete-party" title="Delete Party" style="display:flex">
										<span class="group-name">Delete Party</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="DELETE"></span></a>
</li>
								<li>
									<a href="#update-party" title="Update Party" style="display:flex">
										<span class="group-name">Update Party</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="PUT"></span></a>
</li>
								<li>
									<a href="#get-the-price-and-the-details" title="Get the price and the details" style="display:flex">
										<span class="group-name">Get the price and the details</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="GET"></span></a>
</li>
</ul>
</li>
						<li item-type="group">
							<a href="#messages-after-sending_group" title="Messages - After Sending">
								<span class="group-name">Messages - After Sending</span>
								<i class="fa fa-fw fa-angle-right" style="float: right"></i></a>
							<ul class="sidebar-items__group">
<li>
									<a href="#get-a-message" title="Get a message" style="display:flex">
										<span class="group-name">Get a message</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="GET"></span></a>
</li>
								<li>
									<a href="#sign-for-message" title="Sign for message" style="display:flex">
										<span class="group-name">Sign for message</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="PUT"></span></a>
</li>
								<li>
									<a href="#get-certification" title="Get Certification" style="display:flex">
										<span class="group-name">Get Certification</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="GET"></span></a>
</li>
								<li>
									<a href="#get-recipients-and-their-statuses-parties" title="Get Recipients and their statuses - parties" style="display:flex">
										<span class="group-name">Get Recipients and their statuses - parties</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="GET"></span></a>
</li>
								<li>
									<a href="#get-attachments-list" title="Get attachments list" style="display:flex">
										<span class="group-name">Get attachments list</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="GET"></span></a>
</li>
								<li>
									<a href="#message-transaction-history" title="Message transaction history" style="display:flex">
										<span class="group-name">Message transaction history</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="GET"></span></a>
</li>
								<li>
									<a href="#download-file-attachment" title="Download file attachment" style="display:flex">
										<span class="group-name">Download file attachment</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="GET"></span></a>
</li>
								<li>
									<a href="#download-zip-attachment" title="Download Zip attachment" style="display:flex">
										<span class="group-name">Download Zip attachment</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="GET"></span></a>
</li>
								<li>
									<a href="#generate-tracking-id" title="Generate tracking ID (DONT USE RIGHT NOW)" style="display:flex">
										<span class="group-name">Generate tracking ID (DONT USE RIGHT NOW)</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="POST"></span></a>
</li>
</ul>
</li>
						<li item-type="group">
							<a href="#account_group" title="Account">
								<span class="group-name">Account</span>
								<i class="fa fa-fw fa-angle-right" style="float: right"></i></a>
							<ul class="sidebar-items__group">
<li>
									<a href="#sent-list" title="Sent list" style="display:flex">
										<span class="group-name">Sent list</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="GET"></span></a>
</li>
								<li>
									<a href="#inbox-list" title="Inbox list" style="display:flex">
										<span class="group-name">Inbox list</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="GET"></span></a>
</li>
								<li>
									<a href="#draft-list" title="Draft list" style="display:flex">
										<span class="group-name">Draft list</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="GET"></span></a>
</li>
								<li>
									<a href="#user-account-information" title="User Account Information" style="display:flex">
										<span class="group-name">User Account Information</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="GET"></span></a>
</li>
</ul>
</li>
						<li item-type="group">
							<a href="#misc_group" title="Misc">
								<span class="group-name">Misc</span>
								<i class="fa fa-fw fa-angle-right" style="float: right"></i></a>
							<ul class="sidebar-items__group"><li>
									<a href="#cass-check" title="CASS check" style="display:flex">
										<span class="group-name">CASS check</span>
										<span class="item-type-icon" data-item-type="method" data-http-method="GET"></span></a>
</li></ul>
</li>
</ul>
</li>
				<li item-type="group">
					<a href="#responses_group" title="Data Types and Responses">
						<span class="group-name">Data Types and Responses</span>
						<i class="fa fa-fw fa-angle-right" style="float: right"></i></a>
					<ul class="sidebar-items__group">
<li>
							<a href="#physicalParties" title="physicalParties" style="display:flex">
								<span class="group-name">physicalParties</span>
								<span class="item-type-icon" data-item-type="schema"></span></a>
</li>
						<li>
							<a href="#MailAddress" title="MailAddress" style="display:flex">
								<span class="group-name">MailAddress</span>
								<span class="item-type-icon" data-item-type="schema"></span></a>
</li>
</ul>
</li>
				<li style="height:100px">
</ul>
</div><div class="main">
			<div class="page-content">
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="top" class="spec-header">
								<h1>Docsmit.com</h1>
								<div class="description" style="padding: 10px 0">Send PDFs as USPS mail (Certified, Priority and First Class) as easy as email.</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<h3 id="sp_spec_pathvars">Base URI</h3>
							<div class="httpUrl">http://secure.docsmit.com/api/v1</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="overview" class="spec-item-header" data-entity="group" data-entity-id="overview">
								<a class="head_anchor" href="#overview"></a>Overview</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="getting-started" class="spec-item-header" data-entity="doc" data-entity-id="getting-started">
								<a class="head_anchor" href="#getting-started"></a>Getting Started</div>
							<div class="description doc-content">
<p>The Docsmit REST API lets you send USPS mail using a variety of service levels (e.g. First Class, Certified or Priority) as well as getting data, including Docsmit's Certification, about that mail.  The API accepts JSON in requests and returns it in responses.  The documents to be mailed are received in PDF.  They are generally expected to be US Letter size (8.5x11).</p>
<p>We have SDKs for PHP and C#.  The SDKs provide a layer of abstraction that handles authentication, encoding/decoding, token refreshing and error catching.  SDKs for other languages are easily composed. If you have a particular need, please let us know.  You can download the PHP SDK at <a href="https://secure.tracksmit.com/downloads/DocsmitAPI.php.txt" target="_self">https://secure.tracksmit.com/downloads/DocsmitAPI.php.txt</a> and demo code that shows how to send mail is at <a href="https://secure.tracksmit.com/downloads/demo.php.txt" target="_self">https://secure.tracksmit.com/downloads/demo.php.txt</a>.</p>
<p><strong>Requirements</strong></p>
<p>Before you can access the Docsmit API you must:</p>
<ul>
<li>Have a Docsmit account.   (Sign Up at www.docsmit.com)</li>
<li>Obtain a SoftwareID from api@docsmit.com</li>
</ul>
<p><strong>Production Endpoint: <a href="https://secure.docsmit.com/api/v1" target="_self">https://secure.docsmit.com/api/v1</a></strong> </p>
<p>Examples in this documentation show the API in practice.</p>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="testing-demo" class="spec-item-header" data-entity="doc" data-entity-id="testing-demo">
								<a class="head_anchor" href="#testing-demo"></a>Testing/Demo</div>
							<div class="description doc-content">
<p>We have a testing server (www.tracksmit.com) that you can point your code to, in order to run tests without incurring the expense of sending mail.  If you would like to use it, you will need to sign up separately from Docsmit because it is entirely separate from the production server.  Once you sign up on Tracksmit, please email support@docsmit.com and ask for credits (like a real account balance).  </p>
<p>Tracksmit simulates delivery of mail, including all notices and callbacks.  Rather than taking days, the emulation happens on an accelerated schedule - mail becomes Mailed after no more than 15 minutes.  For mail service levels with delivery tracking (e.g. Certified and Priority), that is simulated too, including the delivery of a return receipt (for Return Receipt Electronic).</p>
<p>Testing endpoint: <a href="https://secure.tracksmit.com/api/v1" target="_self">https://secure.tracksmit.com/api/v1</a></p>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="how-to-use-it" class="spec-item-header" data-entity="doc" data-entity-id="how-to-use-it">
								<a class="head_anchor" href="#how-to-use-it"></a>How To Use It</div>
							<div class="description doc-content">
<p>All calls should have in their headers 'Content-Type: application/json'.</p>
<p>To use the API, the first thing you'll need to do is to auth to get a token.  Use <a href="#get-token" class="link-internal link-method" target="_self">Get Token</a>.</p>
<p><strong>Creating and Sending</strong></p>
<p>Once you have a token, to create a <a href="#messageid-userid-and-partyid" class="link-internal link-doc" target="_self">message</a> (a mailing which goes to one or more recipients) you'll use 3 steps.</p>
<ol>
<li>
<a href="#new-message" class="link-internal link-method" target="_self">/messages/new</a>  Use this to upload your list of recipients in <a href="#physicalParties" class="link-internal link-schema" target="_self">physicalParties</a> and create the new message.  Retain the MessageID that it returns.</li>
<li>
<a href="#upload-a-file" class="link-internal link-method" target="_self">/messages/{messageID}/upload</a>  Use this to upload your PDF to your messaged.</li>
<li>
<p><a href="#send-message" class="link-internal link-method" target="_self">/messages/{messageID}/send</a>  Use this to send your message.</p>
<p><strong>Tracking your mail</strong></p>
</li>
</ol>
<p>You can quickly get the status of all your mail in one call with <a href="#sent-list" class="link-internal link-method" target="_self">/messages/sent</a>. You don't need to iterate over your database of mail sent and make many calls one by one, although you can call for the status of specific messages.</p>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="docs_group" class="spec-item-header" data-entity="group" data-entity-id="docs_group">
								<a class="head_anchor" href="#docs_group"></a>API Details</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="authentication-and-access-tokens" class="spec-item-header" data-entity="doc" data-entity-id="authentication-and-access-tokens">
								<a class="head_anchor" href="#authentication-and-access-tokens"></a>Auth: Tokens, Passwords and SoftwareIDs</div>
							<div class="description doc-content">
<p><strong>Tokens</strong></p>
<p>To use the Docsmit API, at the beginning of the session you must obtain a 16 byte temporary access token, which is required for almost all of the API calls.  This token is unique to the SoftwareID and the user whose account the Software asks to work with.  The token is presented to the API as the <code>username</code> in HTTP Basic Authentication (Token Over Basic Authentication).</p>
<p>The token is obtained by calling <code>POST /token</code> with the username (i.e. email), password and the software ID of the software that is making the request.  A user can have more than one token outstanding at a time, so that more than software can be running at the same time.  A user using two instances of a software with the same SoftwareID at the same time could cause failure. </p>
<p>A token has a Time To Live (TTL) of 1 hour, which is renewed each time it is used.  Once the token expires, a new token must be obtained.  The Docsmit API SDKs (PHP, Javascript and C# .NET) handle getting a new token in the event an API request fails due to an expired token (i.e. a 401 HTTP response code).</p>
<p>After your code has finished running you can delete the token with <code>DELETE /token</code> or allow the TTL to expire naturally. </p>
<p><strong>Passwords</strong></p>
<p>A password is established when a user obtains an account.  The API expects you to hash your cleartext password using SHA-512 and then encoding it to a hex string before submitting it.  This enables your code to not store your password in cleartext and ensures we do not have your password as cleartext.  See <a href="#get-token" class="link-internal link-method" target="_self"><code>/token</code> Get Token</a> for an example.  SHA-512 hashes are readily available in all major languages.  You will not ever have to submit your cleartext password to the API.</p>
<p><strong>SoftwareIDs</strong></p>
<p>Your SoftwareID is your private number that identifies your software and you should not release it to others.  Doing so could allow others to pretend to be your software.  Authenticating through a SoftwareID enables users to limit the softwares that can access their accounts and enables Docsmit to track helpful metrics.  You can obtain a Software ID at no cost by submitting a request to api@docsmit.com. Just let us know a litle about the software, how it will use Docsmit and contact information.</p>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="messageid-userid-and-partyid" class="spec-item-header" data-entity="doc" data-entity-id="messageid-userid-and-partyid">
								<a class="head_anchor" href="#messageid-userid-and-partyid"></a>MessageID, UserID, and PartyID</div>
							<div class="description doc-content">
<p>In Docsmit a Message is a PDF document being sent to one or more recipients by USPS mail.  A unique integer MessageID refers to a particular message.  A MessageID is generated for each new draft message on Docsmit (e.g. using  <a href="#new-message" class="link-internal link-method" target="_self">/messages/new</a> or clicking Compose on the Docsmit website). </p>
<p>A PartyID is different than a UserID.  A PartyID is a record about one Party to a Message (either a sender or a recipient).  It points to both a Message and either a User or a MailAddress.  It also holds information about the transport method being used to deliver to that party (certified mail, etc).  Note that senders are also included as Parties and are indicated with isSender == true.</p>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="client-message-id" class="spec-item-header" data-entity="doc" data-entity-id="client-message-id">
								<a class="head_anchor" href="#client-message-id"></a>ClientMessageID</div>
							<div class="description doc-content">
<p>An API consumer has the option of providing its own message identifier (a <code>ClientMsgID</code>) to Docsmit when it is creating a message.  Docsmit will store the ClientMsgID and the consumer has the ability retrive by a specific ClientMsgID or to use regular expressions to retrieve more than one message.  The ClientMsgID is not limited to numeric (or alphanumeric), though use of special characters is discouraged because it may complicate your regex for searching.</p>
<p>This is useful where a consumer chooses to not store the Docsmit Message ID or has a semantic ID organizational scheme (like in the example below), which would makes the ability to match by regex valuable.  When retrieving messages with <code>/sent</code> (or <code>/drafts</code>), the consumer can seek an exact match with <code>clientMsgID</code> or can use regex with <code>clientMsfID_MR</code>.  The "MR" is a reminder that Docsmit uses <a href="https://dev.mysql.com/doc/refman/5.1/en/regexp.html" target="_self">the mysql form of regex</a>.</p>
<p>For example, let's picture an HR software consumer could send messages that relate to a Matter, Employee number and correspondence number.  The ClientMsgID for the first message to an employee for a particular matter could be: <code>001043-001368-001</code>.  Note that the placement of the hyphens facilitates the use of regex.</p>
<p>Some examples:</p>
<ul>
<li>
<code>^001043-</code> All messages relating to matter 1043</li>
<li>
<code>^a{0,}-001368</code> All messages relating to employee 1368</li>
</ul>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="physical-mail-print-and-mail-parameters" class="spec-item-header" data-entity="doc" data-entity-id="physical-mail-print-and-mail-parameters">
								<a class="head_anchor" href="#physical-mail-print-and-mail-parameters"></a>Physical Mail (Print and Mail) Parameters</div>
							<div class="description doc-content">
<p>When sending mail, there are parameters relate to how the PDF attachment is printed and mailed (some optional).  They are provided in each entry in physicalParties:</p>
<ul>
<li>
<p>sendType - Required. This will indicate whether the item is sent 1st class, priority or some type of certified. Permitted values: "Priority Mail", "Priority Mail with Signature", "First Class", "Certified", "Certified, Electronic Return Receipt", "Certified, Return Receipt". Default value: "Certified, Electronic Return Receipt"</p>
</li>
<li>
<p>envelope - Required. This is the type of envelope used and must be compatible with the sendType and with the number of sheets of paper used to print the PDF  attachment. Permitted values: “#10” (5 sheet limit), “Flat” (47 sheet limit), “Priority Letter” (50 sheet limit).</p>
</li>
<li>
<p>paperSize - Optional. This indicates the size of the paper to be printed on. Currently, this can only be "Letter", meaning 8.5" x 11" paper. In the future, there may be the option for "Legal" which will be 8.5" x 14". [Values here map to DocumentClass in c2m] Permitted values: "Letter". Default value: "Letter"</p>
</li>
<li>
<p>sided - Optional. This can be either 1 or 2 to indicate single sided or double sided. Permitted Values: 1, 2. Default value: 1. [Values map to c2m's printOption]</p>
</li>
<li>
<p>plusRegular - Optional. This indicates that a copy is also to be sent out by first class mail in addition to the method specified in sendType. If sendType is "First Class", plusRegular is ignored (because it is already going out First Class). Permitted values: 1 or 0 or true or false. Default value: 0.</p>
</li>
</ul>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="callback-notices-vs-email-notices" class="spec-item-header" data-entity="doc" data-entity-id="callback-notices-vs-email-notices">
								<a class="head_anchor" href="#callback-notices-vs-email-notices"></a>Callback notices (vs email notices)</div>
							<div class="description doc-content">
<p>Docsmit notifies message parties of various events - for example, when a person sends a Signature Required message to another person and the receiver signs for it, Docsmit notifies the Sender.  Usually this notice is sent by an email that provides a record of the data about the signature (which message, who signed, when, etc).   By sending messages throught the API, you can specify that Docsmit give notice of certain events by making a HTTP POST to a URI in your domain with the data in JSON format, in addition to receiving confirmation by email.  By receiving and processing the data in the POST your system gets an immediate and direct notification in a format that can easily be used by your software, rather than relying on the delivery, encoding and formatting vagaries of email.</p>
<p>The event notices that you can receive by callback URI are:</p>
<ul>
<li>
<code>signedFor</code>: A message was signed for.</li>
<li>
<code>delivered</code>: A message was succesfully delivered.</li>
<li>
<code>bounced</code>: COMING SOON</li>
<li>
<code>flagged</code>: COMING SOON</li>
<li>
<code>reminderSent</code>: COMING SOON</li>
<li>
<code>unclaimed</code>: COMING SOON</li>
</ul>
<p>You set the endpoints that receive the POST when you create a message (<code>/messages/new</code>) and include the <code>callbacks</code> option in the JSON.  Note that for security purposes the domain you specify in the callback must be the same as the domain in your email address. (If your application requires something else, please contact us at api@docsmit.com.) The messageID is substituted for <code>{messageID}</code>, if present, in the callback URI.  For example:</p>
<ul>
<li>"signedFor" : "<a href="https://www.example.com/docsmit/signedFor/%7BmessageID%7D" target="_self">https://www.example.com/docsmit/signedFor/{messageID}</a>"
or</li>
<li>"signedFor" : "<a href="https://www.example.com/docsmit/signedFor.php?msgID=%7BmessageID%7D" target="_self">https://www.example.com/docsmit/signedFor.php?msgID={messageID}</a>"</li>
</ul>
<p>The following data about the event is delivered to your endpoint in JSON:</p>
<ul>
<li>signedFor: email, dateTime, messageID, event, clientMsgID</li>
<li>delivered: email, dateTime, messageID, event, clientMsgID</li>
<li>bounced: email, dateTime, messageID, event, reason, clientMsgID</li>
<li>complained: email, dateTime, messageID, event, reason, clientMsgID</li>
<li>flagged: email, dateTime, messageID, event, reason, clientMsgID</li>
<li>reminderSent: email, dateTime, messageID, event, next dateTime a reminder can be sent, clientMsgID</li>
<li>unclaimed: email, dateTime, messageID, event, next dateTime a reminder can be sent, clientMsgID</li>
</ul>
<p>Your callback should reply with an HTTP response status code of 200 or 201 if the POST is acceptable.  If Docsmit receives a 400+ response status code or no reply at all, you will receive an email advising you of the failed callback.</p>
<p>Docsmit provides the following URI's for you to test your callbacks.  The testing URI's will reply back with the same body contents that are given to them.</p>
<ul>
<li>signedFor: <a href="https://secure.docsmit.com/api/v1/testing/signedFor" target="_self">https://secure.docsmit.com/api/v1/testing/signedFor</a>
</li>
<li>signedFor: <a href="https://secure.docsmit.com/api/v1/testing/signedFor/%7BmessageID%7D" target="_self">https://secure.docsmit.com/api/v1/testing/signedFor/{messageID}</a>
</li>
<li>
<p>signedFor: <a href="https://secure.docsmit.com/api/v1/testing/signedFor?msgID=%7BmessageID%7D" target="_self">https://secure.docsmit.com/api/v1/testing/signedFor?msgID={messageID}</a></p>
</li>
<li>
<p>delivered: <a href="https://secure.docsmit.com/api/v1/testing/delivered" target="_self">https://secure.docsmit.com/api/v1/testing/delivered</a></p>
</li>
<li>delivered: <a href="https://secure.docsmit.com/api/v1/testing/delivered/%7BmessageID%7D" target="_self">https://secure.docsmit.com/api/v1/testing/delivered/{messageID}</a>
</li>
<li>
<p>delivered: <a href="https://secure.docsmit.com/api/v1/testing/delivered?msgID=%7BmessageID%7D" target="_self">https://secure.docsmit.com/api/v1/testing/delivered?msgID={messageID}</a></p>
</li>
<li>
<p>bounced: <a href="https://secure.docsmit.com/api/v1/testing/bounced" target="_self">https://secure.docsmit.com/api/v1/testing/bounced</a></p>
</li>
<li>bounced: <a href="https://secure.docsmit.com/api/v1/testing/bounced/%7BmessageID%7D" target="_self">https://secure.docsmit.com/api/v1/testing/bounced/{messageID}</a>
</li>
<li>
<p>bounced: <a href="https://secure.docsmit.com/api/v1/testing/bounced?msgID=%7BmessageID%7D" target="_self">https://secure.docsmit.com/api/v1/testing/bounced?msgID={messageID}</a></p>
</li>
<li>
<p>complained: <a href="https://secure.docsmit.com/api/v1/testing/complained" target="_self">https://secure.docsmit.com/api/v1/testing/complained</a></p>
</li>
<li>complained: <a href="https://secure.docsmit.com/api/v1/testing/complained/%7BmessageID%7D" target="_self">https://secure.docsmit.com/api/v1/testing/complained/{messageID}</a>
</li>
<li>
<p>complained: <a href="https://secure.docsmit.com/api/v1/testing/complained?msgID=%7BmessageID%7D" target="_self">https://secure.docsmit.com/api/v1/testing/complained?msgID={messageID}</a></p>
</li>
<li>
<p>reminderSent: <a href="https://secure.docsmit.com/api/v1/testing/reminderSent" target="_self">https://secure.docsmit.com/api/v1/testing/reminderSent</a></p>
</li>
<li>reminderSent: <a href="https://secure.docsmit.com/api/v1/testing/reminderSent/%7BmessageID%7D" target="_self">https://secure.docsmit.com/api/v1/testing/reminderSent/{messageID}</a>
</li>
<li>reminderSent: <a href="https://secure.docsmit.com/api/v1/testing/reminderSent?msgID=%7BmessageID%7D" target="_self">https://secure.docsmit.com/api/v1/testing/reminderSent?msgID={messageID}</a>
</li>
</ul>
<p>See the examples for usage.</p>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="misc-notes" class="spec-item-header" data-entity="doc" data-entity-id="misc-notes">
								<a class="head_anchor" href="#misc-notes"></a>Misc Notes</div>
							<div class="description doc-content">
<p>Docsmit API resources communicate using URLs and JSON over HTTPS, unless otherwise specified.  For example, <code>GET /messages/{msgID}/documentation</code> returns a PDF file.</p>
<p>All times should be specified in UTC/GMT.  Any time zone adjustments should be done by the consumer.</p>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="php-sdk" class="spec-item-header" data-entity="doc" data-entity-id="php-sdk">
								<a class="head_anchor" href="#php-sdk"></a>PHP SDK</div>
							<div class="description doc-content">
<p>The PHP SDK facilitates synchronous calling of the API and is contained in class <code>DocsmitAPI</code>.  You can download the PHP SDK at <a href="https://secure.tracksmit.com/downloads/DocsmitAPI.php.txt" target="_self">https://secure.tracksmit.com/downloads/DocsmitAPI.php.txt</a> and demo code that shows how to send mail is at <a href="https://secure.tracksmit.com/downloads/demo.php.txt" target="_self">https://secure.tracksmit.com/downloads/demo.php.txt</a>.</p>
<p>When you create an instance of the SDK class, you provide the user's email address and password as well as the softwareID of the software making the call as authentication data to obtain a temporary access token from the API.  </p>
<pre><code>$docsmitAPI = new DocsmitAPI ($email, $hexHashedPassword, $softwareID[, $URIBase])
</code></pre>
<p>After instantiation you can confirm the authentication was successful by checking the token with <code>if ($docsmitAPI-&gt;tokenWasOK())</code>.  The SDK class retains the token and passes it to the API with each API call.  When calling the API, if the class finds the token has expired, it will refresh the token and then retry the API call.</p>
<p>Note that for security purposes, you do not deliver the password to even the SDK class as cleartext.  When instantiating the class, you deliver the password, after it has been hashed with sha512 and converted to (lowercase) hex.  The SDK provides a static helper function to perform the hashing and encoding:</p>
<pre><code>$hexHashedPassword = DocsmitAPI::hexHashPW(<span class="hljs-string">"myCleartextPassword"</span>);
</code></pre>
<p>Optional parameter <code>URIBase</code> is used in the rare circumstance you need to use a different API version than the default (currently "<a href="https://secure.docsmit.com/api/v1/" target="_self">https://secure.docsmit.com/api/v1/</a>").</p>
<h2 id="php-sdk__HTTP calls" class="anchor">HTTP calls</h2>
<p>Class methods that call the API are named for the verbs used in the call.  <code>$url</code> is the part of the URI that follows the URIBase, such as <code>/messages/inbox</code>).  <code>$params</code> is an associative array (such as <code>array("signed" =&gt; true)</code>) of the parameters to go the API.  All methods except <code>get()</code> convert <code>$params</code> to JSON and send it in the HTTP body. <code>get()</code> converts it to a URI query (like <code>/messages/inbox?signed=true</code>).</p>
<ul>
<li><code>get($url, $params)</code></li>
<li><code>post($url, $params)</code></li>
<li><code>delete($url, $params)</code></li>
<li><code>put($url, $params)</code></li>
</ul>
<h2 id="php-sdk__Uploading Files" class="anchor">Uploading Files</h2>
<p>There are two methods for uploading files using DocsmitAPI SDK, however they both use the same API resource <code>/upload</code>.</p>
<p><strong>Using file path - postFile($uri, $params)</strong></p>
<p><code>postFile()</code> is used to upload a single file that is stored in the file system.  The file is specified by including the file path as <code>filePath</code> in $params.</p>
<p>Example:</p>
<pre><code>$docsmit = new DocsmitAPI($email, $hashedPW, $softwareID);
$params[<span class="hljs-string">"filePath"</span>] = <span class="hljs-string">"D:\images\mcc-profuction-acc(55c3308aa2708).PDF"</span>;
$docsmit-&gt;postFile(<span class="hljs-string">'/messages/1420/upload'</span>, $params);
</code></pre>
<ul>
<li>Upload file(s) using blob - postBlob($URI, $params)</li>
</ul>
<p><code>postBlob()</code> is used to upload one or more files that are stored variables, rather than the local filesystem. This is useful where the file to be uploaded is the result of a process, rather than something that will be kept.  For example, if the file to be uploaded is an encrypted version of a local file, the local file can be transformed in memory (in a variable) and then uploaded without ever saving the file.  Or since FPDF or TCPDF can generate PDF files to a variable, you can similarly upload from a variable without ever saving the file to the filesystem.</p>
<p>It has second parameter which is <code>$params</code>. it also required <code>$params['files']</code> associative array having <code>filename</code>,<code>type</code> and <code>content</code> parameter with valid filename,type and content and <code>$params["fields"]</code>. Below is the example.</p>
<p>Example:</p>
<pre><code>$docsmit = new DocsmitAPI($email, $hashedPW, $softwareID);
    $params[<span class="hljs-string">"files"</span>] = <span class="hljs-keyword">array</span>();

    $filePath1 = <span class="hljs-string">"D:\images\mcc-profuction-acc(55c3308aa2708).PDF"</span>;
    $filename1 = basename($filePath1);
    $finfo1 = new finfo(FILEINFO_MIME);
    $mimeType1 = $finfo1-&gt;file($filePath1);
    $fileBlob1 = file_get_contents($filePath1, <span class="hljs-literal">true</span>);
    $file1 = <span class="hljs-keyword">array</span>(
        <span class="hljs-string">"filename1"</span> =&gt; $filename1,
        <span class="hljs-string">"type2"</span> =&gt; $mimeType1,
        <span class="hljs-string">"content3"</span> =&gt; $fileBlob1
    );

    $filePath2 = <span class="hljs-string">"D:\images\Koala(5534d88f3443c).pdf"</span>;
    $filename2 = basename($filePath2);
    $finfo2 = new finfo(FILEINFO_MIME);
    $mimeType2 = $finfo2-&gt;file($filePath2);
    $fileBlob2 = file_get_contents($filePath2, <span class="hljs-literal">true</span>);
    $file2 = <span class="hljs-keyword">array</span>(
        <span class="hljs-string">"filename"</span> =&gt; $filename2,
        <span class="hljs-string">"type"</span> =&gt; $mimeType2,
        <span class="hljs-string">"content"</span> =&gt; $fileBlob2
    );

   array_push($params[<span class="hljs-string">"files"</span>], $file1, $file2);

   $docsmit = new DocsmitAPI($email, $hashedPW, $softwareID);
   $docsmit-&gt;postBlob(<span class="hljs-string">'/messages/1420/upload2'</span>, $params);
</code></pre>
<p><strong>Handling Responses</strong></p>
<p>Checking the HTTP response status code of the most recent call on the API is supported by methods:</p>
<ul>
<li>
<code>tokenWasOK()</code> - true if the most recent attempt to use the token was accepted.  If the SDK class had to refresh the token, this will be reflective of the refreshed token.</li>
<li>
<code>status()</code> - gives the HTTP return status code. e.g. 200, 201, 400, 401, 403</li>
<li>
<code>returnedError()</code> - true if status &gt;= 400</li>
<li>
<code>returnedSuccess()</code> - true if status is 200 or 201</li>
</ul>
<p>The body of the response is accessed by method:</p>
<ul>
<li>
<code>responseJSON()</code> - gives an object representing the JSON reply</li>
</ul>
<p>The example pages also show the PHP SDK in use.</p>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="ex-1-authentication-check-sent-messages-read-a-message" class="spec-item-header" data-entity="doc" data-entity-id="ex-1-authentication-check-sent-messages-read-a-message">
								<a class="head_anchor" href="#ex-1-authentication-check-sent-messages-read-a-message"></a>Ex 1 - Authentication, check sent messages, read a message</div>
							<div class="description doc-content">
<p>This example uses the PHP SDK to authenticate, obtain a list of sent messages and obtain detailed data about the first message.</p>
<pre><code><span class="php"><span class="hljs-meta">&lt;?php</span>

<span class="hljs-keyword">require_once</span> <span class="hljs-string">'DocsmitAPI.php'</span>;

<span class="hljs-function"><span class="hljs-keyword">function</span> <span class="hljs-title">errorCheck</span> <span class="hljs-params">($docsmit)</span> </span>{
    <span class="hljs-keyword">if</span> ($docsmit-&gt;returnedError()) {
        <span class="hljs-keyword">if</span> ($docsmit-&gt;status() == <span class="hljs-number">401</span>)
            <span class="hljs-keyword">echo</span> <span class="hljs-string">"401 Authentication Failed - Check auth data&lt;BR&gt;"</span>;
        <span class="hljs-keyword">if</span> ($docsmit-&gt;status() == <span class="hljs-number">400</span>)
            <span class="hljs-keyword">echo</span> <span class="hljs-string">"400 Bad Request&lt;BR&gt;"</span>;
        <span class="hljs-keyword">if</span> ($docsmit-&gt;status() == <span class="hljs-number">403</span>)
            <span class="hljs-keyword">echo</span> <span class="hljs-string">"403 Forbidden - Authenticated, but no permissions for that request&lt;BR&gt;"</span>;
        <span class="hljs-keyword">return</span> <span class="hljs-keyword">true</span>;
    }
    <span class="hljs-keyword">return</span> <span class="hljs-keyword">false</span>;
}

$email = <span class="hljs-string">''</span>; <span class="hljs-comment">//The user's email address, e.g. tom@example.com</span>
$password = <span class="hljs-string">''</span>; <span class="hljs-comment">//Your password here, e.g. RainbowUnicorn</span>
$softwareID = <span class="hljs-string">''</span>; <span class="hljs-comment">//Your software ID here, e.g. cdca82178a52505ab5232e97b5a39b39</span>

$hexHashedPW = DocsmitAPI::hexHashPW($password);
$docsmit = <span class="hljs-keyword">new</span> DocsmitAPI($email, $hexHashedPW, $softwareID);

<span class="hljs-keyword">echo</span> <span class="hljs-string">"&lt;h1&gt;Sent Messages for "</span> . $email . <span class="hljs-string">"&lt;/h1&gt;&lt;BR&gt;&lt;BR&gt;"</span>;

$docsmit-&gt;get(<span class="hljs-string">'/messages/sent'</span>);
<span class="hljs-keyword">if</span> (errorCheck($docsmit))
    <span class="hljs-keyword">die</span>(<span class="hljs-string">"ITRW, add error resolution here"</span>);
$response = $docsmit-&gt;responseJSON();
<span class="hljs-keyword">foreach</span> ($response <span class="hljs-keyword">as</span> $message) {
    <span class="hljs-keyword">echo</span> $message-&gt;id . <span class="hljs-string">" "</span>;
    <span class="hljs-keyword">if</span> ($message-&gt;certified)
        <span class="hljs-keyword">echo</span> <span class="hljs-string">"CERTIFIED "</span>;
    <span class="hljs-keyword">echo</span> $message-&gt;title . <span class="hljs-string">"&lt;BR&gt;"</span>;
}
<span class="hljs-keyword">echo</span> <span class="hljs-string">"&lt;BR&gt;"</span>;

$firstID = $response[<span class="hljs-number">0</span>]-&gt;id;
<span class="hljs-keyword">echo</span> <span class="hljs-string">"&lt;H3&gt;First message's details [messageID = $firstID]&lt;/H3&gt;"</span>;
$docsmit-&gt;get(<span class="hljs-string">'/messages/'</span> . $firstID);
<span class="hljs-keyword">if</span> (errorCheck($docsmit))
    <span class="hljs-keyword">die</span>(<span class="hljs-string">"ITRW, add error resolution here"</span>);
$response = $docsmit-&gt;responseJSON();
<span class="hljs-keyword">echo</span> <span class="hljs-string">"decoded = &lt;PRE&gt;"</span>.json_encode($response, JSON_PRETTY_PRINT).<span class="hljs-string">"&lt;/pre&gt;&lt;BR&gt;"</span>;

<span class="hljs-meta">?&gt;</span></span>
</code></pre>
<p>To invoke error catching, try altering any of the authentication data to be wrong or change <code>$firstID</code> to be a wrong number, like 7.</p>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="ex-2-send-mail-merge-with-callback-notice" class="spec-item-header" data-entity="doc" data-entity-id="ex-2-send-mail-merge-with-callback-notice">
								<a class="head_anchor" href="#ex-2-send-mail-merge-with-callback-notice"></a>Ex 2 - Send Mail Merge with callback notice</div>
							<div class="description doc-content">
<p>Sending messages is fairly simple.  In the PHP SDK, the consumer places the relevant parameters into an array, creates the new message, then sends it
To execute a mail merge we loop over information retrieved from a database, sending a message during each iteration.  For the sake of this example, we assume that the results have been received in associative array <code>$dbResults</code>, though you can iterate over any data source.  We set a callback notice endpoint for each email and also show a sample script for that endpoint.</p>
<p><strong>Sample script for Mail Merge</strong></p>
<pre><code>$docsmit = new DocsmitAPI($email, Docsmit::hexhashedPW($clearPW), $softwareID);
$params = arrray ();
foreach ($dbResults <span class="hljs-keyword">as</span> $dbResult) {
    $params [<span class="hljs-string">"subject"</span>] = $dbResult[<span class="hljs-string">"premises"</span>];
    $params[<span class="hljs-string">"to"</span>] = $dbResult [<span class="hljs-string">"to"</span>];
    $params[<span class="hljs-string">"body"</span>] = <span class="hljs-string">"&lt;p&gt;This is to notify you that your lease of "</span> . $dbResult[<span class="hljs-string">"premises"</span>]
        . <span class="hljs-string">" will terminate on "</span> . $dbResult[<span class="hljs-string">"endDate"</span>] . <span class="hljs-string">". Thank you for your business."</span>;
    $messageID = $docsmit-&gt;post(<span class="hljs-string">"/new"</span>, $params);
    /* error checking: confirm the message was constructed successfully */
    $docsmit-&gt;post(<span class="hljs-string">"/send/"</span>.$messageID);
    /* error checking: confirm the message was sent successfully */
}
</code></pre>
<p><strong>Sample script for signedFor callback endpoint</strong></p>
<p><code>SignatureLog</code> is a made-up class that writes data to a made-up database that keeps track of signatures.  In this example we give it the event ("<code>signedFor</code>"), the Docsmit MessageID, the date and time of the signature, and the email address of the person that signed.</p>
<pre><code>&lt;?php
$payloadJSON = file_get_contents(<span class="hljs-string">'php://input'</span>);
$payload= json_decode( $payloadJSON, TRUE ); //convert JSON into <span class="hljs-keyword">array</span>
$signatureLog = new SignatureLog ();
$signatureLog-&gt;record ($payload-&gt;event, $payload-&gt;messageID, $payload-&gt;dateTime, $payload-&gt;email);
</code></pre>
<p>Of course, in the alternative, <code>SignatureLog</code> could have taken a <code>ClientMsgID</code> instead of a messageID (Docsmit's message ID).</p>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="ex-3-blast-messages-clientmsgids-and-message-status-check" class="spec-item-header" data-entity="doc" data-entity-id="ex-3-blast-messages-clientmsgids-and-message-status-check">
								<a class="head_anchor" href="#ex-3-blast-messages-clientmsgids-and-message-status-check"></a>Ex 3 - Blast messages, clientMsgIDs and message status check</div>
							<div class="description doc-content">
<p>Email a bunch of people with an email blast and get notified by HTTP when each person signs.  Each person gets a clientMsgID.</p>
<p>__Sample script for the Blast messages</p>
<pre><code>$email = <span class="hljs-string">''</span>; <span class="hljs-comment">//The user's email address, e.g. tom@example.com</span>
$password = <span class="hljs-string">''</span>; <span class="hljs-comment">//Your password here, e.g. RainbowUnicorn</span>
$softwareID = <span class="hljs-string">''</span>; <span class="hljs-comment">//Your software ID here, e.g. cdca82178a52505ab5232e97b5a39b39</span>

$hexHashedPW = DocsmitAPI::hexHashPW($password);

$docsmit = <span class="hljs-keyword">new</span> DocsmitAPI($email, Docsmit::hexhashedPW($clearPW), $softwareID);
$body = <span class="hljs-keyword">array</span>(
    <span class="hljs-string">"title"</span> =&gt; <span class="hljs-string">"This is new message for the Blast Message"</span>,
    <span class="hljs-string">"bodyText"</span> =&gt; <span class="hljs-string">"This is new message for the Blast Message"</span>,
    <span class="hljs-string">"toList"</span> =&gt; <span class="hljs-keyword">array</span>(
        <span class="hljs-keyword">array</span>(
            <span class="hljs-string">"email"</span> =&gt; <span class="hljs-string">"shc.narola@narolainfotech.com"</span>,
            <span class="hljs-string">"isSender"</span> =&gt; <span class="hljs-string">"1"</span>
        ),
        <span class="hljs-keyword">array</span>(
            <span class="hljs-string">"email"</span> =&gt; <span class="hljs-string">"hid.narola@narolainfotech.com"</span>
        ),
        <span class="hljs-keyword">array</span>(
            <span class="hljs-string">"email"</span> =&gt; <span class="hljs-string">"apa.narola@narolainfotech.com"</span>
        ),
    ),
    <span class="hljs-string">"blastMessage"</span> =&gt; <span class="hljs-keyword">true</span>,
    <span class="hljs-string">"signatureRequired"</span> =&gt; <span class="hljs-keyword">true</span>
);
$docsmit-&gt;post(<span class="hljs-string">'/messages/new'</span>, $body);
<span class="hljs-comment">/* error checking: confirm the message was constructed successfully */</span>

$docsmit-&gt;post(<span class="hljs-string">"/send/"</span>.$messageID);
<span class="hljs-comment">/* error checking: confirm the message was sent successfully */</span>
</code></pre>
<p>After the messages have been sent, we can periodically check the status of the message's recipients' signatures.  This could be done, for example, as a cron job.</p>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="methods_group" class="spec-item-header" data-entity="group" data-entity-id="methods_group">
								<a class="head_anchor" href="#methods_group"></a>API Methods</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="authentication-tokens_group" class="spec-item-header" data-entity="group" data-entity-id="authentication-tokens_group">
								<a class="head_anchor" href="#authentication-tokens_group"></a>Authentication Tokens</div>
							<div class="description description__main">
<p>Handles temporary access tokens.</p>
</div>
							<div class="method-section">
								<div class="group-methods-summary">
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#get-token">Get Token</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="POST">POST</span>
											<span class="method-url">/token/</span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#delete-token">Delete Token</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="DELETE">DELETE</span>
											<span class="method-url">/token</span>
</div>
</div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="get-token" class="spec-item-header" data-entity="method" data-entity-id="get-token">
								<a class="head_anchor" href="#get-token"></a>Get Token</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="POST">POST</span>
									<span class="method-url">/token/</span>
</div>
</div>
							<div class="description description__main">
<p>This endpoint allows the user to obtain a token by authenticating with username (i.e. email address), password and SoftwareID to obtain a token.  See notes below and <a href="#authentication-and-access-tokens" class="link-internal link-doc" target="_self">Auth: Tokens, Passwords and SoftwareIDs</a> about not sending the password as cleartext.  Tokens currently expire 1 hour after issuance.</p>
<p>The token is then presented to the API in all subsequent calls as the <code>username</code> in HTTP Basic Authentication (Token Over Basic Authentication).</p>
<p>Once the token expires (e.g. a 401 HTTP response code is received to an API call), a new token should be obtained.  A user that uses two instances of software with the same SoftwareID could have difficulties. </p>
<p>This is the only API resource that does not require a token to be presented.</p>
</div>
							<div class="method-section">
								<h4>Notes</h4>
								<div class="description method-content-description">
<p>Do not send the password by cleartext.  First apply a SHA-512 hash to the cleartext password, then encode it as a hex string (case does not matter) and send only that hashed result.<br>For example, in PHP:</p>
<pre><code>$email = <span class="hljs-string">'someone@example.com'</span>;
$password = <span class="hljs-string">'cleartext'</span>;
$softwareID = <span class="hljs-string">''</span>; // Enter softwareID here
$hashedPW = hash(<span class="hljs-string">'sha512'</span>, $password)
$uri = <span class="hljs-string">'https://secure.docsmit.com/api/v1/token'</span>; // or use tracksmit.com <span class="hljs-keyword">for</span> testing
$curl = curl_init($uri);
$curl_post_data = <span class="hljs-keyword">array</span>(
    <span class="hljs-string">'email'</span> =&gt; $email,
    <span class="hljs-string">'password'</span> =&gt; $hashedPW,
    <span class="hljs-string">'softwareID'</span> =&gt; $softwareID
);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, <span class="hljs-literal">true</span>);
curl_setopt($curl, CURLOPT_POST, <span class="hljs-literal">true</span>);
curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
$curl_response = curl_exec($curl);
<span class="hljs-keyword">if</span> ($curl_response === <span class="hljs-literal">false</span>) {
    $info = curl_getinfo($curl);
    curl_close($curl);
    die(<span class="hljs-string">'error occured during curl exec. Additional info: '</span> . var_export($info));
}
curl_close($curl);
$decoded = json_decode($curl_response);
<span class="hljs-keyword">if</span> (isset($decoded-&gt;response-&gt;status) &amp;&amp; $decoded-&gt;response-&gt;status == <span class="hljs-string">'ERROR'</span>) {
    die(<span class="hljs-string">'error occured: '</span> . $decoded-&gt;response-&gt;errormessage);
}
echo <span class="hljs-string">'response ok! Token = '</span> . print_r ($decoded-&gt;token, <span class="hljs-literal">true</span>);
</code></pre>
</div>
</div>
							<div class="method-section">
								<h4>Request body</h4>
								<div class="request-body">
									<div class="request-body__content-types">
										<div class="request-body__content-type">application/x-www-form-urlencoded</div>
</div>
									<div class="description method-content-description">
<p><strong>Example:</strong></p>
<pre><code><span class="hljs-keyword">POST</span> /<span class="hljs-keyword">token</span> {
<span class="hljs-string">"email"</span> : <span class="hljs-string">"tom@example.com"</span>,
<span class="hljs-string">"password"</span> : <span class="hljs-string">"0ce71cec25e7e01fc96cb6a43c7a5428a27991dcc311ce5544b45589a956d820b70d6d84a2d9b37cbce39d193fe3e3973c69c6f67bf99613e87f400cbde23826"</span>,
<span class="hljs-string">"softwareID"</span> : <span class="hljs-string">"1d1337900b318d011bf64aee747b442d"</span>
}
</code></pre>
</div>
									<div class="schema">
										<div class="schema-property__definition">
											<div class="schema-property__left">
												<div class="schema__type">
													<span class="primitive">Object</span>
</div>
</div>
											<div class="schema-property__right"></div>
</div>
										<div class="schema-properties">
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">SoftwareID</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>16 bytes in base16 (0-F).  This indicates which software is attempting to access the API. Only softwares which are granted permission by the user will be able to access the API.</p>
</div>
</div>
</div>
</div>
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">password</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>user's password, hashed with sha512 and encoded as a hex string.  DO NOT send password by cleartext.</p>
</div>
</div>
</div>
</div>
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">email</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>email address of the user</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-12">
												<span class="response-details__status" data-response-status="201">201</span>
												<span class="response-details__name">Created</span>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">token</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">e6e89065213c63adda64c43ca22dc2e0</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">userID</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">681</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">name</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">masaledara</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">email</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">masaledaraconsulting@gmail.com</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">company</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">test company2</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">address1</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">3103 N 10th Street</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">address2</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">Suite 201</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">city</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">Arlington</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">state</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">VA</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">zip</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">22210</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">suspended</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="integer">integer</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">timezone</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">America/New_York</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">timezoneAsNumber</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">UTC/GMT -04:00</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">attachmentFilesMaxLimit</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="integer">integer</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token number">314572800</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">bullpenFilesMaxLimit</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="integer">integer</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token number">104857600</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">creditBalance</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">9011.93</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="delete-token" class="spec-item-header" data-entity="method" data-entity-id="delete-token">
								<a class="head_anchor" href="#delete-token"></a>Delete Token</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="DELETE">DELETE</span>
									<span class="method-url">/token</span>
</div>
</div>
							<div class="description description__main">
<p>Deletes the token, revoking its ability to authenticate.</p>
</div>
							<div class="method-section">
								<h4>Request body</h4>
								<div class="request-body">
									<div class="schema">
										<div class="schema-property__definition">
											<div class="schema-property__left">
												<div class="schema__type">
													<span class="primitive">Object</span>
</div>
</div>
											<div class="schema-property__right"></div>
</div>
										<div class="schema-properties">
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">softwareID</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>16 bytes in base16 (0-F).  This indicates which software is attempting to access the API. (See POST /token)</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Either "Token deleted." or "Token not found."</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">Failed</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Needs SoftwareID</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="messages-before-sending_group" class="spec-item-header" data-entity="group" data-entity-id="messages-before-sending_group">
								<a class="head_anchor" href="#messages-before-sending_group"></a>Messages - Before Sending</div>
							<div class="method-section">
								<div class="group-methods-summary">
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#new-message">New Message</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="POST">POST</span>
											<span class="method-url">/messages/new</span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#upload-a-file">Upload a file</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="POST">POST</span>
											<span class="method-url">/messages/<code>{messageID}</code>/upload</span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#send-message">Send Message</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="POST">POST</span>
											<span class="method-url">/messages/<code>{messageID}</code>/send</span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#add-party">Add Party</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="POST">POST</span>
											<span class="method-url">/messages/<code>{messageID}</code>/party</span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#delete-party">Delete Party</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="DELETE">DELETE</span>
											<span class="method-url">/messages/<code>{messageID}</code>/party/<code>{partyID}</code></span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#update-party">Update Party</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="PUT">PUT</span>
											<span class="method-url">/messages/<code>{messageID}</code>/party/<code>{partyID}</code></span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#get-the-price-and-the-details">Get the price and the details</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="GET">GET</span>
											<span class="method-url">/messages/<code>{messageID}</code>/priceCheck</span>
</div>
</div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="new-message" class="spec-item-header" data-entity="method" data-entity-id="new-message">
								<a class="head_anchor" href="#new-message"></a>New Message</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="POST">POST</span>
									<span class="method-url">/messages/new</span>
</div>
</div>
							<div class="description description__main">
<p>Create a new message.  All of the request fields are optional, except <code>title</code>.</p>
</div>
							<div class="method-section">
								<h4>Request body</h4>
								<div class="request-body">
									<div class="description method-content-description">
<p>If the return address parameters (rtn*) are not specified or are blank, when the message is sent Docsmit will attempt to use the user's address.  </p>
</div>
									<div class="schema">
										<div class="schema-property__definition">
											<div class="schema-property__left">
												<div class="schema__type">
													<span class="primitive">Object</span>
</div>
</div>
											<div class="schema-property__right"></div>
</div>
										<div class="schema-properties">
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">title</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>This is not shown to the recipient and can only be viewed by the sender.</p>
</div>
</div>
</div>
</div>
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">physicalParties</div>
														<div class="schema-property__type">
															<a class="item-ref" href="#physicalParties">physicalParties</a>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>physicalParties is an array of MailAddresses, that is a list of mail recipients and how they are being sent the mail.</p>
</div>
</div>
</div>
</div>
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">rtnName</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>Return address name.</p>
</div>
														<div class="schema-property-examples">
															<span class="lbl">Example:</span>
															<div class="schema-property-examples__list"><span class="example-primitive token string">"John Paul"</span></div>
</div>
</div>
</div>
</div>
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">rtnOrganization</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>Return address organization.</p>
</div>
														<div class="schema-property-examples">
															<span class="lbl">Example:</span>
															<div class="schema-property-examples__list"><span class="example-primitive token string">"abc &amp; co."</span></div>
</div>
</div>
</div>
</div>
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">rtnAddress1</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>Return address address1</p>
</div>
														<div class="schema-property-examples">
															<span class="lbl">Example:</span>
															<div class="schema-property-examples__list"><span class="example-primitive token string">"The Audobon Buidling"</span></div>
</div>
</div>
</div>
</div>
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">rtnAddress2</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>Return address address2</p>
</div>
														<div class="schema-property-examples">
															<span class="lbl">Example:</span>
															<div class="schema-property-examples__list"><span class="example-primitive token string">"135 Bergen Ave, Suite 101"</span></div>
</div>
</div>
</div>
</div>
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">rtnCity</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>Return address city</p>
</div>
														<div class="schema-property-examples">
															<span class="lbl">Example:</span>
															<div class="schema-property-examples__list"><span class="example-primitive token string">"Jersey City"</span></div>
</div>
</div>
</div>
</div>
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">rtnState</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>Return address state</p>
</div>
														<div class="schema-property-examples">
															<span class="lbl">Example:</span>
															<div class="schema-property-examples__list"><span class="example-primitive token string">"NJ"</span></div>
</div>
</div>
</div>
</div>
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">rtnZip</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>Return address zip.  This can be either a 5 digit zip or a 9 digit zip+4.</p>
</div>
														<div class="schema-property-examples">
															<span class="lbl">Example:</span>
															<div class="schema-property-examples__list"><span class="example-primitive token string">"12345-1234"</span></div>
</div>
</div>
</div>
</div>
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">SASE</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>(Optional: default = 0)  1 indicates that a Self Addressed Stamped Envelope will be enclosed.  The postage included on the envelope will be enough for a first class #10 envelope.</p>
</div>
</div>
</div>
</div>
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">deliverAfter</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>(NOT CURRENTLY BEING USED) If set, this is the approximate time (but not before) at which the message will be sent out. Note that system adjust the time based on the user's stated time zone.</p>
</div>
														<div class="schema-property-examples">
															<span class="lbl">Example:</span>
															<div class="schema-property-examples__list"><span class="example-primitive token string">07/26/15, 04:49 AM</span></div>
</div>
</div>
</div>
</div>
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">callbackURIs</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>Provides URIs at the sender that Docsmit will call if certain events occur, such as a message bouncing or getting signed or a message becoming flagged.</p>
</div>
														<div class="schema-property-examples">
															<span class="lbl">Example:</span>
															<div class="schema-property-examples__list"><span class="example-primitive token string">{ "bounced" : "http://mailer.sender.com/docsmitAPI/bounced/{messageID}", "flagged" : "http://mailer.sender.com/docsmitAPI/flagged/{messageID}", "unclaimed" : "http://mailer.sender.com/docsmitAPI/unclaimed/{messageID}"}</span></div>
</div>
</div>
</div>
</div>
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">billTo</div>
														<div class="schema-property__type">
															<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>Alphanumeric for sender's use in tracking billing to its clients.  Setting BillTo for a message sets the BillTo for any transactions generated for that message. Grouping by BillTo code is available on Docsmit invoices.  BillTo codes can be edited, even after sending.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="201">201</span>
												<span class="response-details__name">Created</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Success.  Returns <code>messageID</code> with new message number.</p>
</div>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">messageID</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>The MessageID of the new message.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">Bad Request</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Problem with one of the request parameters.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="403">403</span>
												<span class="response-details__name">Forbidden</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>User does not have authority to perform this action on this message.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="500">500</span>
												<span class="response-details__name">Internal Server Error</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Problem inserting.</p>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Ex 1 - Create new message">
									<div class="description method-example__description">
<p>Uses some options</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">POST</span> http://secure.docsmit.com/api/v1/messages/new HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$body = array(
    <span class="token string">"title"</span> =&gt; <span class="token string">"This is new message title from Hiren testcase"</span><span class="token punctuation">,</span>
    <span class="token string">"bodyText"</span> =&gt; <span class="token string">"This is new message body from Hiren testcase"</span><span class="token punctuation">,</span>
    <span class="token string">"toList"</span> =&gt; array(
        array(
            <span class="token string">"email"</span> =&gt; <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
            <span class="token string">"isSender"</span> =&gt; <span class="token string">"1"</span>
        <span class="token punctuation">)</span><span class="token punctuation">,</span>
        array(
            <span class="token string">"email"</span> =&gt; <span class="token string">"sop.narola@narolainfotech.com"</span>
        <span class="token punctuation">)</span>
    <span class="token punctuation">)</span><span class="token punctuation">,</span>
    <span class="token string">"unclaimedNoticeTimeout"</span> =&gt; <span class="token string">"7/30/15, 03:29 AM"</span><span class="token punctuation">,</span>
    <span class="token string">"deliverAfter"</span> =&gt; <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token string">"excludeMessageBody"</span> =&gt; <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token string">"blastMessage"</span> =&gt; <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token string">"signatureRequired"</span> =&gt; <span class="token boolean">true</span>
<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;post('/messages/new'<span class="token punctuation">,</span> $body<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">201 Created </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token string">"2097"</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex2 - With callbacks">
									<div class="description method-example__description">
<p>Create new message that calls testing callback URIs</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">POST</span> http://secure.docsmit.com/api/v1/messages/new HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$body = array(
    <span class="token string">"title"</span> =&gt; <span class="token string">"This is new message title from Hiren testcase"</span><span class="token punctuation">,</span>
    <span class="token string">"bodyHTML"</span> =&gt; <span class="token string">"&lt;h1&gt;This html has also script tag&lt;/h1&gt; we need strip script tag from html."</span><span class="token punctuation">,</span>
    <span class="token string">"toList"</span> =&gt; array(
        array(
            <span class="token string">"email"</span> =&gt; <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
            <span class="token string">"isSender"</span> =&gt; <span class="token string">"1"</span>
        <span class="token punctuation">)</span><span class="token punctuation">,</span>
        array(
            <span class="token string">"email"</span> =&gt; <span class="token string">"sop.narola@narolainfotech.com"</span>
        <span class="token punctuation">)</span>
    <span class="token punctuation">)</span><span class="token punctuation">,</span>
    <span class="token string">"unclaimedNoticeTimeout"</span> =&gt; <span class="token string">"7/30/15, 03:29 AM"</span><span class="token punctuation">,</span>
    <span class="token string">"deliverAfter"</span> =&gt; <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token string">"excludeMessageBody"</span> =&gt; <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token string">"blastMessage"</span> =&gt; <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token string">"signatureRequired"</span> =&gt; <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token string">"callbackURIs"</span> =&gt; array(
        <span class="token string">"delivered"</span> =&gt; <span class="token string">"https://robert.doxmit.com/api/v1/messages/delivered?messgeID={messageID}"</span><span class="token punctuation">,</span>
        <span class="token string">"signedFor"</span> =&gt; <span class="token string">"https://robert.doxmit.com/api/v1/messages/testing/signFor/{messageID}"</span>
    <span class="token punctuation">)</span>
<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;post('/messages/new'<span class="token punctuation">,</span> $body<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">201 Created </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token string">"2058"</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex 3 - With return address">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">POST</span> http://secure.docsmit.com/api/v1/messages/new HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$body = array(
    <span class="token string">"title"</span> =&gt; <span class="token string">"This is new message title from Hiren testcase"</span><span class="token punctuation">,</span>
    <span class="token string">"bodyHTML"</span> =&gt; <span class="token string">"&lt;h1&gt;This html has also script tag&lt;/h1&gt; we need strip script tag from html."</span><span class="token punctuation">,</span>
    <span class="token string">"physicalParties"</span> =&gt; array(
array(
	
		<span class="token string">"firstName"</span>=&gt;<span class="token string">"John"</span><span class="token punctuation">,</span>
		<span class="token string">"lastName"</span>=&gt;<span class="token string">"backham"</span><span class="token punctuation">,</span>
		<span class="token string">"organization"</span>=&gt;<span class="token string">"abc &amp; co."</span><span class="token punctuation">,</span>
		<span class="token string">"address1"</span>=&gt;<span class="token string">"880 Bergen Avenue,"</span>
		<span class="token string">"address2"</span>=&gt;<span class="token string">"Suite 204"</span><span class="token punctuation">,</span>
		<span class="token string">"city"</span>=&gt;<span class="token string">"Jersey City"</span><span class="token punctuation">,</span>
		<span class="token string">"state"</span>=&gt;<span class="token string">"NJ"</span><span class="token punctuation">,</span>
		<span class="token string">"postalCode"</span>=&gt;<span class="token string">"07306"</span><span class="token punctuation">,</span>
	<span class="token punctuation">)</span><span class="token punctuation">,</span>
	array(
		<span class="token string">"firstName"</span>=&gt;<span class="token string">"Dyne"</span><span class="token punctuation">,</span>
		<span class="token string">"lastName"</span>=&gt;<span class="token string">"Hyuston"</span><span class="token punctuation">,</span>
		<span class="token string">"organization"</span>=&gt;<span class="token string">"Dyne &amp; co."</span><span class="token punctuation">,</span>
		<span class="token string">"address1"</span>=&gt;<span class="token string">"120 Main Street"</span><span class="token punctuation">,</span>
		<span class="token string">"city"</span>=&gt;<span class="token string">"New Orleans"</span><span class="token punctuation">,</span>
		<span class="token string">"state"</span>=&gt;<span class="token string">"LA"</span><span class="token punctuation">,</span>
		<span class="token string">"postalCode"</span>=&gt;<span class="token string">"12345-1234"</span><span class="token punctuation">,</span>
	<span class="token punctuation">)</span>
	
<span class="token punctuation">)</span><span class="token punctuation">,</span>
<span class="token string">"rtnName"</span>=&gt; <span class="token string">"John Paul"</span><span class="token punctuation">,</span>
<span class="token string">"rtnOrganization"</span>=&gt; <span class="token string">"ABC &amp; Co"</span><span class="token punctuation">,</span>
<span class="token string">"rtnAddress1"</span>=&gt; <span class="token string">"26 Journal Square, 8th Floor"</span>
<span class="token string">"rtnCity"</span>=&gt; <span class="token string">"Jersey City"</span><span class="token punctuation">,</span>
<span class="token string">"rtnState"</span>=&gt; <span class="token string">"NJ"</span><span class="token punctuation">,</span>
<span class="token string">"rtnZip"</span>=&gt; <span class="token string">"07306"</span><span class="token punctuation">,</span>
<span class="token string">"unclaimedNoticeTimeout"</span> =&gt; <span class="token string">"7/30/15, 03:29 AM"</span><span class="token punctuation">,</span>
<span class="token string">"deliverAfter"</span> =&gt; <span class="token string">""</span><span class="token punctuation">,</span>
<span class="token string">"excludeMessageBody"</span> =&gt; <span class="token boolean">true</span><span class="token punctuation">,</span>
<span class="token string">"blastMessage"</span> =&gt; <span class="token string">""</span><span class="token punctuation">,</span>
<span class="token string">"signatureRequired"</span> =&gt; <span class="token boolean">true</span><span class="token punctuation">,</span>
<span class="token string">"callbackURIs"</span> =&gt; array(
    <span class="token string">"delivered"</span> =&gt; <span class="token string">"https://robert.doxmit.com/api/v1/messages/delivered?messgeID={messageID}"</span><span class="token punctuation">,</span>
    <span class="token string">"signedFor"</span> =&gt; <span class="token string">"https://robert.doxmit.com/api/v1/messages/testing/signFor/{messageID}"</span>
<span class="token punctuation">)</span>
<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;post('/messages/new'<span class="token punctuation">,</span> $body<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">201 Created </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token string">"2035"</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="upload-a-file" class="spec-item-header" data-entity="method" data-entity-id="upload-a-file">
								<a class="head_anchor" href="#upload-a-file"></a>Upload a file</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="POST">POST</span>
									<span class="method-url">/messages/<code>{messageID}</code>/upload</span>
</div>
</div>
							<div class="description description__main">
<p>Uploads a PDF file to be used as the content for a message.
This call does not deliver data by JSON.  The file is delivered by <code>Content-Type:multipart/form-data</code> and that should be in the headers (instead of Content-Type:application/json).</p>
<p>This is an example written in PHP using <code>curl()</code> and <code>CurlFile</code> to upload the file stored as $params['filePath']:</p>
<pre><code>    <span class="hljs-keyword">if</span> (isset($params[<span class="hljs-string">"filePath"</span>]) &amp;&amp; file_exists($params[<span class="hljs-string">"filePath"</span>])) {
        $ch = curl_init($URIBase . $url);
        curl_setopt($ch, CURLOPT_POST, true);
        $finfo = new finfo(FILEINFO_MIME);
        $mimeType = $finfo-&gt;file($params[<span class="hljs-string">"filePath"</span>]);
        $params[<span class="hljs-string">"file"</span>] = new CurlFile($params[<span class="hljs-string">"filePath"</span>], $mimeType);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(<span class="hljs-string">"Content-Type:multipart/form-data"</span>));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  //use for tracksmit.com server
        curl_setopt($ch, CURLOPT_USERPWD, $token);
        $response_body = curl_exec($ch);
    }
</code></pre>
</div>
							<div class="method-section">
								<h4>Path variables</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageID</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right"></div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Request body</h4>
								<div class="request-body">
									<div class="request-body__content-types">
										<div class="request-body__content-type">multipart/form-data</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>File uploaded successfully.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="403">403</span>
												<span class="response-details__name">403
													<a class="small-link" href="#403">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>User does not have authority to perform this action on this message.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">400
													<a class="small-link" href="#400">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Problem with one of the request parameters.</p>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Ex 1 Upload a File using Blob">
									<div class="description method-example__description">
<p>PHP SDK: Upload a file using blob for message as an attachment.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">POST</span> http://secure.docsmit.com/api/v1/messages/1420/upload HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"files"</span><span class="token punctuation">]</span> = array(<span class="token punctuation">)</span><span class="token punctuation">;</span>

$filePath1 = <span class="token string">"D:\images\mcc-profuction-acc(55c3308aa2708).PNG"</span><span class="token punctuation">;</span>
$filename1 = basename($filePath1<span class="token punctuation">)</span><span class="token punctuation">;</span>
$finfo1 = new finfo(FILEINFO_MIME<span class="token punctuation">)</span><span class="token punctuation">;</span>
$mimeType1 = $finfo1-&gt;file($filePath1<span class="token punctuation">)</span><span class="token punctuation">;</span>
$fileBlob1 = file_get_contents($filePath1<span class="token punctuation">,</span> <span class="token boolean">true</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$file1 = array(
    <span class="token string">"filename1"</span> =&gt; $filename1<span class="token punctuation">,</span>
    <span class="token string">"type2"</span> =&gt; $mimeType1<span class="token punctuation">,</span>
    <span class="token string">"content3"</span> =&gt; $fileBlob1
<span class="token punctuation">)</span><span class="token punctuation">;</span>

$filePath2 = <span class="token string">"D:\images\Koala(5534d88f3443c).jpg"</span><span class="token punctuation">;</span>
$filename2 = basename($filePath2<span class="token punctuation">)</span><span class="token punctuation">;</span>
$finfo2 = new finfo(FILEINFO_MIME<span class="token punctuation">)</span><span class="token punctuation">;</span>
$mimeType2 = $finfo2-&gt;file($filePath2<span class="token punctuation">)</span><span class="token punctuation">;</span>
$fileBlob2 = file_get_contents($filePath2<span class="token punctuation">,</span> <span class="token boolean">true</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$file2 = array(
    <span class="token string">"filename"</span> =&gt; $filename2<span class="token punctuation">,</span>
    <span class="token string">"type"</span> =&gt; $mimeType2<span class="token punctuation">,</span>
    <span class="token string">"content"</span> =&gt; $fileBlob2
<span class="token punctuation">)</span><span class="token punctuation">;</span>

array_push($params<span class="token punctuation">[</span><span class="token string">"files"</span><span class="token punctuation">]</span><span class="token punctuation">,</span> $file1<span class="token punctuation">,</span> $file2<span class="token punctuation">)</span><span class="token punctuation">;</span>

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;postBlob('/messages/<span class="token number">1420</span>/upload'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;responseBody(<span class="token punctuation">)</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span> <span class="token property">"fileData"</span><span class="token operator">:</span> <span class="token punctuation">[</span> <span class="token punctuation">{</span> <span class="token property">"fileID"</span><span class="token operator">:</span> <span class="token number">1872</span><span class="token punctuation">,</span> <span class="token property">"fileName"</span><span class="token operator">:</span> <span class="token string">"mcc-profuction-acc(55c3308aa2708).PNG"</span><span class="token punctuation">,</span> <span class="token property">"size"</span><span class="token operator">:</span> <span class="token string">"87897"</span><span class="token punctuation">,</span> <span class="token property">"uploadedTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-22 09:00:16"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span> <span class="token punctuation">{</span> <span class="token property">"fileID"</span><span class="token operator">:</span> <span class="token number">1873</span><span class="token punctuation">,</span> <span class="token property">"fileName"</span><span class="token operator">:</span> <span class="token string">"Koala(5534d88f3443c).jpg"</span><span class="token punctuation">,</span> <span class="token property">"size"</span><span class="token operator">:</span> <span class="token string">"780831"</span><span class="token punctuation">,</span> <span class="token property">"uploadedTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-22 09:00:16"</span> <span class="token punctuation">}</span> <span class="token punctuation">]</span> <span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex 2 Upload File using filepath">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">POST</span> http://secure.docsmit.com/api/v1/messages/1420/upload HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$filePath = <span class="token string">"/home/robert/staging_secure/drive/docs/files/mcc-profuction-acc(55c3308aa2708).PNG"</span><span class="token punctuation">;</span>
$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span> SECUREURL . <span class="token string">"/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span>'filePath'<span class="token punctuation">]</span> = $filePath<span class="token punctuation">;</span>
$docsmit-&gt;post('/messages/<span class="token number">1420</span>/upload'<span class="token punctuation">,</span> $params<span class="token punctuation">,</span> <span class="token number">1</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;responseBody(<span class="token punctuation">)</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span> <span class="token property">"fileData"</span><span class="token operator">:</span> <span class="token punctuation">[</span> <span class="token punctuation">{</span> <span class="token property">"fileID"</span><span class="token operator">:</span> <span class="token number">1872</span><span class="token punctuation">,</span> <span class="token property">"fileName"</span><span class="token operator">:</span> <span class="token string">"mcc-profuction-acc(55c3308aa2708).PNG"</span><span class="token punctuation">,</span> <span class="token property">"size"</span><span class="token operator">:</span> <span class="token string">"87897"</span><span class="token punctuation">,</span> <span class="token property">"uploadedTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-22 09:00:16"</span> <span class="token punctuation">}</span><span class="token punctuation">,</span> <span class="token punctuation">{</span> <span class="token property">"fileID"</span><span class="token operator">:</span> <span class="token number">1873</span><span class="token punctuation">,</span> <span class="token property">"fileName"</span><span class="token operator">:</span> <span class="token string">"Koala(5534d88f3443c).jpg"</span><span class="token punctuation">,</span> <span class="token property">"size"</span><span class="token operator">:</span> <span class="token string">"780831"</span><span class="token punctuation">,</span> <span class="token property">"uploadedTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-22 09:00:16"</span> <span class="token punctuation">}</span> <span class="token punctuation">]</span> <span class="token punctuation">}</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="send-message" class="spec-item-header" data-entity="method" data-entity-id="send-message">
								<a class="head_anchor" href="#send-message"></a>Send Message</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="POST">POST</span>
									<span class="method-url">/messages/<code>{messageID}</code>/send</span>
</div>
</div>
							<div class="description description__main">
<p>Sends a message that has already been created.  If there are problems with the content (e.g. no recipients or no PDF) and the message cannot be sent, this resource will return 403.</p>
<p>Sending messages through the API can only be paid for using already purchased credit.  If there is not enough credit the send will fail and return a 402 HTTP status code.</p>
</div>
							<div class="method-section">
								<h4>Path variables</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageID</div>
											<div class="schema-property__type">
												<span class="primitive">integer</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right"></div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="201">201</span>
												<span class="response-details__name">Created</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Message has been sent successfully.</p>
</div>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">MessageID</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="integer">integer</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>Docsmit Message ID</p>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">ClientMsgID</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>The ClientMsgID of the sent message</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="402">402</span>
												<span class="response-details__name">Payment Required</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Send failed due to inadequate funds.  The amount needed is returned in the JSON as <code>shortfall</code>.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="403">403</span>
												<span class="response-details__name">403
													<a class="small-link" href="#403">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>User does not have authority to perform this action on this message.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">400
													<a class="small-link" href="#400">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Problem with one of the request parameters.</p>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Ex - 1 - Send Message">
									<div class="description method-example__description">
<p>Sends the message for specified <code>messageID</code></p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">POST</span> http://secure.docsmit.com/api/v1/messages/1496/send HTTP/1.1</span> 

<span class="token response-status">HTTP/1.1 <span class="token property">201 Created </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
  messageID<span class="token operator">:</span> <span class="token number">2144</span><span class="token punctuation">,</span>
  messageID<span class="token operator">:</span> <span class="token string">"1496"</span><span class="token punctuation">,</span>
  clientMsgID<span class="token operator">:</span> <span class="token string">""</span>
<span class="token punctuation">}</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">402 Payment Required </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span> 
    <span class="token property">"message"</span><span class="token operator">:</span> <span class="token string">"Your current credit balance is $0.00 which is insufficient to send message with API. Please buy credits to send message using API."</span><span class="token punctuation">,</span>
    <span class="token property">"shortfall"</span><span class="token operator">:</span> <span class="token number">1.12</span>
    
<span class="token punctuation">}</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="add-party" class="spec-item-header" data-entity="method" data-entity-id="add-party">
								<a class="head_anchor" href="#add-party"></a>Add Party</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="POST">POST</span>
									<span class="method-url">/messages/<code>{messageID}</code>/party</span>
</div>
</div>
							<div class="description description__main">
<p>This API call allows you to add one or more recipients by providing them in the <a href="#physicalParties" class="link-internal link-schema" target="_self">physicalParties</a> data type.</p>
</div>
							<div class="method-section">
								<h4>Path variables</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageID</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right"></div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Request body</h4>
								<div class="request-body">
									<div class="schema">
										<div class="schema-property__definition">
											<div class="schema-property__left">
												<div class="schema__type">
													<span class="primitive">Object</span>
</div>
</div>
											<div class="schema-property__right"></div>
</div>
										<div class="schema-properties">
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">physicalParties</div>
														<div class="schema-property__type">
															<a class="item-ref" href="#physicalParties">physicalParties</a>
</div>
</div>
													<div class="schema-property__right">
														<div class="description">
<p>physicalParties is an array of MailAddresses, that is a list of mail recipients and how they are being sent the mail.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="201">201</span>
												<span class="response-details__name">Created</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>The party has been added.</p>
</div>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">PartyID</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">400
													<a class="small-link" href="#400">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Problem with one of the request parameters.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="403">403</span>
												<span class="response-details__name">403
													<a class="small-link" href="#403">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>User does not have authority to perform this action on this message.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="404">404</span>
												<span class="response-details__name">Not Found</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>MessageID or PartyID not found.</p>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name='Ex 1 - Add by "userIDs"'>
									<div class="description method-example__description">
<p>Comma deliminated userIDs to be added as message parties.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">POST</span> http://secure.docsmit.com/api/v1/messages/1200/party HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params = array(<span class="token string">"userIDs"</span> =&gt; <span class="token string">"472,473,474"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;post(<span class="token string">"/messages/1200/party"</span><span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">201 Created </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">5032</span><span class="token punctuation">,</span>
        <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token string">"472"</span><span class="token punctuation">,</span>
        <span class="token property">"name"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"company"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">5033</span><span class="token punctuation">,</span>
        <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token string">"473"</span><span class="token punctuation">,</span>
        <span class="token property">"name"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"company"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">5034</span><span class="token punctuation">,</span>
        <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token string">"474"</span><span class="token punctuation">,</span>
        <span class="token property">"name"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"company"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name='Ex 2 - Add by "email"'>
									<div class="description method-example__description">
<p>email address to be added as message party.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">POST</span> http://secure.docsmit.com/api/v1/messages/1200/party HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params = array(<span class="token string">"email"</span> =&gt; <span class="token string">"recipient11@docsmit.com"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;post(<span class="token string">"/messages/1200/party"</span><span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">201 Created </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">5015</span><span class="token punctuation">,</span>
        <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token number">467</span><span class="token punctuation">,</span>
        <span class="token property">"name"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"company"</span><span class="token operator">:</span> <span class="token null">null</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name='Ex 3 - Add by "recipients"'>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">POST</span> http://secure.docsmit.com/api/v1/messages/2333/party HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params = array(<span class="token string">"recipients"</span> =&gt; <span class="token string">"recipient5@gmail.com,recipient6@gmail.com,recipient7@gmail.com,recipient8@gmail.com"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;post(<span class="token string">"/messages/2333/party"</span><span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">201 Created </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">5021</span><span class="token punctuation">,</span>
        <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token number">472</span><span class="token punctuation">,</span>
        <span class="token property">"name"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"company"</span><span class="token operator">:</span> <span class="token null">null</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">5022</span><span class="token punctuation">,</span>
        <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token number">473</span><span class="token punctuation">,</span>
        <span class="token property">"name"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"company"</span><span class="token operator">:</span> <span class="token null">null</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">5023</span><span class="token punctuation">,</span>
        <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token number">474</span><span class="token punctuation">,</span>
        <span class="token property">"name"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"company"</span><span class="token operator">:</span> <span class="token null">null</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">5024</span><span class="token punctuation">,</span>
        <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token number">475</span><span class="token punctuation">,</span>
        <span class="token property">"name"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"company"</span><span class="token operator">:</span> <span class="token null">null</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name='Ex 4 - Add by "toList"'>
									<div class="description method-example__description">
<p>List of email address to be added as message parties. <code>email</code> and <code>isSender</code> will be added as parameter of list.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">POST</span> http://secure.docsmit.com/api/v1/messages/2333/party HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params = array(<span class="token string">"toList"</span> =&gt; array( array(<span class="token string">"email"</span>=&gt;<span class="token string">"recipient9@gmail.com"</span><span class="token punctuation">)</span><span class="token punctuation">,</span>array(<span class="token string">"email"</span>=&gt;<span class="token string">"recipient10@gmail.com"</span><span class="token punctuation">)</span><span class="token punctuation">,</span>array(<span class="token string">"email"</span>=&gt;<span class="token string">"recipient11@gmail.com"</span><span class="token punctuation">)</span><span class="token punctuation">,</span>array(<span class="token string">"email"</span>=&gt;<span class="token string">"sender12@gmail.com"</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;post(<span class="token string">"/messages/2333/party"</span><span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">201 Created </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">5025</span><span class="token punctuation">,</span>
        <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token number">476</span><span class="token punctuation">,</span>
        <span class="token property">"name"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"company"</span><span class="token operator">:</span> <span class="token null">null</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">5026</span><span class="token punctuation">,</span>
        <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token number">477</span><span class="token punctuation">,</span>
        <span class="token property">"name"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"company"</span><span class="token operator">:</span> <span class="token null">null</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">5027</span><span class="token punctuation">,</span>
        <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token number">478</span><span class="token punctuation">,</span>
        <span class="token property">"name"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"company"</span><span class="token operator">:</span> <span class="token null">null</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">5028</span><span class="token punctuation">,</span>
        <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token number">479</span><span class="token punctuation">,</span>
        <span class="token property">"name"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"company"</span><span class="token operator">:</span> <span class="token null">null</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name='Ex 5 - Add by "physicalParties"'>
									<div class="description method-example__description">
<p>List of physical parties to be added as mail parties of message.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">POST</span> http://secure.docsmit.com/api/v1/messages/2331/party HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params = array(<span class="token string">"physicalParties"</span> =&gt; array(
                    array(<span class="token string">"firstName"</span> =&gt; <span class="token string">"John"</span><span class="token punctuation">,</span>
				<span class="token string">"lastName"</span> =&gt; <span class="token string">"Adams"</span><span class="token punctuation">,</span>
				<span class="token string">"organization"</span> =&gt; <span class="token string">"Masaledara Consulting"</span><span class="token punctuation">,</span>
				<span class="token string">"address1"</span> =&gt; <span class="token string">"1 Pioneer Way"</span><span class="token punctuation">,</span>
				<span class="token string">"city"</span> =&gt; <span class="token string">"Las Vegas"</span><span class="token punctuation">,</span>
				<span class="token string">"state"</span> =&gt; <span class="token string">"NV"</span><span class="token punctuation">,</span>
				<span class="token string">"postalCode"</span> =&gt; <span class="token string">"12345-1234"</span><span class="token punctuation">,</span>
				<span class="token string">"envelope"</span> =&gt; <span class="token string">"#10 Double Window"</span>
				<span class="token punctuation">)</span><span class="token punctuation">,</span>
                    array(<span class="token string">"firstName"</span> =&gt; <span class="token string">"Stan"</span><span class="token punctuation">,</span>
				<span class="token string">"lastName"</span> =&gt; <span class="token string">"Sitwell"</span><span class="token punctuation">,</span>
				<span class="token string">"organization"</span> =&gt; <span class="token string">"Sitwell &amp; Sitwell, LLP"</span><span class="token punctuation">,</span>
				<span class="token string">"address1"</span> =&gt; <span class="token string">"1250 Avenue of the Americas"</span><span class="token punctuation">,</span>
				<span class="token string">"address2"</span> =&gt; <span class="token string">"Floor 22"</span><span class="token punctuation">,</span>
				<span class="token string">"city"</span> =&gt; <span class="token string">"New York"</span><span class="token punctuation">,</span>
				<span class="token string">"state"</span> =&gt; <span class="token string">"NY"</span><span class="token punctuation">,</span>
				<span class="token string">"postalCode"</span> =&gt; <span class="token string">"10021"</span><span class="token punctuation">,</span>
				<span class="token string">"envelope"</span> =&gt; <span class="token string">"#10 Double Window"</span><span class="token punctuation">,</span>
				<span class="token string">"SASESheets"</span>=&gt;<span class="token number">5</span><span class="token punctuation">,</span>
				<span class="token punctuation">)</span><span class="token punctuation">,</span>
                    array(<span class="token string">"firstName"</span> =&gt; <span class="token string">"Herbert"</span><span class="token punctuation">,</span>
				<span class="token string">"lastName"</span> =&gt; <span class="token string">"Hoover"</span><span class="token punctuation">,</span>
				<span class="token string">"organization"</span> =&gt; <span class="token string">"Seven Presidents Consulting"</span><span class="token punctuation">,</span>
				<span class="token string">"address1"</span> =&gt; <span class="token string">"1141 Ocean Ave"</span><span class="token punctuation">,</span>
				<span class="token string">"city"</span> =&gt; <span class="token string">"Long Branch"</span><span class="token punctuation">,</span>
				<span class="token string">"state"</span> =&gt; <span class="token string">"NJ"</span><span class="token punctuation">,</span>
				<span class="token string">"postalCode"</span> =&gt; <span class="token string">"07064"</span><span class="token punctuation">,</span>
				<span class="token string">"envelope"</span> =&gt; <span class="token string">"#10 Double Window"</span><span class="token punctuation">,</span>
				<span class="token string">"SASESheets"</span>=&gt;<span class="token number">3</span><span class="token punctuation">,</span>
				<span class="token punctuation">)</span><span class="token punctuation">,</span>
                    <span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;post(<span class="token string">"/messages/2331/party"</span><span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">201 Created </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">5035</span><span class="token punctuation">,</span>
        <span class="token property">"mailAddressID"</span><span class="token operator">:</span> <span class="token number">96</span><span class="token punctuation">,</span>
        <span class="token property">"name"</span><span class="token operator">:</span> <span class="token string">"masaledara consulting"</span><span class="token punctuation">,</span>
        <span class="token property">"company"</span><span class="token operator">:</span> <span class="token string">"masaledara consulting"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">5036</span><span class="token punctuation">,</span>
        <span class="token property">"mailAddressID"</span><span class="token operator">:</span> <span class="token number">97</span><span class="token punctuation">,</span>
        <span class="token property">"name"</span><span class="token operator">:</span> <span class="token string">"masaledara22222 consulting22222222222222222"</span><span class="token punctuation">,</span>
        <span class="token property">"company"</span><span class="token operator">:</span> <span class="token string">"masaledara consulting22"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">5037</span><span class="token punctuation">,</span>
        <span class="token property">"mailAddressID"</span><span class="token operator">:</span> <span class="token number">98</span><span class="token punctuation">,</span>
        <span class="token property">"name"</span><span class="token operator">:</span> <span class="token string">"masaledara23333 consulting33"</span><span class="token punctuation">,</span>
        <span class="token property">"company"</span><span class="token operator">:</span> <span class="token string">"masaledara consulting33"</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="delete-party" class="spec-item-header" data-entity="method" data-entity-id="delete-party">
								<a class="head_anchor" href="#delete-party"></a>Delete Party</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="DELETE">DELETE</span>
									<span class="method-url">/messages/<code>{messageID}</code>/party/<code>{partyID}</code></span>
</div>
</div>
							<div class="description description__main">
<p>Removes a party from a message.  Can only be performed on messages that have not been sent yet and cannot be performed on the message owner.</p>
</div>
							<div class="method-section">
								<h4>Path variables</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageID</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right">
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">1455</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">partyID</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right">
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">6534</span></div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-12">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">partyID</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token number">4370</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">success</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Ex 1 - Delete Party">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">DELETE</span> http://secure.docsmit.com/api/v1/messages/1455/party/6534 HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;delete(<span class="token string">"/messages/1455/party/6534"</span><span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">6534</span><span class="token punctuation">,</span>
    <span class="token property">"success"</span><span class="token operator">:</span> <span class="token boolean">true</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="update-party" class="spec-item-header" data-entity="method" data-entity-id="update-party">
								<a class="head_anchor" href="#update-party"></a>Update Party</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="PUT">PUT</span>
									<span class="method-url">/messages/<code>{messageID}</code>/party/<code>{partyID}</code></span>
</div>
</div>
							<div class="description description__main">
<p>Updates one or more of the following fields in a party:
firstName, lastName, organization, address1, address2, address3, city, state, postalCode, countryNonUS</p>
</div>
							<div class="method-section">
								<h4>Path variables</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageID</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right">
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">1234</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">partyID</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right"></div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Request parameters</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">firstName</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Updates <code>firstName</code> field.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">Michael</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">lastName</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Updates <code>lastName</code>field.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">Johnson</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">organization</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Updates <code>organization</code> field.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">Johnson On the Spot, Inc.</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">address1</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Updates <code>address1</code> field.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">135 Bergen Ave</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">address2</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Updates <code>address2</code> field.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">Suite 101</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">city</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Updates <code>city</code> field.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">Jersey City</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">state</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Updates <code>state</code> field.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">NJ</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">postalCode</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Updates <code>postalCode</code> field</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">07305</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">countryNonUS</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>(DON'T USE - we only send within the US) Updates <code>countryNonUS</code> field</p>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">SASESheets</div>
											<div class="schema-property__type">
												<span class="primitive">integer</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>10</p>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-12">
												<span class="response-details__status" data-response-status="202">202</span>
												<span class="response-details__name">Accepted</span>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Ex 1 - Update mail party">
									<div class="description method-example__description">
<p>Updates mail party address</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">PUT</span> http://secure.docsmit.com/api/v1/messages/2275/party/4760 HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$body = array(<span class="token string">"firstName"</span> =&gt; <span class="token string">"Albert"</span><span class="token punctuation">,</span>
<span class="token string">"lastName"</span> =&gt;<span class="token string">"Einstein"</span><span class="token punctuation">,</span>
<span class="token string">"organization"</span> =&gt;<span class="token string">"masaledara consulting4"</span><span class="token punctuation">,</span>
<span class="token string">"address1"</span> =&gt;<span class="token string">"this is adres 18"</span><span class="token punctuation">,</span>
<span class="token string">"city"</span> =&gt;<span class="token string">"vegas5"</span><span class="token punctuation">,</span>
<span class="token string">"state"</span> =&gt;<span class="token string">"la"</span><span class="token punctuation">,</span>
<span class="token string">"postalCode"</span> =&gt;<span class="token string">"12345-1234"</span><span class="token punctuation">,</span>
<span class="token string">"endorsement"</span> =&gt;<span class="token string">"i am hiren"</span><span class="token punctuation">,</span>
<span class="token string">"SASESheets"</span>=&gt;<span class="token number">5</span>
<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;put('/messages/<span class="token number">2275</span>/party/<span class="token number">4760</span>'<span class="token punctuation">,</span> $body<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">202 Accepted </span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="get-the-price-and-the-details" class="spec-item-header" data-entity="method" data-entity-id="get-the-price-and-the-details">
								<a class="head_anchor" href="#get-the-price-and-the-details"></a>Get the price and the details</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="GET">GET</span>
									<span class="method-url">/messages/<code>{messageID}</code>/priceCheck</span>
</div>
</div>
							<div class="method-section">
								<h4>Path variables</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageID</div>
											<div class="schema-property__type">
												<span class="primitive">integer</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right"></div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Request parameters</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">certify</div>
											<div class="schema-property__type">
												<span class="primitive">boolean</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right"></div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">months</div>
											<div class="schema-property__type">
												<span class="primitive">integer</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Set Months for 1,3 &amp; 7 Year</p>
</div>
											<div class="view-param-attr">
												<span class="lbl">Enumeration:</span>
												<div class="schema-property-enum">
													<div class="row view-row schema-property-enum__value">
														<div class="col-md-5" style="padding-left:0px;padding-right:0px">
															<span class="val">12</span>
</div>
														<div class="col-md-7 description"></div>
</div>
													<div class="row view-row schema-property-enum__value">
														<div class="col-md-5" style="padding-left:0px;padding-right:0px">
															<span class="val">36</span>
</div>
														<div class="col-md-7 description"></div>
</div>
													<div class="row view-row schema-property-enum__value">
														<div class="col-md-5" style="padding-left:0px;padding-right:0px">
															<span class="val">84</span>
</div>
														<div class="col-md-7 description"></div>
</div>
</div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">sendType</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right">
											<div class="view-param-attr">
												<span class="lbl">Enumeration:</span>
												<div class="schema-property-enum">
													<div class="row view-row schema-property-enum__value">
														<div class="col-md-5" style="padding-left:0px;padding-right:0px">
															<span class="val">SR</span>
</div>
														<div class="col-md-7 description"></div>
</div>
													<div class="row view-row schema-property-enum__value">
														<div class="col-md-5" style="padding-left:0px;padding-right:0px">
															<span class="val">NSR</span>
</div>
														<div class="col-md-7 description"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Returns information about netPrice and detail.</p>
</div>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">netPrice</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">details</div>
																	<div class="schema-property__type">
																		<span class="primitive">Object</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
															<div class="schema-properties">
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">total_base</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">$ 2.49</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">total_mb</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">0.00</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">total</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">$ 2.49</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">actualMB</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">0 MB</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">chargedMB</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">0 MB</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">discountMB</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">0 MB</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">certification</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">Pre-Certification</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">cert_base</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">$ 1.99</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">certMB</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">0.00</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">extension</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">Extend 12 months</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">ext_base</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">$ 0.50</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">extMB</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">0.00</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">expiration_after</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">8/28/17</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">discount</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">$ (2.49)</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">netPrice</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">$ 0.00</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">Bad Request</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Invalid MessageID,certify,months,sendType parameters.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Ex 1">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/1234/priceCheck?certify=false&amp;months=12&amp;sendType=SR HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span><span class="token string">"http://secure.docsmit.com/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$params = array(<span class="token string">"certify"</span> =&gt; <span class="token number">1</span><span class="token punctuation">,</span> <span class="token string">"months"</span> =&gt; <span class="token number">12</span><span class="token punctuation">,</span> <span class="token string">"sendType"</span> =&gt; <span class="token string">"SR"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;get(<span class="token string">"/messages/1234/priceCheck"</span><span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"netPrice"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"details"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
        <span class="token property">"total_base"</span><span class="token operator">:</span> <span class="token string">"$ 2.49"</span><span class="token punctuation">,</span>
        <span class="token property">"total_mb"</span><span class="token operator">:</span> <span class="token string">"0.00"</span><span class="token punctuation">,</span>
        <span class="token property">"total"</span><span class="token operator">:</span> <span class="token string">"$ 2.49"</span><span class="token punctuation">,</span>
        <span class="token property">"actualMB"</span><span class="token operator">:</span> <span class="token string">"0 MB"</span><span class="token punctuation">,</span>
        <span class="token property">"chargedMB"</span><span class="token operator">:</span> <span class="token string">"0 MB"</span><span class="token punctuation">,</span>
        <span class="token property">"discountMB"</span><span class="token operator">:</span> <span class="token string">"0 MB"</span><span class="token punctuation">,</span>
        <span class="token property">"certification"</span><span class="token operator">:</span> <span class="token string">"Pre-Certification"</span><span class="token punctuation">,</span>
        <span class="token property">"cert_base"</span><span class="token operator">:</span> <span class="token string">"$ 1.99"</span><span class="token punctuation">,</span>
        <span class="token property">"certMB"</span><span class="token operator">:</span> <span class="token string">"0.00"</span><span class="token punctuation">,</span>
        <span class="token property">"extension"</span><span class="token operator">:</span> <span class="token string">"Extend 12 months"</span><span class="token punctuation">,</span>
        <span class="token property">"ext_base"</span><span class="token operator">:</span> <span class="token string">"$ 0.50"</span><span class="token punctuation">,</span>
        <span class="token property">"extMB"</span><span class="token operator">:</span> <span class="token string">"0.00"</span><span class="token punctuation">,</span>
        <span class="token property">"expiration_after"</span><span class="token operator">:</span> <span class="token string">"8/28/17"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token string">"$ (2.49)"</span><span class="token punctuation">,</span>
        <span class="token property">"netPrice"</span><span class="token operator">:</span> <span class="token string">"$ 0.00"</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="messages-after-sending_group" class="spec-item-header" data-entity="group" data-entity-id="messages-after-sending_group">
								<a class="head_anchor" href="#messages-after-sending_group"></a>Messages - After Sending</div>
							<div class="description description__main">
<p>Resources for getting data about a message or setting some data after it was sent.</p>
</div>
							<div class="method-section">
								<div class="group-methods-summary">
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#get-a-message">Get a message</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="GET">GET</span>
											<span class="method-url">/messages/<code>{messageID}</code></span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#sign-for-message">Sign for message</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="PUT">PUT</span>
											<span class="method-url">/messages/<code>{messageID}</code>/signFor</span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#get-certification">Get Certification</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="GET">GET</span>
											<span class="method-url">/messages/<code>{messageID}</code>/documentation</span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#get-recipients-and-their-statuses-parties">Get Recipients and their statuses - parties</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="GET">GET</span>
											<span class="method-url">/messages/<code>{messageId}</code>/parties</span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#get-attachments-list">Get attachments list</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="GET">GET</span>
											<span class="method-url">/messages/<code>{messageID}</code>/attachments</span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#message-transaction-history">Message transaction history</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="GET">GET</span>
											<span class="method-url">/messages/<code>{messageID}</code>/transactions</span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#download-file-attachment">Download file attachment</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="GET">GET</span>
											<span class="method-url">/messages/<code>{messageId}</code>/download/<code>{fileId}</code></span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#download-zip-attachment">Download Zip attachment</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="GET">GET</span>
											<span class="method-url">/messages/<code>{messageID}</code>/download/<code>{ZipID}</code></span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#generate-tracking-id">Generate tracking ID (DONT USE RIGHT NOW)</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="POST">POST</span>
											<span class="method-url">/messages/<code>{messageID}</code>/trackingID</span>
</div>
</div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="get-a-message" class="spec-item-header" data-entity="method" data-entity-id="get-a-message">
								<a class="head_anchor" href="#get-a-message"></a>Get a message</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="GET">GET</span>
									<span class="method-url">/messages/<code>{messageID}</code></span>
</div>
</div>
							<div class="description description__main">
<p>Returns information about the message including recipients, its contents (or pointers to contents for some parts) and its status.</p>
</div>
							<div class="method-section">
								<h4>Path variables</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageID</div>
											<div class="schema-property__type">
												<span class="primitive">integer</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right"></div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Returns message body, title, list of attachments, recipients (with statuses), sent Status/time</p>
</div>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">id</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="integer">integer</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>MessageID</p>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">from</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>The sender of the Message</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">shc.narola@narolainfotech.com</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">title</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>Message Title</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">Test Message :: 2879</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">to</div>
																	<div class="schema-property__type">
																		<span class="primitive">Object</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
															<div class="schema-properties">
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">email</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>Recipient Email</p>
</div>
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">ps.narola@narolainfotech.com</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">unread</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>If true then message is unread by the Recipient.</p>
</div>
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">SignedFor</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>The Message signedFor Date.</p>
</div>
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">2015-09-18 06:33:31</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">attachments</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>If true, Message has atatchments.</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">threadRoot</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="integer">integer</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">certified</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>If true, Message is certified.</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">attSize</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="integer">integer</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>Message Attachment size.</p>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">sent</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
																		<span class="schema__format">date</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">2015-09-18 05:49:02</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">transaction</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>Message Transaction status with p,c,f,a</p>
</div>
																	<div class="view-param-attr">
																		<span class="lbl">Enumeration:</span>
																		<div class="schema-property-enum">
																			<div class="row view-row schema-property-enum__value">
																				<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																					<span class="val">aborted : a</span>
</div>
																				<div class="col-md-7 description"></div>
</div>
</div>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">Paypal Completed : c</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">bookmarked</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>If true, Message is bookmarked.</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">isFlagged</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>If true then,Message is Flagged</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">isDeliverAfter</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>If true, Message is set as DeliverAfter</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">isSent</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>If true, Message is sent.</p>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">isBlastMessage</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>If true, recipients can't see other recipients information.</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">totalRecipientsCount</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="integer">integer</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>Total Recipients of the Message.</p>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">totalUnsignedCount</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="integer">integer</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>Total Unsigned Message count.</p>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">isEncrypted</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>If True, Message is encrypted.</p>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">alreadySentType</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>Returns Message Last Sent Type</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">SR</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">SRSent</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
																		<span class="schema__format">date</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>If Message sent as SR then returns SR sent DateTime</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">2015-09-18 05:49:02</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">NSRSent</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
																		<span class="schema__format">date</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>If Message sent as NSR then returns NSR sent DateTime</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">0000-00-00 00:00:00</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">SRDeliverAfter</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>DateTime of SRDeliverAfter.</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">0000-00-00 00:00:00</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">NSRDeliverAfter</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
																		<span class="schema__format">date</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>DateTime of NSRDeliverAfter.</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Default:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">0000-00-00 00:00:00</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">taglist</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>Messages Tag List</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">tag1,tag2</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">AsOf</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
																		<span class="schema__format">date</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>Current Time in UTC.</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">2015-09-23 05:16:20</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">isBounced</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>If true, Message is bounced.</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">bouncedMsgCount</div>
																	<div class="schema-property__type">
																		<span class="primitive">Object</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
															<div class="schema-properties">
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">total</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="integer">integer</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>Total Bounced parties count.</p>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">initialBounce</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="integer">integer</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>Total initialbounced parties count.</p>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">reminderBounce</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="integer">integer</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>Total Reminder Bounced parties count.</p>
</div>
</div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">parties</div>
																	<div class="schema-property__type">
																		<span class="primitive">Object</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
															<div class="schema-properties">
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">id</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="integer">integer</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>Party ID</p>
</div>
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token number">11438</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">userID</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="integer">integer</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>Party UserId for the Message.</p>
</div>
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token number">321</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">email</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>Party email Address</p>
</div>
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">shc.narola@narolainfotech.com</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">name</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>Name of the Party.</p>
</div>
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">shcnarola</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">company</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>Company Name of the company.</p>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">isSender</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>If true, party is sender otherwise user is message Recipients.</p>
</div>
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">isOwner</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>If true, Party is owner of the Message.</p>
</div>
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">canEdit</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>If true, Party can edit the Message.</p>
</div>
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">unclaimedNoticeTimeout</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
																				<span class="schema__format">date</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>Message Timeout DateTime.</p>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">views</div>
																			<div class="schema-property__type">
																				<span class="primitive">Object</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
																	<div class="schema-properties">
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">totalViews</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="integer">integer</span>
</div>
</div>
																				<div class="schema-property__right">
																					<div class="description">
<p>Total count of the Message view.</p>
</div>
</div>
</div>
</div>
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">firstView</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="string">string</span>
																						<span class="schema__format">date</span>
</div>
</div>
																				<div class="schema-property__right">
																					<div class="description">
<p>Message Firstview DateTime.</p>
</div>
																					<div class="schema-property-examples">
																						<span class="lbl">Example:</span>
																						<div class="schema-property-examples__list"><span class="example-primitive token string">2015-01-01 13:00:00</span></div>
</div>
</div>
</div>
</div>
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">lastView</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="string">string</span>
																						<span class="schema__format">date</span>
</div>
</div>
																				<div class="schema-property__right">
																					<div class="description">
<p>Message LastView DateTime</p>
</div>
																					<div class="schema-property-examples">
																						<span class="lbl">Example:</span>
																						<div class="schema-property-examples__list"><span class="example-primitive token string">2015-01-01 13:00:00</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">isSignedFor</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>If true, Message is SignedFor</p>
</div>
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">signedFor</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
																				<span class="schema__format">date</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>DateTime of the Message signedFor.</p>
</div>
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">2015-01-01 13:00:00</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">reminders</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>Message Reminder dates.</p>
</div>
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">2015-09-18 07:50:29</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">DELsmtpResponse</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>SMTP Response of the message delivery.</p>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">DELtimestamp</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>Timestamp of the Message delivery.</p>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">DLs</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>If message has Zip then returns Total Zip download count.</p>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">addedAfter</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>Party is added After Message sent.</p>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">addedBy</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>The emailaddress of the party addedBy</p>
</div>
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">shc.narola@narolainfotech.com</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">sent</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
																				<span class="schema__format">date</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>Date of the MessageSent.</p>
</div>
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">2015-09-18 05:49:02</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">bounced</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>If true, Message is bounced.</p>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">isInitialBounce</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>If true, Party is initial bounced.</p>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">isReminderBounce</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>If true, Party is Reminder bounced.</p>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">bounceTimestamp</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
																				<span class="schema__format">date</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>The dateTime of the Bounced Timestamp.</p>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">bounceDiagnosticCode</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>The Diagnostic code for the bounced message.</p>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">bounceAction</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>Action for the Bounced message.</p>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">canRemind</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>if true, Party can remind.</p>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">nextRemind</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
																				<span class="schema__format">date</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="description">
<p>The dateTime of the Next Reminder.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="403">403</span>
												<span class="response-details__name">403
													<a class="small-link" href="#403">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>User does not have authority to perform this action on this message.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">400
													<a class="small-link" href="#400">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Problem with one of the request parameters.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="204">204</span>
												<span class="response-details__name">204</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Content not found.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Ex 1 - Get a message">
									<div class="description method-example__description">
<p>Get a message details by specified <code>messageID</code></p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/2879 HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/<span class="token number">2879</span>'<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">2879</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"Test Message :: 2879"</span><span class="token punctuation">,</span>
    <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"ps.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 06:33:31"</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"ay.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
            <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token string">""</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">2879</span><span class="token punctuation">,</span>
    <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 05:49:02"</span><span class="token punctuation">,</span>
    <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
    <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"isDeliverAfter"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"isSent"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isBlastMessage"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"totalRecipientsCount"</span><span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span>
    <span class="token property">"totalUnsignedCount"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
    <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 05:49:02"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"SRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"taglist"</span><span class="token operator">:</span> <span class="token string">"tag1,tag2"</span><span class="token punctuation">,</span>
    <span class="token property">"AsOf"</span><span class="token operator">:</span> <span class="token string">"2015-09-23 05:16:20"</span><span class="token punctuation">,</span>
    <span class="token property">"isBounced"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
    <span class="token property">"bouncedMsgCount"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
        <span class="token property">"total"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"initialBounce"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"reminderBounce"</span><span class="token operator">:</span> <span class="token number">0</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"parties"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">11438</span><span class="token punctuation">,</span>
            <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token number">321</span><span class="token punctuation">,</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
            <span class="token property">"name"</span><span class="token operator">:</span> <span class="token string">"shcnarola"</span><span class="token punctuation">,</span>
            <span class="token property">"company"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"isSender"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
            <span class="token property">"isOwner"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
            <span class="token property">"canEdit"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
            <span class="token property">"unclaimedNoticeTimeout"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"views"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
                <span class="token property">"totalViews"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
                <span class="token property">"firstView"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
                <span class="token property">"lastView"</span><span class="token operator">:</span> <span class="token string">""</span>
            <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token property">"isSignedFor"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"signedFor"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"reminders"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
            <span class="token property">"DELsmtpResponse"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
            <span class="token property">"DELtimestamp"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"DLs"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"addedAfter"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"addedBy"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
            <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 05:49:02"</span><span class="token punctuation">,</span>
            <span class="token property">"bounced"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"isInitialBounce"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"isReminderBounce"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"bounceTimestamp"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"bounceDiagnosticCode"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"bounceAction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"canRemind"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"nextRemind"</span><span class="token operator">:</span> <span class="token string">""</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span>
            <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">11439</span><span class="token punctuation">,</span>
            <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token number">339</span><span class="token punctuation">,</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"ps.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
            <span class="token property">"name"</span><span class="token operator">:</span> <span class="token string">"Narola"</span><span class="token punctuation">,</span>
            <span class="token property">"company"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"isSender"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"isOwner"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"canEdit"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"unclaimedNoticeTimeout"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"views"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
                <span class="token property">"totalViews"</span><span class="token operator">:</span> <span class="token number">6</span><span class="token punctuation">,</span>
                <span class="token property">"firstView"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 06:33:31"</span><span class="token punctuation">,</span>
                <span class="token property">"lastView"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 07:13:45"</span>
            <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token property">"isSignedFor"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
            <span class="token property">"signedFor"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 06:33:31"</span><span class="token punctuation">,</span>
            <span class="token property">"reminders"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
            <span class="token property">"DELsmtpResponse"</span><span class="token operator">:</span> <span class="token string">"250 Requested mail action okay, completed: partyID=0Lp694-1Z0BwJ2YwG-00exZY"</span><span class="token punctuation">,</span>
            <span class="token property">"DELtimestamp"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 05:49:04"</span><span class="token punctuation">,</span>
            <span class="token property">"DLs"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"addedAfter"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"addedBy"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
            <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 05:49:02"</span><span class="token punctuation">,</span>
            <span class="token property">"bounced"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"isInitialBounce"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"isReminderBounce"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"bounceTimestamp"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"bounceDiagnosticCode"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"bounceAction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"canRemind"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
            <span class="token property">"nextRemind"</span><span class="token operator">:</span> <span class="token string">""</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span>
            <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">11440</span><span class="token punctuation">,</span>
            <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token number">670</span><span class="token punctuation">,</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"ay.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
            <span class="token property">"name"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"company"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"isSender"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"isOwner"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"canEdit"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"unclaimedNoticeTimeout"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"views"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
                <span class="token property">"totalViews"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
                <span class="token property">"firstView"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
                <span class="token property">"lastView"</span><span class="token operator">:</span> <span class="token string">""</span>
            <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token property">"isSignedFor"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"signedFor"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"reminders"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"2015-09-18 07:50:29"</span><span class="token punctuation">,</span>
                <span class="token string">"2015-09-23 05:16:13"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"DELsmtpResponse"</span><span class="token operator">:</span> <span class="token string">"250 Requested mail action okay, completed: partyID=0MgvFm-1ZGtwd3Nw0-00M3Aq"</span><span class="token punctuation">,</span>
            <span class="token property">"DELtimestamp"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 05:49:04"</span><span class="token punctuation">,</span>
            <span class="token property">"DLs"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"addedAfter"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"addedBy"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
            <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 05:49:02"</span><span class="token punctuation">,</span>
            <span class="token property">"bounced"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"isInitialBounce"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"isReminderBounce"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"bounceTimestamp"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"bounceDiagnosticCode"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"bounceAction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"canRemind"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token property">"nextRemind"</span><span class="token operator">:</span> <span class="token string">"2015-09-24 00:16:13"</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span>
<span class="token punctuation">}</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">403 Forbidden </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    code<span class="token operator">:</span> <span class="token number">403</span><span class="token punctuation">,</span>
    message<span class="token operator">:</span> <span class="token string">"User does not have authority to perform this action on this message."</span>
<span class="token punctuation">}</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">400 Bad Request </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    code<span class="token operator">:</span> <span class="token number">400</span><span class="token punctuation">,</span>
    message<span class="token operator">:</span> <span class="token string">"Problem with one of the request parameters."</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="sign-for-message" class="spec-item-header" data-entity="method" data-entity-id="sign-for-message">
								<a class="head_anchor" href="#sign-for-message"></a>Sign for message</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="PUT">PUT</span>
									<span class="method-url">/messages/<code>{messageID}</code>/signFor</span>
</div>
</div>
							<div class="description description__main">
<p>Sign for message.<code>pw</code> parameter is required to sign for the message.</p>
</div>
							<div class="method-section">
								<h4>Path variables</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageID</div>
											<div class="schema-property__type">
												<span class="primitive">integer</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right"></div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Request parameters</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">pw</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>hashed password.</p>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">signer_conf_mail</div>
											<div class="schema-property__type">
												<span class="primitive">boolean</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p><code>1</code> or <code>0</code>. Send an email to signer letting him know of the first view, if he prefer to receive while sign in.</p>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">exclude_message_body</div>
											<div class="schema-property__type">
												<span class="primitive">boolean</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p><code>1</code> or <code>0</code>. Private (Message excluded from email receipt)</p>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Request body</h4>
								<div class="request-body">
									<div class="schema">
										<div class="schema-property__definition">
											<div class="schema-property__left">
												<div class="schema__type">
													<span class="primitive">Object</span>
</div>
</div>
											<div class="schema-property__right"></div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-12">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="406">406</span>
												<span class="response-details__name">Not Acceptable</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Hashed password did not match.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">Bad Request</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Message is already signed for.</p>
</div>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">code</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token number">400</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">message</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">This message is already signed for.</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">Missing Parameters</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Parameter(s) is missing.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="403">403</span>
												<span class="response-details__name">Invalid Inputs</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Invalid input parameters</p>
</div>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">code</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token number">403</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">message</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">Invalid value supplied for `exclude_message_body` parameter. valid inputs for exclude_message_body params are true, false, 1, 0</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Ex - 1 Sign for message">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">PUT</span> http://secure.docsmit.com/api/v1/messages/2044/signFor?pw=787897898df78d8df7d8f8d7f8df78df8f7dsdfsdf7sd7f8sd87f8sd7f8 HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span><span class="token string">"http://secure.docsmit.com/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"pw"</span><span class="token punctuation">]</span> = <span class="token string">"787897898df78d8df7d8f8d7f8df78df8f7dsdfsdf7sd7f8sd87f8sd7f8"</span><span class="token punctuation">;</span>
$docsmit-&gt;put('/messages/<span class="token number">2044</span>/signFor'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span> <span class="token property">"message"</span><span class="token operator">:</span> <span class="token string">"Message signed for successfully."</span><span class="token punctuation">,</span> <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">953</span> <span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex - 2 Sign for message - missing params">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">PUT</span> http://secure.docsmit.com/api/v1/messages/953/signFor HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span><span class="token string">"http://secure.docsmit.com/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;put('/messages/<span class="token number">2044</span>/signFor'<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">400 Bad Request </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span> <span class="token property">"code"</span><span class="token operator">:</span> <span class="token number">400</span><span class="token punctuation">,</span> <span class="token property">"message"</span><span class="token operator">:</span> <span class="token string">"Problem with one of the request parameters. Invalid parameters to pw signature"</span> <span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex - 3 Sign for message - Signer confirmation mail">
									<div class="description method-example__description">
<p>It will send sign for confirmation mail to the sender.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">PUT</span> http://secure.docsmit.com/api/v1/messages/1928/signFor?pw=789878sd789fsd7f89s98f98s&amp;signer_conf_mail=false HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span><span class="token string">"http://secure.docsmit.com/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"pw"</span><span class="token punctuation">]</span> = $hashedPW<span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"signer_conf_mail"</span><span class="token punctuation">]</span> = <span class="token number">1</span><span class="token punctuation">;</span>
$docsmit-&gt;put('/messages/<span class="token number">2044</span>/signFor'<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span> <span class="token property">"message"</span><span class="token operator">:</span> <span class="token string">"Message signed for successfully."</span><span class="token punctuation">,</span> <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1928</span> <span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="EX - 4 Sign for message - Exclude message body">
									<div class="description method-example__description">
<p>It will send sign for mail to the sender but body will not send in mail. it will keep private.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">PUT</span> http://secure.docsmit.com/api/v1/messages/954/signFor?signer_conf_mail=false&amp;exclude_message_body=false&amp;pw=7778sdfs8s7d7898sd8f789s7f8f8sf8f78f8f HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span><span class="token string">"http://secure.docsmit.com/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"pw"</span><span class="token punctuation">]</span> = $hashedPW<span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"signer_conf_mail"</span><span class="token punctuation">]</span> = <span class="token number">1</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"exclude_message_body"</span><span class="token punctuation">]</span> = <span class="token number">1</span><span class="token punctuation">;</span>
$docsmit-&gt;put('/messages/<span class="token number">2044</span>/signFor'<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span> <span class="token property">"message"</span><span class="token operator">:</span> <span class="token string">"Message signed for successfully."</span><span class="token punctuation">,</span> <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">954</span> <span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex - 5 Sign for message - invalid inputs">
									<div class="description method-example__description">
<p>Sign for message invlaid inputs</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">PUT</span> http://secure.docsmit.com/api/v1/messages/1478/signFor?pw=7878df878df78df7d8f7d87f8df8df9f8&amp;signer_conf_mail=false&amp;exclude_message_body=false HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span> SECUREURL . <span class="token string">"/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"pw"</span><span class="token punctuation">]</span> = $hashedPW<span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"signer_conf_mail"</span><span class="token punctuation">]</span> = '<span class="token boolean">true</span>'<span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"exclude_message_body"</span><span class="token punctuation">]</span> = 'falsesdfsf'<span class="token punctuation">;</span> // user can pass invalid value other than <span class="token boolean">true</span><span class="token punctuation">,</span><span class="token boolean">false</span><span class="token punctuation">,</span><span class="token number">1</span><span class="token punctuation">,</span>o
$docsmit-&gt;put('/messages/<span class="token number">1953</span>/signFor'<span class="token punctuation">,</span>$params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;responseBody(<span class="token punctuation">)</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">403 Forbidden </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"code"</span><span class="token operator">:</span> <span class="token number">403</span><span class="token punctuation">,</span>
    <span class="token property">"message"</span><span class="token operator">:</span> <span class="token string">"Invalid value supplied for `exclude_message_body` parameter. valid inputs for exclude_message_body params are true, false, 1, 0"</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="get-certification" class="spec-item-header" data-entity="method" data-entity-id="get-certification">
								<a class="head_anchor" href="#get-certification"></a>Get Certification</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="GET">GET</span>
									<span class="method-url">/messages/<code>{messageID}</code>/documentation</span>
</div>
</div>
							<div class="description description__main">
<p>A Certification is a PDF containing Docsmit's official statement of detailed information about a message.  It has information about the order placed and the production and delivery of the message's mailpieces.  It also contains a copy of the mailpiece's content </p>
<p>The first page of the PDF states the addresses for each of the recipients, how it was being shipped to them (including any USPS tracking numbers), the date and time of the order, mailing and delivery.  The next pages are the actual PDF that was mailed in case there is any argument raised by the recipient about the contents of the envelope.  They are followed by any USPS generated Proof of Delivery forms for any certified mail (return receipt electronic).</p>
<p>NOTE: the response is not JSON.  The response is the actual PDF file.</p>
</div>
							<div class="method-section">
								<h4>Path variables</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageID</div>
											<div class="schema-property__type">
												<span class="primitive">integer</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Message Id</p>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>If document served as PDF</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">Not Found</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>MessageIDNot Found.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="403">403</span>
												<span class="response-details__name">403
													<a class="small-link" href="#403">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>User does not have authority to perform this action on this message.</p>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Ex 1: Fetch &amp; Serve Cert">
									<div class="description method-example__description">
<p>Fetch &amp; Serve Certification PDF.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/1896/documentation HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/<span class="token number">1896</span>/documentation'<span class="token punctuation">,</span> <span class="token null">null</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
if($docsmit-&gt;status(<span class="token punctuation">)</span> == <span class="token number">200</span><span class="token punctuation">)</span>
<span class="token punctuation">{</span>
    header('Cache-Control<span class="token operator">:</span> public'<span class="token punctuation">)</span><span class="token punctuation">;</span>
    header('Content-type<span class="token operator">:</span> application/pdf'<span class="token punctuation">)</span><span class="token punctuation">;</span>
    header('Content-Disposition<span class="token operator">:</span> attachment<span class="token punctuation">;</span> filename=<span class="token string">"Certifcation-1896.pdf"</span>'<span class="token punctuation">)</span><span class="token punctuation">;</span>                
<span class="token punctuation">}</span>
echo $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex 2 - Serve document - Unauthorized">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/2045/documentation HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span>SECUREURL . <span class="token string">"/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/<span class="token number">2045</span>/documentation'<span class="token punctuation">,</span> <span class="token null">null</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
if($docsmit-&gt;status(<span class="token punctuation">)</span> == <span class="token number">200</span><span class="token punctuation">)</span>
<span class="token punctuation">{</span>
    header('Cache-Control<span class="token operator">:</span> public'<span class="token punctuation">)</span><span class="token punctuation">;</span>
    header('Content-type<span class="token operator">:</span> application/pdf'<span class="token punctuation">)</span><span class="token punctuation">;</span>
    header('Content-Disposition<span class="token operator">:</span> attachment<span class="token punctuation">;</span> filename=<span class="token string">"documentation-2045.pdf"</span>'<span class="token punctuation">)</span><span class="token punctuation">;</span>                
<span class="token punctuation">}</span>
echo $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">403 Forbidden </span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="get-recipients-and-their-statuses-parties" class="spec-item-header" data-entity="method" data-entity-id="get-recipients-and-their-statuses-parties">
								<a class="head_anchor" href="#get-recipients-and-their-statuses-parties"></a>Get Recipients and their statuses - parties</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="GET">GET</span>
									<span class="method-url">/messages/<code>{messageId}</code>/parties</span>
</div>
</div>
							<div class="description description__main">
<p>Get the infornation about parties including Sender and Recipents.</p>
</div>
							<div class="method-section">
								<h4>Path variables</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageId</div>
											<div class="schema-property__type">
												<span class="primitive">integer</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right"></div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-12">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">id</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>Party Id</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">1902</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">userID</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">393</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">email</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">shc.narola@narolainfotech.com</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">name</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">shc.narola@narolainfotech.com</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">company</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>Company</p>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">isSender</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">1</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">isOwner</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token number">1</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">canEdit</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">1</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">unclaimedNoticeTimeout</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">isUnclaimedNoticeTimeoutPast</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">Bad Request</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Invalid MessageId supplied.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Ex 1">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/1234/parties HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params = array(<span class="token string">"view"</span> =&gt; <span class="token string">"1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;get(<span class="token string">"/messages/1234/parties"</span><span class="token punctuation">,</span>$params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
  <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">11438</span><span class="token punctuation">,</span>
  <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token number">321</span><span class="token punctuation">,</span>
  <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
  <span class="token property">"name"</span><span class="token operator">:</span> <span class="token string">"shcnarola"</span><span class="token punctuation">,</span>
  <span class="token property">"company"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"isSender"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"isOwner"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"canEdit"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"unclaimedNoticeTimeout"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"views"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
    <span class="token property">"totalViews"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"firstView"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"lastView"</span><span class="token operator">:</span> <span class="token string">""</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token property">"isSignedFor"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"signedFor"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"reminders"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
  <span class="token property">"DELsmtpResponse"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
  <span class="token property">"DELtimestamp"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"DLs"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"addedAfter"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"addedBy"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
  <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 05:49:02"</span><span class="token punctuation">,</span>
  <span class="token property">"bounced"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"isInitialBounce"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"isReminderBounce"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"bounceTimestamp"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"bounceDiagnosticCode"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"bounceAction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"canRemind"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"nextRemind"</span><span class="token operator">:</span> <span class="token string">""</span>
<span class="token punctuation">}</span><span class="token punctuation">,</span>
<span class="token punctuation">{</span>
  <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">11439</span><span class="token punctuation">,</span>
  <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token number">339</span><span class="token punctuation">,</span>
  <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"ps.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
  <span class="token property">"name"</span><span class="token operator">:</span> <span class="token string">"Narola"</span><span class="token punctuation">,</span>
  <span class="token property">"company"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"isSender"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"isOwner"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"canEdit"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"unclaimedNoticeTimeout"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"views"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
    <span class="token property">"totalViews"</span><span class="token operator">:</span> <span class="token number">6</span><span class="token punctuation">,</span>
    <span class="token property">"firstView"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 06:33:31"</span><span class="token punctuation">,</span>
    <span class="token property">"lastView"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 07:13:45"</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token property">"isSignedFor"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"signedFor"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 06:33:31"</span><span class="token punctuation">,</span>
  <span class="token property">"reminders"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
  <span class="token property">"DELsmtpResponse"</span><span class="token operator">:</span> <span class="token string">"250 Requested mail action okay, completed: partyID=0Lp694-1Z0BwJ2YwG-00exZY"</span><span class="token punctuation">,</span>
  <span class="token property">"DELtimestamp"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 05:49:04"</span><span class="token punctuation">,</span>
  <span class="token property">"DLs"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"addedAfter"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"addedBy"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
  <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 05:49:02"</span><span class="token punctuation">,</span>
  <span class="token property">"bounced"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"isInitialBounce"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"isReminderBounce"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"bounceTimestamp"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"bounceDiagnosticCode"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"bounceAction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"canRemind"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"nextRemind"</span><span class="token operator">:</span> <span class="token string">""</span>
<span class="token punctuation">}</span><span class="token punctuation">,</span>
<span class="token punctuation">{</span>
  <span class="token property">"partyID"</span><span class="token operator">:</span> <span class="token number">11440</span><span class="token punctuation">,</span>
  <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token number">670</span><span class="token punctuation">,</span>
  <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"ay.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
  <span class="token property">"name"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"company"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"isSender"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"isOwner"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"canEdit"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"unclaimedNoticeTimeout"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"views"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
    <span class="token property">"totalViews"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"firstView"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"lastView"</span><span class="token operator">:</span> <span class="token string">""</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token property">"isSignedFor"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"signedFor"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"reminders"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
    <span class="token string">"2015-09-18 07:50:29"</span><span class="token punctuation">,</span>
    <span class="token string">"2015-09-23 05:16:13"</span>
  <span class="token punctuation">]</span><span class="token punctuation">,</span>
  <span class="token property">"DELsmtpResponse"</span><span class="token operator">:</span> <span class="token string">"250 Requested mail action okay, completed: partyID=0MgvFm-1ZGtwd3Nw0-00M3Aq"</span><span class="token punctuation">,</span>
  <span class="token property">"DELtimestamp"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 05:49:04"</span><span class="token punctuation">,</span>
  <span class="token property">"DLs"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"addedAfter"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"addedBy"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
  <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-09-18 05:49:02"</span><span class="token punctuation">,</span>
  <span class="token property">"bounced"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"isInitialBounce"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"isReminderBounce"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"bounceTimestamp"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"bounceDiagnosticCode"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"bounceAction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
  <span class="token property">"canRemind"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"nextRemind"</span><span class="token operator">:</span> <span class="token string">""</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="get-attachments-list" class="spec-item-header" data-entity="method" data-entity-id="get-attachments-list">
								<a class="head_anchor" href="#get-attachments-list"></a>Get attachments list</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="GET">GET</span>
									<span class="method-url">/messages/<code>{messageID}</code>/attachments</span>
</div>
</div>
							<div class="description description__main">
<p>Return the information about the message attachments including id, fileId, fileName, size, attachmentId.</p>
</div>
							<div class="method-section">
								<h4>Path variables</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageID</div>
											<div class="schema-property__type">
												<span class="primitive">integer</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right"></div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-12">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">fileID</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">fileName</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">size</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">attachmentID</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">Bad Request</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Invalid MessageID supplied.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-12">
												<span class="response-details__status" data-response-status="403">403</span>
												<span class="response-details__name">Forbidden</span>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">code</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token number">403</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">message</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">User does not have authority to perform this action on this message.</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Ex - 1 Get attachments list - Unauthorized">
									<div class="description method-example__description">
<p>Get the attachments</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/1953/attachments HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/<span class="token number">1953</span>/attachments'<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">403 Forbidden </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"code"</span><span class="token operator">:</span> <span class="token number">403</span><span class="token punctuation">,</span>
    <span class="token property">"message"</span><span class="token operator">:</span> <span class="token string">"User does not have authority to perform this action on this message."</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex - 2 Get attachments list - OK">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/975/attachments HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/<span class="token number">970</span>/attachments'<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span> 
    <span class="token punctuation">{</span> 
        <span class="token property">"fileID"</span><span class="token operator">:</span> <span class="token number">682</span><span class="token punctuation">,</span>
        <span class="token property">"fileName"</span><span class="token operator">:</span> <span class="token string">"steve-jobs-quotes-about-life-352.jpg"</span><span class="token punctuation">,</span>
        <span class="token property">"size"</span><span class="token operator">:</span> <span class="token number">74253</span><span class="token punctuation">,</span>
        <span class="token property">"attachmentID"</span><span class="token operator">:</span>
        <span class="token string">"740"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span> 
    <span class="token punctuation">{</span>
        <span class="token property">"fileID"</span><span class="token operator">:</span> <span class="token number">683</span><span class="token punctuation">,</span> 
        <span class="token property">"fileName"</span><span class="token operator">:</span> <span class="token string">"images (1).jpg"</span><span class="token punctuation">,</span>
        <span class="token property">"size"</span><span class="token operator">:</span> <span class="token number">19244</span><span class="token punctuation">,</span> 
        <span class="token property">"attachmentID"</span><span class="token operator">:</span> <span class="token string">"741"</span> 
    <span class="token punctuation">}</span> 
<span class="token punctuation">]</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="message-transaction-history" class="spec-item-header" data-entity="method" data-entity-id="message-transaction-history">
								<a class="head_anchor" href="#message-transaction-history"></a>Message transaction history</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="GET">GET</span>
									<span class="method-url">/messages/<code>{messageID}</code>/transactions</span>
</div>
</div>
							<div class="description description__main">
<p>Returns all financial transactions for the Message.</p>
</div>
							<div class="method-section">
								<h4>Path variables</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageID</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right"></div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Request parameters</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageID</div>
											<div class="schema-property__type">
												<span class="primitive">number</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>2951</p>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">recipientID</div>
											<div class="schema-property__type">
												<span class="primitive">number</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Transactions can be filtered using <code>recipientID</code>.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token number">324</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">to</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<span class="schema__format">date</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Transactions can be filter using <code>to</code> date.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">2015-10-23</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">from</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<span class="schema__format">date</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Transactions can be filter using <code>from</code> date.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">2015-10-30</span></div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Returns the information about Transaction history for the particular message.</p>
</div>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">TransactionID</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token number">1000002165</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">DateTime</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">2015-10-28 15:59:10</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">CoR</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">C</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">discount</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token number">1.99</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">price</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">Months</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">Certs</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token number">1</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">MsgID</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token number">2951</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">CertType</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">pre</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">recipients</div>
																	<div class="schema-property__type">
																		<span class="primitive">Array</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
															<div class="schema-array-items">
																<div class="schema">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema__type">
																				<span class="primitive">Object</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
																	<div class="schema-properties">
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">RecipientID</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																				<div class="schema-property__right">
																					<div class="schema-property-examples">
																						<span class="lbl">Example:</span>
																						<div class="schema-property-examples__list"><span class="example-primitive token string">588</span></div>
</div>
</div>
</div>
</div>
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">email</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																				<div class="schema-property__right">
																					<div class="schema-property-examples">
																						<span class="lbl">Example:</span>
																						<div class="schema-property-examples__list"><span class="example-primitive token string">recipient1@docsmit.com</span></div>
</div>
</div>
</div>
</div>
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">Name</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																				<div class="schema-property__right">
																					<div class="schema-property-examples">
																						<span class="lbl">Example:</span>
																						<div class="schema-property-examples__list"><span class="example-primitive token string">recipient1 docsmit</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">Bad Request</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Invalid MessageID supplied.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Ex 1 - Transactions">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/2951/transactions HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;get(<span class="token string">"/messages/2951/transactions"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002158</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-27 18:24:46"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002159</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-27 18:25:05"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002160</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-27 19:18:07"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002161</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-27 19:19:29"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002162</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-27 19:21:37"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002163</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-27 19:22:24"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002164</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-27 19:28:16"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002165</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 15:59:10"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002166</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:20:41"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002167</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:21:40"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002168</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:28:57"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002169</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:29:38"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002170</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:40:11"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002171</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:41:53"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002172</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:42:21"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002173</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:43:00"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002174</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:43:23"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002175</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:44:18"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002176</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:44:47"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002177</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:47:29"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002178</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:50:58"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002179</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:01:31"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002180</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:10:29"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002181</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:13:25"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002182</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:16:26"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002183</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:17:02"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002184</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:17:36"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002185</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:18:10"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002186</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:24:34"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002187</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:30:28"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex 2 - Transactions - RecipientID">
									<div class="description method-example__description">
<p>Transactions can be filter using <code>RecipientID</code></p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/2951/transactions HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params = array(<span class="token string">"recipientID"</span>=&gt;<span class="token number">588</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;get(<span class="token string">"/messages/2951/transactions"</span><span class="token punctuation">,</span>$params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002158</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-27 18:24:46"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002159</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-27 18:25:05"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002160</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-27 19:18:07"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002161</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-27 19:19:29"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002162</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-27 19:21:37"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002163</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-27 19:22:24"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002164</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-27 19:28:16"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002165</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 15:59:10"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002166</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:20:41"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002167</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:21:40"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002168</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:28:57"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002169</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:29:38"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002170</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:40:11"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002171</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:41:53"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002172</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:42:21"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002173</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:43:00"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002174</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:43:23"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002175</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:44:18"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002176</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:44:47"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002177</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:47:29"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002178</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:50:58"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002179</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:01:31"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002180</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:10:29"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002181</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:13:25"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002182</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:16:26"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002183</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:17:02"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002184</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:17:36"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002185</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:18:10"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002186</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:24:34"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002187</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:30:28"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex 3 - Transaction - Date Filter">
									<div class="description method-example__description">
<p>Transactions can be filter using either <code>to</code> date or <code>from</code> date or both.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/2951/transactions HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params = array(<span class="token string">"from"</span>=&gt;<span class="token string">"2015-10-28"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;get(<span class="token string">"/messages/2951/transactions"</span><span class="token punctuation">,</span>$params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002165</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 15:59:10"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002166</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:20:41"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002167</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:21:40"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002168</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:28:57"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002169</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:29:38"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002170</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:40:11"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002171</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:41:53"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002172</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:42:21"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002173</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:43:00"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002174</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:43:23"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002175</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:44:18"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002176</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:44:47"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002177</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:47:29"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002178</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 16:50:58"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002179</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:01:31"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002180</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:10:29"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002181</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:13:25"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002182</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:16:26"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002183</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:17:02"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002184</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:17:36"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002185</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:18:10"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002186</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:24:34"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"TransactionID"</span><span class="token operator">:</span> <span class="token number">1000002187</span><span class="token punctuation">,</span>
        <span class="token property">"DateTime"</span><span class="token operator">:</span> <span class="token string">"2015-10-28 17:30:28"</span><span class="token punctuation">,</span>
        <span class="token property">"CoR"</span><span class="token operator">:</span> <span class="token string">"C"</span><span class="token punctuation">,</span>
        <span class="token property">"discount"</span><span class="token operator">:</span> <span class="token number">1.99</span><span class="token punctuation">,</span>
        <span class="token property">"price"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Months"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"Certs"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"MsgID"</span><span class="token operator">:</span> <span class="token number">2951</span><span class="token punctuation">,</span>
        <span class="token property">"CertType"</span><span class="token operator">:</span> <span class="token string">"pre"</span><span class="token punctuation">,</span>
        <span class="token property">"recipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"RecipientID"</span><span class="token operator">:</span> <span class="token string">"588"</span><span class="token punctuation">,</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"recipient1@docsmit.com"</span><span class="token punctuation">,</span>
                <span class="token property">"Name"</span><span class="token operator">:</span> <span class="token string">"recipient1 docsmit"</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="download-file-attachment" class="spec-item-header" data-entity="method" data-entity-id="download-file-attachment">
								<a class="head_anchor" href="#download-file-attachment"></a>Download file attachment</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="GET">GET</span>
									<span class="method-url">/messages/<code>{messageId}</code>/download/<code>{fileId}</code></span>
</div>
</div>
							<div class="method-section">
								<h4>Path variables</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageId</div>
											<div class="schema-property__type">
												<span class="primitive">integer</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right"></div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">fileId</div>
											<div class="schema-property__type">
												<span class="primitive">integer</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right"></div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Download success.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">Bad Request</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Invalid MessageId/fileId supplied.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Example 1">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/1234/download/236 HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;get(<span class="token string">"/messages/1234/download/236"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="download-zip-attachment" class="spec-item-header" data-entity="method" data-entity-id="download-zip-attachment">
								<a class="head_anchor" href="#download-zip-attachment"></a>Download Zip attachment</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="GET">GET</span>
									<span class="method-url">/messages/<code>{messageID}</code>/download/<code>{ZipID}</code></span>
</div>
</div>
							<div class="method-section">
								<h4>Path variables</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageID</div>
											<div class="schema-property__type">
												<span class="primitive">integer</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right"></div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">ZipID</div>
											<div class="schema-property__type">
												<span class="primitive">integer</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right"></div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Download Success.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">Bad Request</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Invalid MessageID/ZipID supply.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="generate-tracking-id" class="spec-item-header" data-entity="method" data-entity-id="generate-tracking-id">
								<a class="head_anchor" href="#generate-tracking-id"></a>Generate tracking ID (DONT USE RIGHT NOW)</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="POST">POST</span>
									<span class="method-url">/messages/<code>{messageID}</code>/trackingID</span>
</div>
</div>
							<div class="method-section">
								<h4>Path variables</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageID</div>
											<div class="schema-property__type">
												<span class="primitive">integer</span>
												<i class="property__attr property__attr--required" data-required="true">required</i>
</div>
</div>
										<div class="param__right"></div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-12">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">success</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">Y</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">narrative</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">Tracking ID has been generated successfully.</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="400">400</span>
												<span class="response-details__name">400
													<a class="small-link" href="#400">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Problem with one of the request parameters.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="403">403</span>
												<span class="response-details__name">403
													<a class="small-link" href="#403">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>User does not have authority to perform this action on this message.</p>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Ex 1">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">POST</span> http://secure.docsmit.com/api/v1/messages/1035/trackingID HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;post('/messages/<span class="token number">1035</span>/trackingID'<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="account_group" class="spec-item-header" data-entity="group" data-entity-id="account_group">
								<a class="head_anchor" href="#account_group"></a>Account</div>
							<div class="description description__main">
<p>Deals with purchased credit.</p>
</div>
							<div class="method-section">
								<div class="group-methods-summary">
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#sent-list">Sent list</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="GET">GET</span>
											<span class="method-url">/messages/sent</span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#inbox-list">Inbox list</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="GET">GET</span>
											<span class="method-url">/messages/inbox</span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#draft-list">Draft list</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="GET">GET</span>
											<span class="method-url">/messages/draft</span>
</div>
</div>
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#user-account-information">User Account Information</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="GET">GET</span>
											<span class="method-url">/account/info</span>
</div>
</div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="sent-list" class="spec-item-header" data-entity="method" data-entity-id="sent-list">
								<a class="head_anchor" href="#sent-list"></a>Sent list</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="GET">GET</span>
									<span class="method-url">/messages/sent</span>
</div>
</div>
							<div class="description description__main">
<p>Get the list of messages that have been sent.</p>
</div>
							<div class="method-section">
								<h4>Request parameters</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">clientMsgID</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Sent messages can be filtered using <code>clientMsgID</code> param. It will return all the sent messages which match exactly the specified <code>clientMsgID</code>.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">"1400.3.0011"</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">turnOffSoftwareID</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Normally only sent messages sent with the calling SoftwareID are returned.  If the call is made with <code>turnOffSoftwareID</code> set to be true, it will return all the sent messages from the user without filtering based on <code>softwareID</code>.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">1</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">clientMsgID_MR</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Sent messages can be filter using <code>clientMsgID_MR</code> param. This parameter accepts MySQL style REGEX. It will list all the messages which match the regex.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Examples:</span>
												<div class="schema-property-examples__list">
<span class="example-primitive token string">^1400</span><span class="example-primitive token string">[abc]</span><span class="example-primitive token string">ab$</span>
</div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">recipient</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Sent message can be filter using recipient. It will return all the sent messages where the string exactly matches a recipeint.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">"Ashish"</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageIDList</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>A list of comma separated values. If this is included messages with exactly matching MessageIDs will be returned.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">"42255,44790, 44891, 49106"</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">clientMsgIDList</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>A list of comma separated values. If this is included messages with exactly matching clientMsgIDs will be returned.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">"1400.3.0011,Johnson-14"</span></div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Everything worked as expected</p>
</div>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">id</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>MessageID</p>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">from</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">title</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">to</div>
																	<div class="schema-property__type">
																		<span class="primitive">Object</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
															<div class="schema-properties">
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">email</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">unread</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">SignedFor</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
																				<span class="schema__format">date</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">attachments</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">threadRoot</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">certified</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">attSize</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">sent</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">0000-00-00 00:00:00</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">transaction</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Examples:</span>
																		<div class="schema-property-examples__list">
<span class="example-primitive token string">c</span><span class="example-primitive token string">p</span><span class="example-primitive token string">f</span><span class="example-primitive token string">a</span>
</div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">bookmarked</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">isFlagged</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">deliverAfter</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">2015-04-16 12:49:00</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">isDeliverAfter</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">isSent</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">isBlastMessage</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">totalRecipientsCount</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">totalUnsignedCount</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">isBounced</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">bouncedMsgStr</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">Bounced : 2 (Inital : 1/Reminder :1)</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">isEncrypted</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">initialBouncedMsgRecipients</div>
																	<div class="schema-property__type">
																		<span class="primitive">Object</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
															<div class="schema-properties">
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">email</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">reminderBouncedMsgRecipients</div>
																	<div class="schema-property__type">
																		<span class="primitive">Object</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
															<div class="schema-properties">
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">email</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">alreadySentType</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Examples:</span>
																		<div class="schema-property-examples__list">
<span class="example-primitive token string">SR</span><span class="example-primitive token string">NSR</span><span class="example-primitive token string">BOTH</span>
</div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">SRSent</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
																		<span class="schema__format">date</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">NSRSent</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
																		<span class="schema__format">date</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">2015-04-16 16:49:00</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">SRDeliverAfter</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
																		<span class="schema__format">date</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">2015-04-16 16:49:00</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">NSRDeliverAfter</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
																		<span class="schema__format">date</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">0000-00-00 00:00:00</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="403">403</span>
												<span class="response-details__name">403
													<a class="small-link" href="#403">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>User does not have authority to perform this action on this message.</p>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Example">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/sent HTTP/1.1</span> 

<span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
  <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token string">"690"</span><span class="token punctuation">,</span>
  <span class="token property">"from"</span><span class="token operator">:</span> <span class="token string">"shcnarola"</span><span class="token punctuation">,</span>
  <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"Docsmit Message"</span><span class="token punctuation">,</span>
  <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"hid.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
      <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token string">"2015-01-01T13:00:00"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"apa.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
      <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token string">"2015-01-01T13:00:00"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"ps.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
      <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token string">"2015-01-01T13:00:00"</span>
    <span class="token punctuation">}</span>
  <span class="token punctuation">]</span><span class="token punctuation">,</span>
  <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
  <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token string">"690"</span><span class="token punctuation">,</span>
  <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token string">"0"</span><span class="token punctuation">,</span>
  <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
  <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
  <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"deliverAfter"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 12:49:00"</span><span class="token punctuation">,</span>
  <span class="token property">"isDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
  <span class="token property">"isSent"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"isBlastMessage"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
  <span class="token property">"totalRecipientsCount"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
  <span class="token property">"totalUnsignedCount"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
  <span class="token property">"isBounced"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
  <span class="token property">"bouncedMsgStr"</span><span class="token operator">:</span> <span class="token string">"Bounced : 2 (Inital : 1/Reminder :1)"</span><span class="token punctuation">,</span>
  <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"initialBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"apa.narola@narolainfotech.com"</span>
    <span class="token punctuation">}</span>
  <span class="token punctuation">]</span><span class="token punctuation">,</span>
  <span class="token property">"reminderBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"ps.narola@narolainfotech.com"</span>
    <span class="token punctuation">}</span>
  <span class="token punctuation">]</span><span class="token punctuation">,</span>
  <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
  <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-01-01T13:00:00"</span><span class="token punctuation">,</span>
  <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 16:49:00"</span><span class="token punctuation">,</span>
  <span class="token property">"SRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 16:49:00"</span><span class="token punctuation">,</span>
  <span class="token property">"NSRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex 1 - Sent">
									<div class="description method-example__description">
<p>List all Sent Messages</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/sent HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span> <span class="token string">"http://secure.docsmit.com/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/sent'<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"id"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token string">"2015-01-01T13:00:00"</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
    <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"deliverAfter"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 12:49:00"</span><span class="token punctuation">,</span>
    <span class="token property">"isDeliverAfter"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"isSent"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isBlastMessage"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"totalRecipientsCount"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"totalUnsignedCount"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"isBounced"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"bouncedMsgStr"</span><span class="token operator">:</span> <span class="token string">"Bounced : 2 (Inital : 1/Reminder :1)"</span><span class="token punctuation">,</span>
    <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"initialBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">""</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"reminderBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">""</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
    <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-01-01T13:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 16:49:00"</span><span class="token punctuation">,</span>
    <span class="token property">"SRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 16:49:00"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex 2 - Sent - Filter `unsigned` messages">
									<div class="description method-example__description">
<p>Retrieve <code>unsigned</code> messages from sent. User can filter sent message by specifying <code>unsigned</code> search param. The valid value for this param is <code>true</code>, <code>false</code>, <code>1</code>, '0'. It will retrieve all the messages which matches the specified value for <code>unsigned</code> param from sent.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/sent?unsigned=true HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span> <span class="token string">"http://secure.docsmit.com/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"unsigned"</span><span class="token punctuation">]</span>= <span class="token string">"true"</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/sent'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token string">"2015-01-01T13:00:00"</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
    <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"deliverAfter"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 12:49:00"</span><span class="token punctuation">,</span>
    <span class="token property">"isDeliverAfter"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"isSent"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isBlastMessage"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"totalRecipientsCount"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"totalUnsignedCount"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"isBounced"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"bouncedMsgStr"</span><span class="token operator">:</span> <span class="token string">"Bounced : 2 (Inital : 1/Reminder :1)"</span><span class="token punctuation">,</span>
    <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"initialBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">""</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"reminderBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">""</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
    <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-01-01T13:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 16:49:00"</span><span class="token punctuation">,</span>
    <span class="token property">"SRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 16:49:00"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex3 - Sent - Filter messages by `clientMsgID`">
									<div class="description method-example__description">
<p>Sent messages can be filter by specifying <code>clientMsgID</code> search params. It will retrieve all the messages which matches the specified <code>clientMsgID</code> from Sent messages.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/sent?clientMsgID="clientmsgidgoeshere" HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span> <span class="token string">"http://secure.docsmit.com/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"clientMsgID"</span><span class="token punctuation">]</span> = <span class="token string">"test123"</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/sent'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token string">"2015-01-01T13:00:00"</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
    <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"deliverAfter"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 12:49:00"</span><span class="token punctuation">,</span>
    <span class="token property">"isDeliverAfter"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"isSent"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isBlastMessage"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"totalRecipientsCount"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"totalUnsignedCount"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"isBounced"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"bouncedMsgStr"</span><span class="token operator">:</span> <span class="token string">"Bounced : 2 (Inital : 1/Reminder :1)"</span><span class="token punctuation">,</span>
    <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"initialBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">""</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"reminderBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">""</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
    <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-01-01T13:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 16:49:00"</span><span class="token punctuation">,</span>
    <span class="token property">"SRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 16:49:00"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex4 - Sent - Turn off softwareID filter">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/sent?turnOffSoftwareID=true HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span><span class="token string">"http://secure.docsmit.com/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"turnOffSoftwareID"</span><span class="token punctuation">]</span> = <span class="token string">"true"</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/sent'<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"id"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token string">"2015-01-01T13:00:00"</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
    <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"deliverAfter"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 12:49:00"</span><span class="token punctuation">,</span>
    <span class="token property">"isDeliverAfter"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"isSent"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isBlastMessage"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"totalRecipientsCount"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"totalUnsignedCount"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"isBounced"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"bouncedMsgStr"</span><span class="token operator">:</span> <span class="token string">"Bounced : 2 (Inital : 1/Reminder :1)"</span><span class="token punctuation">,</span>
    <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"initialBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">""</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"reminderBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">""</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
    <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-01-01T13:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 16:49:00"</span><span class="token punctuation">,</span>
    <span class="token property">"SRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 16:49:00"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex5 - Sent - Filter messages by `clientMsgID_MR`">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/sent HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span> <span class="token string">"http://secure.docsmit.com/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"clientMsgID_MR"</span><span class="token punctuation">]</span> = <span class="token string">"^[ab]|rs$"</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/sent'<span class="token punctuation">,</span>$params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"id"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
            <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token string">"2015-01-01T13:00:00"</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
    <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"deliverAfter"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 12:49:00"</span><span class="token punctuation">,</span>
    <span class="token property">"isDeliverAfter"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"isSent"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isBlastMessage"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"totalRecipientsCount"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"totalUnsignedCount"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"isBounced"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"bouncedMsgStr"</span><span class="token operator">:</span> <span class="token string">"Bounced : 2 (Inital : 1/Reminder :1)"</span><span class="token punctuation">,</span>
    <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"initialBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">""</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"reminderBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">""</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">]</span><span class="token punctuation">,</span>
    <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
    <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-01-01T13:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 16:49:00"</span><span class="token punctuation">,</span>
    <span class="token property">"SRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"2015-04-16 16:49:00"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex 6-  Filter messages using 'messageIDList'">
									<div class="description method-example__description">
<p>Sent messages can be fileter by specifying messageIDList search params. It will retrieve all the messages which matches the specified messageID List from sent.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/sent HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span> <span class="token string">"http://secure.docsmit.com/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"messageIDList"</span><span class="token punctuation">]</span> = <span class="token string">"1086,1087"</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/sent'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1087</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"This is new message title from Hiren testcase"</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"sop.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
                <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
                <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token null">null</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">1087</span><span class="token punctuation">,</span>
        <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 08:16:28"</span><span class="token punctuation">,</span>
        <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
        <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"deliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"isDeliverAfter"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isSent"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"isBlastMessage"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"totalRecipientsCount"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"totalUnsignedCount"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
        <span class="token property">"isBounced"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
        <span class="token property">"bouncedMsgStr"</span><span class="token operator">:</span> <span class="token string">"Bounced : 0 (Inital : 0/Reminder :0)"</span><span class="token punctuation">,</span>
        <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"initialBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"reminderBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
        <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 12:16:28"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"SRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"clientMsgID"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"id"</span><span class="token operator">:</span> <span class="token number">1086</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"This is new message title from Hiren testcase"</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"sop.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
                <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
                <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token null">null</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">1086</span><span class="token punctuation">,</span>
        <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 08:14:40"</span><span class="token punctuation">,</span>
        <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
        <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"deliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"isDeliverAfter"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isSent"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"isBlastMessage"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"totalRecipientsCount"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"totalUnsignedCount"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
        <span class="token property">"isBounced"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
        <span class="token property">"bouncedMsgStr"</span><span class="token operator">:</span> <span class="token string">"Bounced : 0 (Inital : 0/Reminder :0)"</span><span class="token punctuation">,</span>
        <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"initialBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"reminderBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
        <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 12:14:40"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"SRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"clientMsgID"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex 7 - Filter messages by 'clientMsgIDLis">
									<div class="description method-example__description">
<p>Sent messages can be fileter by specifying clientMsgIDList search params. It will retrieve all the messages which matches the specified clientMsgID List from Sent.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/sent HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span> <span class="token string">"http://secure.docsmit.com/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"clientMsgIDList"</span><span class="token punctuation">]</span> = <span class="token string">"test123,test456"</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/sent'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1090</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"This is new message title from Hiren testcase"</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"sop.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
                <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
                <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token null">null</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">1090</span><span class="token punctuation">,</span>
        <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 08:20:22"</span><span class="token punctuation">,</span>
        <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
        <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"deliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"isDeliverAfter"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isSent"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"isBlastMessage"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"totalRecipientsCount"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"totalUnsignedCount"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
        <span class="token property">"isBounced"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
        <span class="token property">"bouncedMsgStr"</span><span class="token operator">:</span> <span class="token string">"Bounced : 0 (Inital : 0/Reminder :0)"</span><span class="token punctuation">,</span>
        <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"initialBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"reminderBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
        <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 12:20:22"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"SRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"clientMsgID"</span><span class="token operator">:</span> <span class="token string">"test123"</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex 8 -Filter by messageIDs and clientMsgIDList">
									<div class="description method-example__description">
<p>Sent messages can be fileter by specifying clientMsgIDs as well messageIDs search params. It will retrieve all the messages which matches the specified clientMsgID and messageID from sent.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/sent HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span> <span class="token string">"http://secure.docsmit.com/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"messageIDList"</span><span class="token punctuation">]</span> = <span class="token string">"1086,1087"</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"clientMsgIDList"</span><span class="token punctuation">]</span> = <span class="token string">"test123"</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/sent'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1090</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"This is new message title from Hiren testcase"</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"sop.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
                <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
                <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token null">null</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">1090</span><span class="token punctuation">,</span>
        <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 08:20:22"</span><span class="token punctuation">,</span>
        <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
        <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"deliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"isDeliverAfter"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isSent"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"isBlastMessage"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"totalRecipientsCount"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"totalUnsignedCount"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
        <span class="token property">"isBounced"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
        <span class="token property">"bouncedMsgStr"</span><span class="token operator">:</span> <span class="token string">"Bounced : 0 (Inital : 0/Reminder :0)"</span><span class="token punctuation">,</span>
        <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"initialBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"reminderBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
        <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 12:20:22"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"SRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"clientMsgID"</span><span class="token operator">:</span> <span class="token string">"test123"</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1087</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"This is new message title from Hiren testcase"</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"sop.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
                <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
                <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token null">null</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">1087</span><span class="token punctuation">,</span>
        <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 08:16:28"</span><span class="token punctuation">,</span>
        <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
        <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"deliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"isDeliverAfter"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isSent"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"isBlastMessage"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"totalRecipientsCount"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"totalUnsignedCount"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
        <span class="token property">"isBounced"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
        <span class="token property">"bouncedMsgStr"</span><span class="token operator">:</span> <span class="token string">"Bounced : 0 (Inital : 0/Reminder :0)"</span><span class="token punctuation">,</span>
        <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"initialBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"reminderBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
        <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 12:16:28"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"SRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"clientMsgID"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1086</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"This is new message title from Hiren testcase"</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span>
                <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"sop.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
                <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
                <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token null">null</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">1086</span><span class="token punctuation">,</span>
        <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 08:14:40"</span><span class="token punctuation">,</span>
        <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
        <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"deliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"isDeliverAfter"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isSent"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"isBlastMessage"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"totalRecipientsCount"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"totalUnsignedCount"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
        <span class="token property">"isBounced"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
        <span class="token property">"bouncedMsgStr"</span><span class="token operator">:</span> <span class="token string">"Bounced : 0 (Inital : 0/Reminder :0)"</span><span class="token punctuation">,</span>
        <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"initialBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"reminderBouncedMsgRecipients"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
        <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 12:14:40"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"SRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"clientMsgID"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex 10 - Filter messages by sender">
									<div class="description method-example__description">
<p>Sent messages can be filter using sender param.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/sent HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span> <span class="token string">"http://secure.docsmit.com/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"sender"</span><span class="token punctuation">]</span> = <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/sent'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
  <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">2923</span><span class="token punctuation">,</span>
  <span class="token property">"from"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
  <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"This is message title from REST API"</span><span class="token punctuation">,</span>
  <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"ps.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
      <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token null">null</span>
    <span class="token punctuation">}</span>
  <span class="token punctuation">]</span><span class="token punctuation">,</span>
  <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">2923</span><span class="token punctuation">,</span>
  <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-09-23 10:23:43"</span><span class="token punctuation">,</span>
  <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
  <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"isDeliverAfter"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"isSent"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"isBlastMessage"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"totalRecipientsCount"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"totalUnsignedCount"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
  <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
  <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-09-23 10:23:43"</span><span class="token punctuation">,</span>
  <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
  <span class="token property">"SRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
  <span class="token property">"NSRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
  <span class="token property">"clientMsgID"</span><span class="token operator">:</span> <span class="token string">"safdsdfds123"</span><span class="token punctuation">,</span>
  <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span><span class="token punctuation">,</span>
  <span class="token property">"isBounced"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
  <span class="token property">"bouncedMsgCount"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
    <span class="token property">"total"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"initialBounce"</span><span class="token operator">:</span> <span class="token string">"0"</span><span class="token punctuation">,</span>
    <span class="token property">"reminderBounce"</span><span class="token operator">:</span> <span class="token string">"0"</span>
  <span class="token punctuation">}</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex - 11">
									<div class="description method-example__description">
<p>Sent message can be filter using recipient. It will return all the sent messages which belogs specified recipient.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/sent HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">,</span> <span class="token string">"http://secure.docsmit.com/api/v1"</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"recipient"</span><span class="token punctuation">]</span> = <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/sent'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
  <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">2923</span><span class="token punctuation">,</span>
  <span class="token property">"from"</span><span class="token operator">:</span> <span class="token string">"shc.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
  <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"This is message title from REST API"</span><span class="token punctuation">,</span>
  <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"ps.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
      <span class="token property">"SignedFor"</span><span class="token operator">:</span> <span class="token null">null</span>
    <span class="token punctuation">}</span>
  <span class="token punctuation">]</span><span class="token punctuation">,</span>
  <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">2923</span><span class="token punctuation">,</span>
  <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-09-23 10:23:43"</span><span class="token punctuation">,</span>
  <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
  <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"isDeliverAfter"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"isSent"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"isBlastMessage"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"totalRecipientsCount"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">"totalUnsignedCount"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
  <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
  <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
  <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-09-23 10:23:43"</span><span class="token punctuation">,</span>
  <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
  <span class="token property">"SRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
  <span class="token property">"NSRDeliverAfter"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
  <span class="token property">"clientMsgID"</span><span class="token operator">:</span> <span class="token string">"safdsdfds123"</span><span class="token punctuation">,</span>
  <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span><span class="token punctuation">,</span>
  <span class="token property">"isBounced"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
  <span class="token property">"bouncedMsgCount"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
    <span class="token property">"total"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"initialBounce"</span><span class="token operator">:</span> <span class="token string">"0"</span><span class="token punctuation">,</span>
    <span class="token property">"reminderBounce"</span><span class="token operator">:</span> <span class="token string">"0"</span>
  <span class="token punctuation">}</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="inbox-list" class="spec-item-header" data-entity="method" data-entity-id="inbox-list">
								<a class="head_anchor" href="#inbox-list"></a>Inbox list</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="GET">GET</span>
									<span class="method-url">/messages/inbox</span>
</div>
</div>
							<div class="description description__main">
<p>Get the list of received messages (the inbox).</p>
</div>
							<div class="method-section">
								<h4>Request parameters</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">unsigned</div>
											<div class="schema-property__type">
												<span class="primitive">boolean</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Filter Inbox messages using <code>unsigned</code> param. It will return all messages with a matching <code>unsigned</code> value.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">clientMsgID</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Filter Inbox messages using the <code>clientMsgID</code> param. It will return all the inbox messages which match exactly the <code>clientMsgID</code>.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">"POHME-12455-233456"</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">turnOffSoftwareID</div>
											<div class="schema-property__type">
												<span class="primitive">boolean</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Getting the list of inbox messages normally returns only those with a softwareID matching the software making the request. When true, it will return all the inbox messages without filtering for matching <code>softwareID</code>.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageIDList</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>The <code>messageIDList</code> is a comma separated list of messageIDs to return. It will return the inbox messages with messageIDs that appear in that list.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">1084,1080</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">clientMsgIDList</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>The <code>clientMsgIDList</code> is a comma separate list of clientMsgIDs. It will return the inbox messages with a clientMsgID that appears in the list.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">safdsdfds123,demoemailclientmsgid</span></div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-12">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Array</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-array-items">
														<div class="schema">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema__type">
																		<span class="primitive">Object</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
															<div class="schema-properties">
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">id</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">1971</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">from</div>
																			<div class="schema-property__type">
																				<span class="primitive">Object</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
																	<div class="schema-properties">
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">email</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																				<div class="schema-property__right">
																					<div class="schema-property-examples">
																						<span class="lbl">Example:</span>
																						<div class="schema-property-examples__list"><span class="example-primitive token string">docsmit@docsmit.com</span></div>
</div>
</div>
</div>
</div>
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">unread</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																				<div class="schema-property__right">
																					<div class="schema-property-examples">
																						<span class="lbl">Example:</span>
																						<div class="schema-property-examples__list"><span class="example-primitive token string">1</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">title</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">Welcome to Docsmit!</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">sent</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">2015-07-20 15:20:14</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">unread</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">bookmarked</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token number">1</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">certified</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">1</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">transaction</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">threadRoot</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">1971</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">attachments</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token number">1</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">attSize</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">0</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">isFlagged</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token number">1</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">isEncrypted</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token number">1</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">alreadySentType</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">SR</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">SRSent</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">2015-07-20 09:50:14</span></div>
</div>
</div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">NSRSent</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right">
																			<div class="schema-property-examples">
																				<span class="lbl">Example:</span>
																				<div class="schema-property-examples__list"><span class="example-primitive token string">0000-00-00 00:00:00</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="403">403</span>
												<span class="response-details__name">Forbidden</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Invalid value supplied for parameter.</p>
</div>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">code</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token number">403</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">message</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">Invalid value supplied for `unsigned` parameter. valid inputs for unsigned param are true, false, 1, 0</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Ex 1 - Inbox">
									<div class="description method-example__description">
<p>List all Inbox Messages</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/inbox HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/inbox'<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token string">"1971"</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
        <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token string">"docsmit@docsmit.com"</span>
        <span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token string">"1"</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"Welcome to Docsmit!"</span><span class="token punctuation">,</span>
    <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-07-20 15:20:14"</span><span class="token punctuation">,</span>
    <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
    <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token string">"1971"</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token string">"0"</span><span class="token punctuation">,</span>
    <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
    <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-07-20 09:50:14"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex2 - Inbox - Filter `unsigned` messages">
									<div class="description method-example__description">
<p>Retrieve <code>unsigned</code> messages from inbox. User can filter inbox message by specifying <code>unsigned</code> search param. The valid value for this param is <code>true</code>, <code>false</code>, <code>1</code>, '0'. It will retrieve all the messages which matches the specified value for <code>unsigned</code> param from inbox.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/inbox?unsigned=true HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"unsigned"</span><span class="token punctuation">]</span>= <span class="token string">"true"</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/inbox'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token string">"1971"</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
        <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token string">"docsmit@docsmit.com"</span>
        <span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token string">"1"</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"Welcome to Docsmit!"</span><span class="token punctuation">,</span>
    <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-07-20 15:20:14"</span><span class="token punctuation">,</span>
    <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
    <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token string">"1971"</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token string">"0"</span><span class="token punctuation">,</span>
    <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
    <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-07-20 09:50:14"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex3 - Inbox - Filter messages by `clientMsgID`">
									<div class="description method-example__description">
<p>Inbox messages can be fileter by specifying <code>clientMsgID</code> search params. It will retrieve all the messages which matches the specified <code>clientMsgID</code> from inbox.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/inbox?clientMsgID=clientmessageidgoeshere HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"clientMsgID"</span><span class="token punctuation">]</span> = <span class="token string">"test123"</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/inbox'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token string">"1971"</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
        <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token string">"docsmit@docsmit.com"</span>
        <span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token string">"1"</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"Welcome to Docsmit!"</span><span class="token punctuation">,</span>
    <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-07-20 15:20:14"</span><span class="token punctuation">,</span>
    <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token string">"1"</span><span class="token punctuation">,</span>
    <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token string">"1971"</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token string">"0"</span><span class="token punctuation">,</span>
    <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
    <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-07-20 09:50:14"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex4 - Inbox - Turn off softwareID filter">
									<div class="description method-example__description">
<p>By default, Inbox messages are filtered using <code>softwareID</code>, to turn off softwareID filter, use turnOffSoftwareID filter.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/inbox?turnOffSoftwareID=true HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"turnOffSoftwareID"</span><span class="token punctuation">]</span> = <span class="token string">"true"</span><span class="token punctuation">;</span> // or <span class="token boolean">true</span><span class="token punctuation">,</span><span class="token number">1</span>
$docsmit-&gt;get('/messages/inbox'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
  <span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">682</span><span class="token punctuation">,</span>
    <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"hid.narola@narolainfotech.com"</span>
      <span class="token punctuation">]</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"0"</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"khh.narola@narolainfotech.com"</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"sdfsf"</span><span class="token punctuation">,</span>
    <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-01-06 12:58:48"</span><span class="token punctuation">,</span>
    <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
    <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
    <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">682</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
    <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-01-06 07:28:48"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"d1bfa10082084dcd1bb9fd48750d1970"</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">680</span><span class="token punctuation">,</span>
    <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"hid.narola@narolainfotech.com"</span>
      <span class="token punctuation">]</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"0"</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"khh.narola@narolainfotech.com"</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"Test Email from KHH"</span><span class="token punctuation">,</span>
    <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2014-12-16 11:26:00"</span><span class="token punctuation">,</span>
    <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
    <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">"c"</span><span class="token punctuation">,</span>
    <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">680</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
    <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2014-12-16 05:56:00"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"d1bfa10082084dcd1bb9fd48750d1970"</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">625</span><span class="token punctuation">,</span>
    <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"hid.narola@narolainfotech.com"</span>
      <span class="token punctuation">]</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"0"</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"docsmit@docsmit.com"</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"Welcome to Docsmit!"</span><span class="token punctuation">,</span>
    <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2014-12-15 10:24:12"</span><span class="token punctuation">,</span>
    <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
    <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">625</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
    <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
    <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2014-12-15 04:54:12"</span><span class="token punctuation">,</span>
    <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
    <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"d1bfa10082084dcd1bb9fd48750d1970"</span>
  <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex5 - Inbox Filter messages by 'messageIDList'">
									<div class="description method-example__description">
<p>Inbox messages can be fileter by specifying messageIDList search params. It will retrieve all the messages which matches the specified messageID List from inbox.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/inbox HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"messageIDList"</span><span class="token punctuation">]</span> = '<span class="token number">1077</span><span class="token punctuation">,</span><span class="token number">1085</span>'<span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/inbox'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"id"</span><span class="token operator">:</span> <span class="token number">1085</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"apa.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"Test :: 1085"</span><span class="token punctuation">,</span>
        <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 05:36:12"</span><span class="token punctuation">,</span>
        <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
        <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
        <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">1085</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
        <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 09:36:12"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"id"</span><span class="token operator">:</span> <span class="token number">1077</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"apa.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"TEST :: 1077"</span><span class="token punctuation">,</span>
        <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 01:01:04"</span><span class="token punctuation">,</span>
        <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
        <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
        <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">1077</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"NSR"</span><span class="token punctuation">,</span>
        <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 05:01:04"</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex6 - inbox Filter messages by 'clientMsgIDList'">
									<div class="description method-example__description">
<p>Inbox messages can be fileter by specifying clientMsgIDList search params. It will retrieve all the messages which matches the specified clientMsgID List from inbox.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/inbox HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"clientMsgIDList"</span><span class="token punctuation">]</span> = 'safdsdfds123<span class="token punctuation">,</span><span class="token number">123456789</span>'<span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/inbox'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1078</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"0"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"apa.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"TESt :: 1078"</span><span class="token punctuation">,</span>
        <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 01:06:36"</span><span class="token punctuation">,</span>
        <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
        <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
        <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">1078</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"BOTH"</span><span class="token punctuation">,</span>
        <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 05:06:36"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 05:06:35"</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1074</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"psh.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"TEST :: 1074"</span><span class="token punctuation">,</span>
        <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 00:56:09"</span><span class="token punctuation">,</span>
        <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
        <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
        <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">1074</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"BOTH"</span><span class="token punctuation">,</span>
        <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 04:56:09"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 04:55:50"</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Ex7 -  Filter by messageIDs and clientMsgIDList">
									<div class="description method-example__description">
<p>Inbox messages can be fileter by specifying clientMsgIDs as well messageIDs  search params. It will retrieve all the messages which matches the specified clientMsgID and messageID from inbox.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/inbox HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"messageIDList"</span><span class="token punctuation">]</span> = '<span class="token number">1077</span><span class="token punctuation">,</span><span class="token number">1085</span>'<span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"clientMsgIDList"</span><span class="token punctuation">]</span> = 'safdsdfds123<span class="token punctuation">,</span><span class="token number">123456789</span>'<span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/inbox'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1085</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"apa.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"Test :: 1085"</span><span class="token punctuation">,</span>
        <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 05:36:12"</span><span class="token punctuation">,</span>
        <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
        <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
        <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">1085</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"SR"</span><span class="token punctuation">,</span>
        <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-31 09:36:12"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1078</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"0"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"apa.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"TESt :: 1078"</span><span class="token punctuation">,</span>
        <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 01:06:36"</span><span class="token punctuation">,</span>
        <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
        <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
        <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">1078</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"BOTH"</span><span class="token punctuation">,</span>
        <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 05:06:36"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 05:06:35"</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1077</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"apa.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"TEST :: 1077"</span><span class="token punctuation">,</span>
        <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 01:01:04"</span><span class="token punctuation">,</span>
        <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
        <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
        <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">1077</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"NSR"</span><span class="token punctuation">,</span>
        <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"0000-00-00 00:00:00"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 05:01:04"</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1074</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"psh.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"TEST :: 1074"</span><span class="token punctuation">,</span>
        <span class="token property">"sent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 00:56:09"</span><span class="token punctuation">,</span>
        <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
        <span class="token property">"bookmarked"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"certified"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
        <span class="token property">"transaction"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
        <span class="token property">"threadRoot"</span><span class="token operator">:</span> <span class="token number">1074</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isFlagged"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"isEncrypted"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"alreadySentType"</span><span class="token operator">:</span> <span class="token string">"BOTH"</span><span class="token punctuation">,</span>
        <span class="token property">"SRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 04:56:09"</span><span class="token punctuation">,</span>
        <span class="token property">"NSRSent"</span><span class="token operator">:</span> <span class="token string">"2015-08-26 04:55:50"</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="draft-list" class="spec-item-header" data-entity="method" data-entity-id="draft-list">
								<a class="head_anchor" href="#draft-list"></a>Draft list</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="GET">GET</span>
									<span class="method-url">/messages/draft</span>
</div>
</div>
							<div class="description description__main">
<p>Get the list of unsent (draft) messages.</p>
</div>
							<div class="method-section">
								<h4>Request parameters</h4>
								<div class="parameters-list">
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">clientMsgID</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Draft messages can be filtered using <code>clientMsgID</code>.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">validclientmessageid</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">turnOffSoftwareID</div>
											<div class="schema-property__type">
												<span class="primitive">boolean</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>By default draft messages are filtered using <code>softwareID</code>. To turn of this filter use <code>turnOffSoftwareID</code> param.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">clientMsgID_MR</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Draft messages can be filter using <code>clientMsgID_MR</code> param. This parameter accepts MySQL REGEX. It will list all the messages which matches specified regex.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Examples:</span>
												<div class="schema-property-examples__list">
<span class="example-primitive token string">^ab</span><span class="example-primitive token string">ab</span><span class="example-primitive token string">abc$</span>
</div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">messageIDList</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Draft messages can be filter using <code>messageIDList</code> param. It will return all the draft messages filter using <code>messageIDList</code>.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">1084,1080</span></div>
</div>
</div>
</div>
									<div class="param">
										<div class="param__left">
											<div class="schema-property__name">clientMsgIDList</div>
											<div class="schema-property__type">
												<span class="primitive">string</span>
												<i class="property__attr property__attr--required" data-required="false">optional</i>
</div>
</div>
										<div class="param__right">
											<div class="description">
<p>Draft messages can be filter using <code>clientMsgIDList</code> param. It will return all the draft messages filter using <code>clientMsgIDList</code>.</p>
</div>
											<div class="schema-property-examples">
												<span class="lbl">Example:</span>
												<div class="schema-property-examples__list"><span class="example-primitive token string">safdsdfds123,demoemailclientmsgid</span></div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-12">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">id</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token number">847</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">from</div>
																	<div class="schema-property__type">
																		<span class="primitive">Object</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
															<div class="schema-properties">
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">email</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">unread</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">fromList</div>
																	<div class="schema-property__type">
																		<span class="primitive">Object</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
															<div class="schema-properties">
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">email</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">unread</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">title</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">to</div>
																	<div class="schema-property__type">
																		<span class="primitive">Object</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
															<div class="schema-properties">
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">email</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
</div>
																<div class="schema-property">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema-property__name">unread</div>
																			<div class="schema-property__type">
																				<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">attSize</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">777835</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">attachments</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="boolean">boolean</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token boolean">true</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Example - AuthOK">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/draft HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>                                
$docsmit-&gt;get('/messages/draft'<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">847</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"shc.narola@narolainfotech.com"</span>
      <span class="token punctuation">]</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"1"</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"fromList"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"shc.narola@narolainfotech.com"</span>
      <span class="token punctuation">]</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"1"</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"psh.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
        <span class="token string">"ps.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
        <span class="token string">"demo.narola@gmail.com"</span><span class="token punctuation">,</span>
        <span class="token string">"as.narola@narolainfotech.com"</span>
      <span class="token punctuation">]</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"1"</span><span class="token punctuation">,</span>
        <span class="token string">"1"</span><span class="token punctuation">,</span>
        <span class="token string">"1"</span><span class="token punctuation">,</span>
        <span class="token string">"1"</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token string">"777835"</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token boolean">true</span>
  <span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Example 2 - Bad Email">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/draft HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$email =<span class="token string">"123"</span><span class="token punctuation">;</span>
$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/draft'<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">401 Unauthorized </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"code"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
        <span class="token property">"value"</span><span class="token operator">:</span> <span class="token string">"401"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"message"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
        <span class="token property">"value"</span><span class="token operator">:</span> <span class="token string">"Authentication Failed"</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Example 3 - Client message ID filter">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/draft HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"clientMsgID"</span><span class="token punctuation">]</span> = <span class="token string">"test123"</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/draft'<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">847</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"shc.narola@narolainfotech.com"</span>
      <span class="token punctuation">]</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"1"</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"fromList"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"shc.narola@narolainfotech.com"</span>
      <span class="token punctuation">]</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"1"</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"psh.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
        <span class="token string">"ps.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
        <span class="token string">"demo.narola@gmail.com"</span><span class="token punctuation">,</span>
        <span class="token string">"as.narola@narolainfotech.com"</span>
      <span class="token punctuation">]</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"1"</span><span class="token punctuation">,</span>
        <span class="token string">"1"</span><span class="token punctuation">,</span>
        <span class="token string">"1"</span><span class="token punctuation">,</span>
        <span class="token string">"1"</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token string">"777835"</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token boolean">true</span>
  <span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Example 4 - Turn off softwareID filter">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/draft?turnOffSoftwareID=true HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"turnOffSoftwareID"</span><span class="token punctuation">]</span> = <span class="token string">"true"</span><span class="token punctuation">;</span> // or <span class="token boolean">true</span><span class="token punctuation">,</span><span class="token number">1</span>
$docsmit-&gt;get('/messages/draft'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">847</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"shc.narola@narolainfotech.com"</span>
      <span class="token punctuation">]</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"1"</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"fromList"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"shc.narola@narolainfotech.com"</span>
      <span class="token punctuation">]</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"1"</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"psh.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
        <span class="token string">"ps.narola@narolainfotech.com"</span><span class="token punctuation">,</span>
        <span class="token string">"demo.narola@gmail.com"</span><span class="token punctuation">,</span>
        <span class="token string">"as.narola@narolainfotech.com"</span>
      <span class="token punctuation">]</span><span class="token punctuation">,</span>
      <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">"1"</span><span class="token punctuation">,</span>
        <span class="token string">"1"</span><span class="token punctuation">,</span>
        <span class="token string">"1"</span><span class="token punctuation">,</span>
        <span class="token string">"1"</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token string">"777835"</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token boolean">true</span>
  <span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Example 5 - Filter messages using `clientMsgID_MR`">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/draft HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"clientMsgID_MR"</span><span class="token punctuation">]</span> = <span class="token string">"^[ab]|rs$"</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/draft'<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token string">"847"</span><span class="token punctuation">,</span>
    <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
        <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token string">""</span>
        <span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token string">""</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"fromList"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
        <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token string">""</span>
        <span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token string">""</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
        <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token string">""</span>
        <span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token string">""</span>
        <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token string">"777835"</span><span class="token punctuation">,</span>
    <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token string">"true"</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Example 6 - Filter messages using 'messageIDList'">
									<div class="description method-example__description">
<p>Draft messages can be fileter by specifying messageIDList search params. It will retrieve all the messages which matches the specified messageID List from draft.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/draft HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span>'messageIDList'<span class="token punctuation">]</span> = '<span class="token number">1084</span><span class="token punctuation">,</span><span class="token number">1080</span>'<span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/draft'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1084</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"fromList"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"This is message title from REST API"</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
        <span class="token property">"clientMsgID"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"id"</span><span class="token operator">:</span> <span class="token number">1080</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"fromList"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"This is new message title from Hiren testcase"</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"sop.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
        <span class="token property">"clientMsgID"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Example 7 Filter messages by 'clientMsgIDList'">
									<div class="description method-example__description">
<p>Draft messages can be fileter by specifying clientMsgIDList search params. It will retrieve all the messages which matches the specified clientMsgID List from draft.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/draft HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"clientMsgIDList"</span><span class="token punctuation">]</span> = 'safdsdfds123<span class="token punctuation">,</span>demoemailclientmsgid'<span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/draft'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1079</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"fromList"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"This is message title from REST API"</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
        <span class="token property">"clientMsgID"</span><span class="token operator">:</span> <span class="token string">"safdsdfds123"</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
								<div class="method-example" data-example-name="Example 8 Filter by messageIDs and clientMsgIDList">
									<div class="description method-example__description">
<p>Draft messages can be fileter by specifying clientMsgIDs as well messageIDs search params. It will retrieve all the messages which matches the specified clientMsgID and messageID from draft.</p>
</div>
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/messages/draft HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"messageIDList"</span><span class="token punctuation">]</span> = '<span class="token number">1084</span><span class="token punctuation">,</span><span class="token number">1080</span>'<span class="token punctuation">;</span>
$params<span class="token punctuation">[</span><span class="token string">"clientMsgIDList"</span><span class="token punctuation">]</span> = 'safdsdfds123<span class="token punctuation">,</span>demoemailclientmsgid'<span class="token punctuation">;</span>
$docsmit-&gt;get('/messages/draft'<span class="token punctuation">,</span> $params<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
        <span class="token property">"messageID"</span><span class="token operator">:</span> <span class="token number">1084</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"fromList"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"This is message title from REST API"</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
        <span class="token property">"clientMsgID"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"id"</span><span class="token operator">:</span> <span class="token number">1080</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"fromList"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"This is new message title from Hiren testcase"</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"sop.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
        <span class="token property">"clientMsgID"</span><span class="token operator">:</span> <span class="token null">null</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
        <span class="token property">"id"</span><span class="token operator">:</span> <span class="token number">1079</span><span class="token punctuation">,</span>
        <span class="token property">"from"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"fromList"</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">"email"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"shc.narola@narolainfotech.com"</span>
            <span class="token punctuation">]</span><span class="token punctuation">,</span>
            <span class="token property">"unread"</span><span class="token operator">:</span> <span class="token punctuation">[</span>
                <span class="token string">"1"</span>
            <span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token property">"title"</span><span class="token operator">:</span> <span class="token string">"This is message title from REST API"</span><span class="token punctuation">,</span>
        <span class="token property">"attSize"</span><span class="token operator">:</span> <span class="token number">0</span><span class="token punctuation">,</span>
        <span class="token property">"to"</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
        <span class="token property">"attachments"</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
        <span class="token property">"clientMsgID"</span><span class="token operator">:</span> <span class="token string">"safdsdfds123"</span><span class="token punctuation">,</span>
        <span class="token property">"softwareID"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">]</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="user-account-information" class="spec-item-header" data-entity="method" data-entity-id="user-account-information">
								<a class="head_anchor" href="#user-account-information"></a>User Account Information</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="GET">GET</span>
									<span class="method-url">/account/info</span>
</div>
</div>
							<div class="description description__main">
<p>Returns information about the currently logged in user, including userID, name, email, company, address, timezone, suspended status, credit balance and softwareIDs, as well as other data. The address can be used as a suggestion for a return address for messages with mail recipients.</p>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Success!</p>
</div>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">userID</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">450</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">name</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">Ronny Gold</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">email</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">rgold@goldlewis.com</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">company</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">Gold, Lewis, LLP</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">address1</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">15 E Broad St.</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">address2</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">city</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">Hazleton</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">state</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">PA</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">zip</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">18201</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">softwareIDs</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">12345678901234567890123456789012</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">suspended</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">timezone</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">America/New_York</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">timezoneAsNumber</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="description">
<p>Difference from GMT</p>
</div>
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">UTC/GMT -04:00</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">attachmentFilesMaxLimit</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token number">314572800</span></div>
</div>
</div>
</div>
</div>
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">creditBalance</div>
																	<div class="schema-property__type">
																		<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																<div class="schema-property__right">
																	<div class="schema-property-examples">
																		<span class="lbl">Example:</span>
																		<div class="schema-property-examples__list"><span class="example-primitive token string">81.60</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="401">401</span>
												<span class="response-details__name">401
													<a class="small-link" href="#401">
														<i class="fa fa-link"></i></a></span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Examples</h4>
								<div class="method-example" data-example-name="Example 1">
									<div class="method_example__code"><pre><code class="language-http"><span class="token request-line"><span class="token property">GET</span> http://secure.docsmit.com/api/v1/account/info HTTP/1.1</span> 

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

$docsmit = new DocsmitAPI($email<span class="token punctuation">,</span> $hashedPW<span class="token punctuation">,</span> $softwareID<span class="token punctuation">)</span><span class="token punctuation">;</span>
$docsmit-&gt;get('/account/info'<span class="token punctuation">)</span><span class="token punctuation">;</span>
$status = $docsmit-&gt;status(<span class="token punctuation">)</span><span class="token punctuation">;</span>
$response = $docsmit-&gt;responseBody(<span class="token punctuation">)</span><span class="token punctuation">;</span>

</span><span class="token response-status">HTTP/1.1 <span class="token property">200 OK </span></span>

<span class="token header-name keyword">Content-Type:</span> application/json<span class="token application/json">

<span class="token punctuation">{</span>
    <span class="token property">"userID"</span><span class="token operator">:</span> <span class="token string">"450"</span><span class="token punctuation">,</span>
    <span class="token property">"name"</span><span class="token operator">:</span> <span class="token string">"John User"</span><span class="token punctuation">,</span>
    <span class="token property">"email"</span><span class="token operator">:</span> <span class="token string">"john.user@gmail.com"</span><span class="token punctuation">,</span>
    <span class="token property">"company"</span><span class="token operator">:</span> <span class="token string">"John and Co."</span><span class="token punctuation">,</span>
    <span class="token property">"address"</span><span class="token operator">:</span> <span class="token string">"10 Main St"</span><span class="token punctuation">,</span>
    <span class="token property">"address1"</span><span class="token operator">:</span> <span class="token string">"Suite 210"</span><span class="token punctuation">,</span>
    <span class="token property">"address2"</span><span class="token operator">:</span> <span class="token string">""</span><span class="token punctuation">,</span>
    <span class="token property">"city"</span><span class="token operator">:</span> <span class="token string">"Cleveland"</span><span class="token punctuation">,</span>
    <span class="token property">"state"</span><span class="token operator">:</span> <span class="token string">"OH"</span><span class="token punctuation">,</span>
    <span class="token property">"zip"</span><span class="token operator">:</span> <span class="token string">"12345-1234"</span><span class="token punctuation">,</span>
    <span class="token property">"softwareIDs"</span><span class="token operator">:</span> <span class="token string">"12345678901234567890123456789012"</span><span class="token punctuation">,</span>
    <span class="token property">"suspended"</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    <span class="token property">"timezone"</span><span class="token operator">:</span> <span class="token string">"Eastern/US"</span><span class="token punctuation">,</span>
    <span class="token property">"attachmentFilesMaxLimit"</span><span class="token operator">:</span> <span class="token number">314572800</span><span class="token punctuation">,</span>
    <span class="token property">"bullpenFilesMaxLimit"</span><span class="token operator">:</span> <span class="token number">104857600</span><span class="token punctuation">,</span>
    <span class="token property">"creditBalance"</span><span class="token operator">:</span> <span class="token string">"81.60"</span>
<span class="token punctuation">}</span></span></code></pre></div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="misc_group" class="spec-item-header" data-entity="group" data-entity-id="misc_group">
								<a class="head_anchor" href="#misc_group"></a>Misc</div>
							<div class="description description__main">
<p>Miscellaneous endpoints</p>
</div>
							<div class="method-section">
								<div class="group-methods-summary">
									<div class="row">
										<div class="col-md-5">
											<a class="method-ref" href="#cass-check">CASS check</a>
</div>
										<div class="col-md-7" style="padding-left: 0; padding-right: 0">
											<span class="http-method small" data-method="GET">GET</span>
											<span class="method-url">/utils/multiCASS</span>
</div>
</div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="cass-check" class="spec-item-header" data-entity="method" data-entity-id="cass-check">
								<a class="head_anchor" href="#cass-check"></a>CASS check</div>
							<div class="method-section">
								<div class="method-http-details">
									<span class="http-method" data-method="GET">GET</span>
									<span class="method-url">/utils/multiCASS</span>
</div>
</div>
							<div class="description description__main">
<p>CASS validate and correct an address</p>
</div>
							<div class="method-section">
								<h4>Request body</h4>
								<div class="request-body">
									<div class="schema">
										<div class="schema-property__definition">
											<div class="schema-property__left">
												<div class="schema__type">
													<span class="primitive">Object</span>
</div>
</div>
											<div class="schema-property__right"></div>
</div>
										<div class="schema-properties">
											<div class="schema-property">
												<div class="schema-property__definition">
													<div class="schema-property__left">
														<div class="schema-property__name">addresses</div>
														<div class="schema-property__type">
															<span class="primitive">Array</span>
</div>
</div>
													<div class="schema-property__right"></div>
</div>
												<div class="schema-array-items">
													<div class="schema">
														<div class="schema-property__definition">
															<div class="schema-property__left">
																<div class="schema__type">
																	<span class="primitive">Object</span>
</div>
</div>
															<div class="schema-property__right"></div>
</div>
														<div class="schema-properties">
															<div class="schema-property">
																<div class="schema-property__definition">
																	<div class="schema-property__left">
																		<div class="schema-property__name">firstName</div>
																		<div class="schema-property__type">
																			<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																	<div class="schema-property__right"></div>
</div>
</div>
															<div class="schema-property">
																<div class="schema-property__definition">
																	<div class="schema-property__left">
																		<div class="schema-property__name">lastName</div>
																		<div class="schema-property__type">
																			<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																	<div class="schema-property__right"></div>
</div>
</div>
															<div class="schema-property">
																<div class="schema-property__definition">
																	<div class="schema-property__left">
																		<div class="schema-property__name">address1</div>
																		<div class="schema-property__type">
																			<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																	<div class="schema-property__right"></div>
</div>
</div>
															<div class="schema-property">
																<div class="schema-property__definition">
																	<div class="schema-property__left">
																		<div class="schema-property__name">address2</div>
																		<div class="schema-property__type">
																			<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																	<div class="schema-property__right"></div>
</div>
</div>
															<div class="schema-property">
																<div class="schema-property__definition">
																	<div class="schema-property__left">
																		<div class="schema-property__name">city</div>
																		<div class="schema-property__type">
																			<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																	<div class="schema-property__right"></div>
</div>
</div>
															<div class="schema-property">
																<div class="schema-property__definition">
																	<div class="schema-property__left">
																		<div class="schema-property__name">state</div>
																		<div class="schema-property__type">
																			<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																	<div class="schema-property__right"></div>
</div>
</div>
															<div class="schema-property">
																<div class="schema-property__definition">
																	<div class="schema-property__left">
																		<div class="schema-property__name">zip</div>
																		<div class="schema-property__type">
																			<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																	<div class="schema-property__right"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<h4>Responses</h4>
								<div class="method-responses">
									<div class="method-response">
										<div class="row response-details">
											<div class="col-md-5" style="padding-right:0">
												<span class="response-details__status" data-response-status="200">200</span>
												<span class="response-details__name">OK</span>
</div>
											<div class="col-md-7">
												<div class="description">
<p>Returns the corrected version (or original, if there were no corrections).  If address is bad and cannot be fixed, then nonMailable is returned true.</p>
</div>
</div>
</div>
										<div class="response-content">
											<div class="response-body">
												<h5 class="response-body__header">Body</h5>
												<div class="schema">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema__type">
																<span class="primitive">Object</span>
</div>
</div>
														<div class="schema-property__right"></div>
</div>
													<div class="schema-properties">
														<div class="schema-property">
															<div class="schema-property__definition">
																<div class="schema-property__left">
																	<div class="schema-property__name">addresses</div>
																	<div class="schema-property__type">
																		<span class="primitive">Array</span>
</div>
</div>
																<div class="schema-property__right"></div>
</div>
															<div class="schema-array-items">
																<div class="schema">
																	<div class="schema-property__definition">
																		<div class="schema-property__left">
																			<div class="schema__type">
																				<span class="primitive">Object</span>
</div>
</div>
																		<div class="schema-property__right"></div>
</div>
																	<div class="schema-properties">
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">name</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																				<div class="schema-property__right">
																					<div class="description">
<p>Contains first and last together (can ignore)</p>
</div>
</div>
</div>
</div>
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">address1</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																				<div class="schema-property__right"></div>
</div>
</div>
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">address2</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																				<div class="schema-property__right"></div>
</div>
</div>
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">city</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																				<div class="schema-property__right"></div>
</div>
</div>
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">state</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																				<div class="schema-property__right"></div>
</div>
</div>
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">zip</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																				<div class="schema-property__right"></div>
</div>
</div>
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">coarseCorrectionMsg</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																				<div class="schema-property__right">
																					<div class="description">
<p>A description of what correction was made</p>
</div>
</div>
</div>
</div>
																		<div class="schema-property">
																			<div class="schema-property__definition">
																				<div class="schema-property__left">
																					<div class="schema-property__name">nonMailable</div>
																					<div class="schema-property__type">
																						<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
																				<div class="schema-property__right">
																					<div class="description">
<p>indicates if the request and response are invalid and cannot be mailed to</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="responses_group" class="spec-item-header" data-entity="group" data-entity-id="responses_group">
								<a class="head_anchor" href="#responses_group"></a>Data Types and Responses</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="physicalParties" class="spec-item-header" data-entity="schema" data-entity-id="physicalParties">
								<a class="head_anchor" href="#physicalParties"></a>physicalParties</div>
							<div class="method-section">
								<div class="description description__main">
<p>A number of methods, such as <a href="#new-message" class="link-internal link-method" target="_self">/messages/new</a> and <a href="#add-party" class="link-internal link-method" target="_self">/messages/{messageID}/party</a> use the physicalParties type.  It holds one or more recipiends of physical/snail mail.</p>
<p>The physicalParties type is an array of the <a href="#MailAddress" class="link-internal link-schema" target="_self">MailAddress</a> type - that is, it's a list of recipients.</p>
</div>
								<div class="schema">
									<div class="schema-property__definition">
										<div class="schema-property__left">
											<div class="schema__type">
												<span class="primitive">Array</span>
												<span class="primitive" style="text-transform: lowercase">of</span>
												<a class="item-ref" href="#MailAddress">MailAddress</a>
</div>
</div>
										<div class="schema-property__right"></div>
</div>
									<div class="schema-array-items"></div>
</div>
</div>
							<div class="method-section">
								<div class="spec-item-references">
									<i>Methods:</i>
									<a href="#add-party">Add Party</a>
									<a href="#new-message">New Message</a>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div id="MailAddress" class="spec-item-header" data-entity="schema" data-entity-id="MailAddress">
								<a class="head_anchor" href="#MailAddress"></a>MailAddress</div>
							<div class="method-section">
								<div class="description description__main">
<p>A mailAddress holds information about one mailpiece.  It includes the name company and address of the mail recipient and how the mailpiece is being sent to that recipient.  When a mailAddress is added to a message, such as by <code>/messages/new</code>, it is given a MailAddressID.</p>
</div>
								<div class="schema">
									<div class="schema-property__definition">
										<div class="schema-property__left">
											<div class="schema__type">
												<span class="primitive">Object</span>
</div>
</div>
										<div class="schema-property__right"></div>
</div>
									<div class="schema-properties">
										<div class="schema-property">
											<div class="schema-property__definition">
												<div class="schema-property__left">
													<div class="schema-property__name">firstName</div>
													<div class="schema-property__type">
														<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
												<div class="schema-property__right">
													<div class="description">
<p>There must be either an organization or a firstName and lastName.</p>
</div>
													<div class="schema-property-examples">
														<span class="lbl">Example:</span>
														<div class="schema-property-examples__list"><span class="example-primitive token string">John</span></div>
</div>
</div>
</div>
</div>
										<div class="schema-property">
											<div class="schema-property__definition">
												<div class="schema-property__left">
													<div class="schema-property__name">lastName</div>
													<div class="schema-property__type">
														<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
												<div class="schema-property__right">
													<div class="description">
<p>There must be either an organization or a firstName and lastName.</p>
</div>
													<div class="schema-property-examples">
														<span class="lbl">Example:</span>
														<div class="schema-property-examples__list"><span class="example-primitive token string">Backham</span></div>
</div>
</div>
</div>
</div>
										<div class="schema-property">
											<div class="schema-property__definition">
												<div class="schema-property__left">
													<div class="schema-property__name">organization</div>
													<div class="schema-property__type">
														<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
												<div class="schema-property__right">
													<div class="description">
<p>There must be either an organization or a firstName and lastName.</p>
</div>
													<div class="schema-property-examples">
														<span class="lbl">Example:</span>
														<div class="schema-property-examples__list"><span class="example-primitive token string">abc &amp; co.</span></div>
</div>
</div>
</div>
</div>
										<div class="schema-property">
											<div class="schema-property__definition">
												<div class="schema-property__left">
													<div class="schema-property__name">address1</div>
													<div class="schema-property__type">
														<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
												<div class="schema-property__right">
													<div class="schema-property-examples">
														<span class="lbl">Example:</span>
														<div class="schema-property-examples__list"><span class="example-primitive token string">50 Journal Square, 8th Fl</span></div>
</div>
</div>
</div>
</div>
										<div class="schema-property">
											<div class="schema-property__definition">
												<div class="schema-property__left">
													<div class="schema-property__name">address2</div>
													<div class="schema-property__type">
														<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
												<div class="schema-property__right">
													<div class="description">
<p>(optional) The second line of address.</p>
</div>
													<div class="schema-property-examples">
														<span class="lbl">Example:</span>
														<div class="schema-property-examples__list"><span class="example-primitive token string">8th Floor</span></div>
</div>
</div>
</div>
</div>
										<div class="schema-property">
											<div class="schema-property__definition">
												<div class="schema-property__left">
													<div class="schema-property__name">city</div>
													<div class="schema-property__type">
														<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
												<div class="schema-property__right">
													<div class="schema-property-examples">
														<span class="lbl">Example:</span>
														<div class="schema-property-examples__list"><span class="example-primitive token string">Jersey City</span></div>
</div>
</div>
</div>
</div>
										<div class="schema-property">
											<div class="schema-property__definition">
												<div class="schema-property__left">
													<div class="schema-property__name">state</div>
													<div class="schema-property__type">
														<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
												<div class="schema-property__right">
													<div class="description">
<p>Two digit US state abbreviation</p>
</div>
													<div class="view-param-attr">
														<span class="lbl">Pattern:</span><span class="val">cc</span>
</div>
													<div class="schema-property-examples">
														<span class="lbl">Example:</span>
														<div class="schema-property-examples__list"><span class="example-primitive token string">NJ</span></div>
</div>
</div>
</div>
</div>
										<div class="schema-property">
											<div class="schema-property__definition">
												<div class="schema-property__left">
													<div class="schema-property__name">postalCode</div>
													<div class="schema-property__type">
														<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
												<div class="schema-property__right">
													<div class="view-param-attr">
														<span class="lbl">Pattern:</span><span class="val">nnnnn or nnnnn-nnnn</span>
</div>
													<div class="schema-property-examples">
														<span class="lbl">Example:</span>
														<div class="schema-property-examples__list"><span class="example-primitive token string">07306 or 07306-1234</span></div>
</div>
</div>
</div>
</div>
										<div class="schema-property">
											<div class="schema-property__definition">
												<div class="schema-property__left">
													<div class="schema-property__name">sendType</div>
													<div class="schema-property__type">
														<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
												<div class="schema-property__right">
													<div class="view-param-attr">
														<span class="lbl">Enumeration:</span>
														<div class="schema-property-enum">
															<div class="row view-row schema-property-enum__value">
																<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																	<span class="val">First Class</span>
</div>
																<div class="col-md-7 description"></div>
</div>
															<div class="row view-row schema-property-enum__value">
																<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																	<span class="val">Certified, Electronic Return Receipt</span>
</div>
																<div class="col-md-7 description"></div>
</div>
															<div class="row view-row schema-property-enum__value">
																<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																	<span class="val">Certified, Return Receipt</span>
</div>
																<div class="col-md-7 description"></div>
</div>
															<div class="row view-row schema-property-enum__value">
																<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																	<span class="val">Certified</span>
</div>
																<div class="col-md-7 description"></div>
</div>
															<div class="row view-row schema-property-enum__value">
																<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																	<span class="val">Priority Mail</span>
</div>
																<div class="col-md-7 description"></div>
</div>
															<div class="row view-row schema-property-enum__value">
																<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																	<span class="val">Priority Mail with Signature</span>
</div>
																<div class="col-md-7 description"></div>
</div>
															<div class="row view-row schema-property-enum__value">
																<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																	<span class="val">Priority Mail, Flat Rate Env</span>
</div>
																<div class="col-md-7 description"></div>
</div>
</div>
</div>
</div>
</div>
</div>
										<div class="schema-property">
											<div class="schema-property__definition">
												<div class="schema-property__left">
													<div class="schema-property__name">envelope</div>
													<div class="schema-property__type">
														<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
												<div class="schema-property__right">
													<div class="view-param-attr">
														<span class="lbl">Enumeration:</span>
														<div class="schema-property-enum">
															<div class="row view-row schema-property-enum__value">
																<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																	<span class="val">#10</span>
</div>
																<div class="col-md-7 description">
<p>#10 Envelope (for First Class or Certified)</p>
</div>
</div>
															<div class="row view-row schema-property-enum__value">
																<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																	<span class="val">Flat</span>
</div>
																<div class="col-md-7 description">
<p>Flat Envelope (for First Class or Certified)</p>
</div>
</div>
															<div class="row view-row schema-property-enum__value">
																<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																	<span class="val">Priority Flat</span>
</div>
																<div class="col-md-7 description">
<p>Priority Flat Rate Envelope Letter sized</p>
</div>
</div>
															<div class="row view-row schema-property-enum__value">
																<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																	<span class="val">Priority Padded</span>
</div>
																<div class="col-md-7 description">
<p>Priority Flat Rate Envelope Letter sized</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
										<div class="schema-property">
											<div class="schema-property__definition">
												<div class="schema-property__left">
													<div class="schema-property__name">sided</div>
													<div class="schema-property__type">
														<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
												<div class="schema-property__right">
													<div class="description">
<p>(optional: default is single sided) single sided or double sided</p>
</div>
													<div class="view-param-attr">
														<span class="lbl">Enumeration:</span>
														<div class="schema-property-enum">
															<div class="row view-row schema-property-enum__value">
																<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																	<span class="val">1</span>
</div>
																<div class="col-md-7 description">
<p>Single Sided</p>
</div>
</div>
															<div class="row view-row schema-property-enum__value">
																<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																	<span class="val">2</span>
</div>
																<div class="col-md-7 description">
<p>Double Sided</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
										<div class="schema-property">
											<div class="schema-property__definition">
												<div class="schema-property__left">
													<div class="schema-property__name">plusRegular</div>
													<div class="schema-property__type">
														<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
												<div class="schema-property__right">
													<div class="description">
<p>(Optional. Default = 0) This indicates that a copy is also to be sent out by first class mail in addition to the method specified in sendType. If sendType is "First Class", plusRegular is ignored (because it is already going out First Class). Permitted values: 1 or 0 or true or false.</p>
</div>
													<div class="view-param-attr">
														<span class="lbl">Enumeration:</span>
														<div class="schema-property-enum">
															<div class="row view-row schema-property-enum__value">
																<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																	<span class="val">0</span>
</div>
																<div class="col-md-7 description"></div>
</div>
															<div class="row view-row schema-property-enum__value">
																<div class="col-md-5" style="padding-left:0px;padding-right:0px">
																	<span class="val">1</span>
</div>
																<div class="col-md-7 description"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
							<div class="method-section">
								<div class="spec-item-references">
									<i>Types:</i>
									<a href="#physicalParties">physicalParties</a>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div class="common-response common-response--first" id="400">
								<div class="row response-details">
									<div class="col-md-5" style="padding-right:0">
										<span class="response-details__status" data-response-status="400">400</span>
										<span class="response-details__name">400</span>
</div>
									<div class="col-md-7">
										<div class="description">
<p>Problem with one of the request parameters.</p>
</div>
</div>
</div>
								<div class="row">
									<div class="col-md-11">
										<div class="spec-item-references">
											<label class="more" for="hider_400" onclick="hide(this)">Show usage (5)</label>
											<input type="checkbox" class="more" id="hider_400"><a href="#add-party">Add Party</a>
											<a href="#generate-tracking-id">Generate tracking ID (DONT USE RIGHT NOW)</a>
											<a href="#get-a-message">Get a message</a>
											<a href="#send-message">Send Message</a>
											<a href="#upload-a-file">Upload a file</a>
</div>
</div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div class="common-response" id="401">
								<div class="row response-details">
									<div class="col-md-5" style="padding-right:0">
										<span class="response-details__status" data-response-status="401">401</span>
										<span class="response-details__name">401</span>
</div>
									<div class="col-md-7">
										<div class="description">
<p>Email and password or token was not accepted.</p>
</div>
</div>
</div>
								<div class="response-content">
									<div class="response-body">
										<h5 class="response-body__header">Body</h5>
										<div class="schema">
											<div class="schema-property__definition">
												<div class="schema-property__left">
													<div class="schema__type">
														<span class="primitive">Object</span>
</div>
</div>
												<div class="schema-property__right"></div>
</div>
											<div class="schema-properties">
												<div class="schema-property">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema-property__name">code</div>
															<div class="schema-property__type">
																<span class="primitive" data-schema-type="number">number</span>
</div>
</div>
														<div class="schema-property__right">
															<div class="schema-property-examples">
																<span class="lbl">Example:</span>
																<div class="schema-property-examples__list"><span class="example-primitive token number">401</span></div>
</div>
</div>
</div>
</div>
												<div class="schema-property">
													<div class="schema-property__definition">
														<div class="schema-property__left">
															<div class="schema-property__name">message</div>
															<div class="schema-property__type">
																<span class="primitive" data-schema-type="string">string</span>
</div>
</div>
														<div class="schema-property__right">
															<div class="schema-property-examples">
																<span class="lbl">Example:</span>
																<div class="schema-property-examples__list"><span class="example-primitive token string">Authentication Failed</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
								<div class="row">
									<div class="col-md-11">
										<div class="spec-item-references">
											<label class="more" for="hider_401" onclick="hide(this)">Show usage (16)</label>
											<input type="checkbox" class="more" id="hider_401"><a href="#add-party">Add Party</a>
											<a href="#download-zip-attachment">Download Zip attachment</a>
											<a href="#download-file-attachment">Download file attachment</a>
											<a href="#draft-list">Draft list</a>
											<a href="#generate-tracking-id">Generate tracking ID (DONT USE RIGHT NOW)</a>
											<a href="#get-certification">Get Certification</a>
											<a href="#get-recipients-and-their-statuses-parties">Get Recipients and their statuses - parties</a>
											<a href="#get-a-message">Get a message</a>
											<a href="#get-attachments-list">Get attachments list</a>
											<a href="#get-the-price-and-the-details">Get the price and the details</a>
											<a href="#inbox-list">Inbox list</a>
											<a href="#message-transaction-history">Message transaction history</a>
											<a href="#new-message">New Message</a>
											<a href="#sent-list">Sent list</a>
											<a href="#sign-for-message">Sign for message</a>
											<a href="#user-account-information">User Account Information</a>
</div>
</div>
</div>
</div>
</div>
</div>
					<div class="spec-item">
						<div class="spec-item-copy">
							<div class="common-response common-response--last" id="403">
								<div class="row response-details">
									<div class="col-md-5" style="padding-right:0">
										<span class="response-details__status" data-response-status="403">403</span>
										<span class="response-details__name">403</span>
</div>
									<div class="col-md-7">
										<div class="description">
<p>User does not have authority to perform this action on this message.</p>
</div>
</div>
</div>
								<div class="row">
									<div class="col-md-11">
										<div class="spec-item-references">
											<label class="more" for="hider_403" onclick="hide(this)">Show usage (7)</label>
											<input type="checkbox" class="more" id="hider_403"><a href="#add-party">Add Party</a>
											<a href="#generate-tracking-id">Generate tracking ID (DONT USE RIGHT NOW)</a>
											<a href="#get-certification">Get Certification</a>
											<a href="#get-a-message">Get a message</a>
											<a href="#send-message">Send Message</a>
											<a href="#sent-list">Sent list</a>
											<a href="#upload-a-file">Upload a file</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>