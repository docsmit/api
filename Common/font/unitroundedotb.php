<?php
$type = 'TrueType';
$name = 'UnitRoundedOT-Bold';
$desc = array('Ascent'=>793,'Descent'=>-207,'CapHeight'=>695,'Flags'=>32,'FontBBox'=>'[-65 -251 1810 959]','ItalicAngle'=>0,'StemV'=>120,'MissingWidth'=>521);
$up = -50;
$ut = 106;
$cw = array(
	chr(0)=>521,chr(1)=>521,chr(2)=>521,chr(3)=>521,chr(4)=>521,chr(5)=>521,chr(6)=>521,chr(7)=>521,chr(8)=>521,chr(9)=>521,chr(10)=>521,chr(11)=>521,chr(12)=>521,chr(13)=>521,chr(14)=>521,chr(15)=>521,chr(16)=>521,chr(17)=>521,chr(18)=>521,chr(19)=>521,chr(20)=>521,chr(21)=>521,
	chr(22)=>521,chr(23)=>521,chr(24)=>521,chr(25)=>521,chr(26)=>521,chr(27)=>521,chr(28)=>521,chr(29)=>521,chr(30)=>521,chr(31)=>521,' '=>226,'!'=>298,'"'=>478,'#'=>538,'$'=>507,'%'=>873,'&'=>667,'\''=>260,'('=>359,')'=>359,'*'=>491,'+'=>580,
	','=>290,'-'=>322,'.'=>284,'/'=>380,'0'=>633,'1'=>478,'2'=>517,'3'=>516,'4'=>606,'5'=>536,'6'=>588,'7'=>508,'8'=>585,'9'=>588,':'=>318,';'=>318,'<'=>450,'='=>566,'>'=>450,'?'=>450,'@'=>862,'A'=>573,
	'B'=>575,'C'=>540,'D'=>610,'E'=>495,'F'=>469,'G'=>611,'H'=>608,'I'=>383,'J'=>304,'K'=>571,'L'=>472,'M'=>746,'N'=>611,'O'=>646,'P'=>562,'Q'=>646,'R'=>583,'S'=>544,'T'=>512,'U'=>610,'V'=>584,'W'=>798,
	'X'=>572,'Y'=>554,'Z'=>511,'['=>322,'\\'=>381,']'=>323,'^'=>551,'_'=>377,'`'=>522,'a'=>484,'b'=>527,'c'=>432,'d'=>528,'e'=>502,'f'=>320,'g'=>517,'h'=>521,'i'=>292,'j'=>292,'k'=>502,'l'=>286,'m'=>755,
	'n'=>521,'o'=>519,'p'=>527,'q'=>528,'r'=>364,'s'=>428,'t'=>339,'u'=>522,'v'=>491,'w'=>704,'x'=>502,'y'=>490,'z'=>426,'{'=>357,'|'=>252,'}'=>357,'~'=>543,chr(127)=>521,chr(128)=>594,chr(129)=>521,chr(130)=>272,chr(131)=>428,
	chr(132)=>501,chr(133)=>768,chr(134)=>412,chr(135)=>412,chr(136)=>522,chr(137)=>1242,chr(138)=>544,chr(139)=>331,chr(140)=>816,chr(141)=>521,chr(142)=>511,chr(143)=>521,chr(144)=>521,chr(145)=>272,chr(146)=>272,chr(147)=>501,chr(148)=>501,chr(149)=>409,chr(150)=>548,chr(151)=>727,chr(152)=>522,chr(153)=>807,
	chr(154)=>428,chr(155)=>331,chr(156)=>791,chr(157)=>521,chr(158)=>426,chr(159)=>554,chr(160)=>226,chr(161)=>299,chr(162)=>435,chr(163)=>524,chr(164)=>596,chr(165)=>592,chr(166)=>252,chr(167)=>534,chr(168)=>522,chr(169)=>734,chr(170)=>469,chr(171)=>561,chr(172)=>533,chr(173)=>322,chr(174)=>540,chr(175)=>522,
	chr(176)=>412,chr(177)=>581,chr(178)=>374,chr(179)=>376,chr(180)=>522,chr(181)=>522,chr(182)=>592,chr(183)=>285,chr(184)=>522,chr(185)=>348,chr(186)=>502,chr(187)=>561,chr(188)=>1020,chr(189)=>1005,chr(190)=>1031,chr(191)=>450,chr(192)=>573,chr(193)=>573,chr(194)=>573,chr(195)=>573,chr(196)=>573,chr(197)=>573,
	chr(198)=>821,chr(199)=>540,chr(200)=>495,chr(201)=>495,chr(202)=>495,chr(203)=>495,chr(204)=>383,chr(205)=>383,chr(206)=>383,chr(207)=>383,chr(208)=>615,chr(209)=>611,chr(210)=>646,chr(211)=>646,chr(212)=>646,chr(213)=>646,chr(214)=>646,chr(215)=>575,chr(216)=>646,chr(217)=>610,chr(218)=>610,chr(219)=>610,
	chr(220)=>610,chr(221)=>554,chr(222)=>563,chr(223)=>557,chr(224)=>484,chr(225)=>484,chr(226)=>484,chr(227)=>484,chr(228)=>484,chr(229)=>484,chr(230)=>742,chr(231)=>432,chr(232)=>502,chr(233)=>502,chr(234)=>502,chr(235)=>502,chr(236)=>292,chr(237)=>292,chr(238)=>292,chr(239)=>292,chr(240)=>521,chr(241)=>521,
	chr(242)=>519,chr(243)=>519,chr(244)=>519,chr(245)=>519,chr(246)=>519,chr(247)=>581,chr(248)=>519,chr(249)=>522,chr(250)=>522,chr(251)=>522,chr(252)=>522,chr(253)=>490,chr(254)=>526,chr(255)=>490);
$enc = 'cp1252';
$uv = array(0=>array(0,128),128=>8364,130=>8218,131=>402,132=>8222,133=>8230,134=>array(8224,2),136=>710,137=>8240,138=>352,139=>8249,140=>338,142=>381,145=>array(8216,2),147=>array(8220,2),149=>8226,150=>array(8211,2),152=>732,153=>8482,154=>353,155=>8250,156=>339,158=>382,159=>376,160=>array(160,96));
$file = 'UnitRoundedOT-Bold.z';
$originalsize = 33856;
$subsetted = true;
?>
