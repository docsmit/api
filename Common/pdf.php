<?php
include "./Common/FPDFPlus.php";
Class PDF extends FPDFPlus {

    var $title;

//set title
    function SetTitle($title,$isUTF8 = false) {
        $this->title = $title;
    }

//set title
    function SetDate($date) {
        $this->date = $date;
    }

// Page header
    function Header() {
        // Logo
        $this->Image('./assets/logoonwhite.png', 10, 6, 50); //('./assets/docsmitLrg-bmp.png',10,6,50);
        $this->SetFont('Arial', 'B', 18);
        // Move to the right
        $this->Cell(70);
        // Title
        $this->Cell(75, 10, $this->title, 0, 0, 'C');
        // Line break
        $this->Ln(20);
    }

// Page footer
    function Footer() {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
        $this->Setx($this->lMargin);
        //$this->Cell(30,10,"Generated ". date("n/j/y, g:i:s A"),0,0);
        $this->Cell(30, 10, "Generated " . $this->date, 0, 0);
    }

}
?>