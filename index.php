<?php

require_once dirname(__FILE__) . '/share/bootstrap.php';

//include_once '../demo/REST/DocsmitAPI.php';
use API\Middleware\TokenOverBasicAuth;
use API\Exception;
use API\Exception\ValidationException;

// to avoid Warning</b>:  Uncaught exception 'ErrorException' with message 'require_once(utils/util.php): failed to open stream: No such file or directory' in /home/robert/staging_secure/Classes/Docsmail.php:6
set_include_path(get_include_path() . ':' . __DIR__ . '/../');


// General API group
$app->get('/', function () {
        echo "<h1>Welcome to Docsmit REST API V1 !!!</h1>";
});

// Group for API Version 1
$app->group(
        '/v1',
        // API Methods
        function () use ($app, $log) {

// Docsmit API v1 root
    $app->get('/', function () {
        echo "Welcome to Docsmit REST API V1 !!!";
        return;
    });


// Get token using post data as json
    $app->post('/token', function () use ($app, $log) {
        $body = $app->request()->getBody();
DEBLOG("token request body = " . print_r($body, true));
        $errors = $app->validateToken($body);
        if (!empty($errors))
            throw new ValidationException("Invalid authentication data", 400, $errors);
        $email = $body['email'];
        $password = strtolower($body['password']);
        $softwareID = $body['softwareID'];
        $apiToken = new \Docsmit\APIToken($app->mysqli, $email, $password, $softwareID);
        if (!$apiToken->getToken()) {
            throw new ValidationException("Invalid Authentication", 401);
        }
        $output["token"] = $apiToken->token();

        $user = new \Docsmit\User($app->mysqli,$apiToken->userID());
        $output["userID"] = $user->userID();
        $output["name"] = $user->name();
        $output["email"] = $user->email();
        $output["company"] = $user->company();
        $output["address1"] = $user->address1();
        $output["address2"] = $user->address2();
        $output["city"] = $user->city();
        $output["state"] = $user->state();
        $output["zip"] = $user->zip();
        $output["timezone"] = $user->timezone();
        $output["timezoneAsNumber"] = timezoneNumberFind($user->timezone());
        $output["attachmentFilesMaxLimit"] = MAX_UPLOAD_ATTACHMENT_FILESIZE * MEGABYTE; // 300 mb;
        $userSettings = new \Docsmit\UserSettings($app->mysqli, $user->userID());
        $output["creditBalance"] = $userSettings->creditBalance();

        echo json_encode($output, JSON_PRETTY_PRINT);
        return;
    });

// Delete token
    $app->delete('/token', function () use ($app, $log) {
        echo "DELETE token for user " . $app->user->email() . "\n";
        $body = $app->request()->getBody();
        if (!isset($body['softwareID']))
            throw new ValidationException("Must include softwareID", 400);
        $apiToken = new \Docsmit\APIToken($app->mysqli, $app->user->email(), '', $body['softwareID']);
        if ($apiToken->deleteToken())
            echo json_encode(array ("reply", "Token deleted."), JSON_PRETTY_PRINT);
        else
            echo json_encode(array ("reply", "Token not found."), JSON_PRETTY_PRINT);
        return;
    });

// Group for utils API
    $app->group('/utils', function () use ($app, $log) {
        $app->post('/multiCASS', function () use ($app, $log) {
            $post = (object) $app->request()->getBody();
            DEBLOG("multiCASS :".print_r($post,true));
            if(!isset($post->addresses))
                throw new ValidationException("Invalid params", 400);

            $addresses = new \C2M\AddressList();
            //pr($get->addresses,1);
            foreach ($post->addresses as $getAddress) {
                $getAddress = (object)$getAddress;
                $address = new \C2M\AddressCorrection();
                $name = "";
                if(empty($getAddress))
                    continue;

                if(isset($getAddress->firstName) )
                    $name .= $getAddress->firstName;
                if(isset($getAddress->lastName))
                    $name .= !empty($name) ? " ".$getAddress->lastName : $getAddress->lastName;

                $address->name = $name;

                if(isset($getAddress->address1))
                $address->address1 = $getAddress->address1;

                if(isset($getAddress->city))
                $address->city = $getAddress->city;

                if(isset($getAddress->state))
                $address->state = $getAddress->state;

                if(isset($getAddress->zip))
                $address->zip = $getAddress->zip;

                $addresses->add($address);
            }
            $c2mAPI = new \C2M\RestAPI(C2M_USERNAME, C2M_PASSWORD);
            $correction_result = $c2mAPI->addressCorrection($addresses);
            $response = simplexml_load_string($correction_result);
            //pr($response);
            if($response->status != 0)
                throw new ValidationException("Error from C2M response.", 400);

            $arr = array();
            foreach($response->addresses->address as $addr)
            {
                $name = explode(" ",$addr[0]->name);
                $temp = array(
                "firstName"=> !empty($name[0])?$name[0]: "",
                "lastName"=> !empty($name[1])?$name[1]: "",
                "address1"=> (string) $addr[0]->address1,
                "address2"=> (string) $addr[0]->address2,
                "city"=>(string)$addr[0]->city,
                "state"=>(string)$addr[0]->state,
                "zip"=>(string)$addr[0]->zip,
                "coarseCorrectionMessage"=>(string)$addr[0]->coarseCorrectionMessage,
                "nonMailable"=>(string)$addr[0]->nonMailable,
                );
                $arr[] = $temp;
            }

            $output["addresses"] = $arr;
            //print "<pre>" . print_r(simplexml_load_string($correction_result),true) . "</pre>";
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });
    });

// Group for messages API
    $app->group('/messages', function () use ($app, $log) {
// Message - Inbox
        $app->get('/inbox', function () use ($app, $log) {
            $get = $app->request()->get();

            $searchParams = null;
            if (isset($get["unsigned"])) {
                $searchTerm = $get["unsigned"];
                $validSearchParamsUnsigned = array("true", "false", "1", "0");
                if (!in_array($searchTerm, $validSearchParamsUnsigned)) {
                    throw new ValidationException("Invalid value supplied for `unsigned` parameter. valid inputs for unsigned param are " . implode(", ", $validSearchParamsUnsigned), 403);
                }

                if ($searchTerm == "true" || $searchTerm == 1)
                    $searchParams = " AND (parties.SignedFor IS NULL OR parties.SignedFor = '0000-00-00 00:00:00') ";
                elseif ($searchTerm == "false" || $searchTerm == 0)
                    $searchParams = " AND (parties.SignedFor IS NOT NULL OR parties.SignedFor <> '0000-00-00 00:00:00') ";
            }
            $clientMsgID = 1;
            if (!empty($get["clientMsgID"])) {
                $clientMsgID = $app->mysqli->escape_string($get["clientMsgID"]);
                $searchParams .= " AND messages.ClientMsgID = ?";
            } else {
                $searchParams .= " AND ? ";
            }

            if (!empty($get["turnOffSoftwareID"])) {
                $searchTerm = $app->mysqli->escape_string($get["turnOffSoftwareID"]);
                $validSearchParamsTurnOffSoftwareID = array("true", "false", "1", "0");
                if (!in_array($searchTerm, $validSearchParamsTurnOffSoftwareID)) {
                    throw new ValidationException("Invalid value supplied for `turnOffSoftwareID` parameter. valid inputs for turnOffSoftwareID param are " . implode(", ", $validSearchParamsTurnOffSoftwareID), 403);
                }
                $softwareID = 1;
                $searchParams .= " AND ? ";
            } else {
                $softwareID = !empty($app->APIToken->softwareID()) ? $app->APIToken->softwareID() : null;
                $searchParams .= " AND messages.softwareID = ?";
            }

            if (!empty($get["messageIDList"]) && !empty($get["clientMsgIDList"])) {
                $messageIDList = $app->mysqli->escape_string($get["messageIDList"]);
                $searchParams .= " AND (messages.message_id IN (" . $messageIDList . ")";

                $clientMsgIDList = explode(',', $app->mysqli->escape_string($get["clientMsgIDList"]));
                $searchParams .= " OR messages.ClientMsgID IN ('" . implode("','", $clientMsgIDList) . "'))";
            } else {
                if (!empty($get["messageIDList"])) {
                    $messageIDList = $app->mysqli->escape_string($get["messageIDList"]);
                    $searchParams .= " AND messages.message_id IN (" . $messageIDList . ")";
                }

                if (!empty($get["clientMsgIDList"])) {
                    $clientMsgIDList = explode(',', $app->mysqli->escape_string($get["clientMsgIDList"]));
                    $searchParams .= " AND messages.ClientMsgID IN ('" . implode("','", $clientMsgIDList) . "')";
                }
            }

            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            $user = $app->user;
            $total = 0;
            $ar = array();
            $recdQuery = "SELECT parties.party_id, parties.MessageID, messages.ThreadRoot,
                            messages.SRSent, messages.NSRSent, parties.Bookmarked, messages.AttSize,
                            messages.SRCertType, messages.NSRCertType, parties.Unread
                            FROM parties
                            INNER JOIN messages ON parties.MessageID = messages.message_id " . $searchParams . "
                            WHERE RecipientID = ?
                            AND messages.Hold = '0'
                            AND Deleted != '1'
                            AND parties.isSender = '0'
                            ORDER BY
                            CASE
                                WHEN messages.SRSent >= messages.NSRSent
                                    THEN  messages.SRSent
                                WHEN messages.NSRSent > messages.SRSent
                                    THEN  messages.NSRSent
                                ELSE  messages.SRSent
                            END
                            DESC;";
            if ($stmt = $app->mysqli->prepare($recdQuery)) {
                $stmt->bind_param('ssi', $clientMsgID, $softwareID, $userID);
                if ($stmt->execute()) {
                    /* store result */
                    $stmt->store_result(); // its required to fix `Commands out of sync; you can't run this command now`, solution : http://stackoverflow.com/questions/614671/commands-out-of-sync-you-cant-run-this-command-now
                    $stmt->bind_result($party_id, $MessageID, $ThreadRoot, $SRSent, $NSRSent, $Bookmarked, $AttSize, $SRCertType, $NSRCertType, $MsgUnread);
                    while ($stmt->fetch()) {
                        $msg = new \Docsmit\Message($app->mysqli, $MessageID);
                        $docsmOwner = new \Docsmit\User($app->mysqli, $msg->ownerID());

                        $query2 = "SELECT RecipientID, Unread, isSender FROM parties WHERE MessageID = $MessageID";
                        $partiesResult = $app->mysqli->query($query2);
                        $to = Array();
                        $from = Array();
                        while (list($RecipientID, $Unread, $isSender) = $partiesResult->fetch_row()) {
                            $recip = new \Docsmit\User($app->mysqli, $RecipientID);
                            if ($isSender == "1") {
                                $from['email'][] = $recip->email();
                                //$from['unread'][] = $Unread;
                            } else {
                                $to['email'][] = $recip->email();
                                $to['unread'][] = $Unread;
                            }
                        }

                        $query3 = "SELECT Status FROM transactions WHERE MsgID = $MessageID AND PayorID=$userID ORDER BY DateTime DESC  limit 1;";
                        $trResult = $app->mysqli->query($query3);
                        list($status) = $trResult->fetch_row();
                        if (isset($status)) {
                            switch ($status) {
                                case 'pending':
                                    $status = 'p';
                                    break;
                                case PAYPAL_PENDING:
                                    $status = 'p';
                                    break;
                                case PAYPAL_COMPLETED:
                                    $status = 'c';
                                    break;
                                case PAYPAL_FAILED:
                                    $status = 'f';
                                    break;
                                case PAYPAL_VOIDED:
                                    $status = 'f';
                                    break;
                                case 'aborted':
                                    $status = 'a';
                                    break;
                                default:
                                    $status = '';
                                    break;
                            }
                        } else
                            $status = '';

                        $isFlagged = 0;
                        $isReceive = $msg->isRecipient($app->mysqli, $userID);
                        if ($isReceive) {
                            $isFlagged = $msg->isFlagedByRecipient($userID) ? 1 : 0;
                        }
                        $ar[] = array(
                            'messageID' => $MessageID + 0,
                            'to' => $to,
                            'from' => $from, //$docsmOwner->email(),
                            'title' => $msg->title(),
                            'sent' => $msg->sent(),
                            'unread' => $msg->notViewedYet($app->mysqli, $userID), //TODO 1 Why not trust parties.unread??
                            'bookmarked' => (int) $Bookmarked,
                            'certified' => ($msg->isCertified()) ? 1 : 0,
                            'transaction' => $status,
                            'threadRoot' => $ThreadRoot,
                            'attachments' => ($msg->zipID() ? 1 : 0),
                            'attSize' => $AttSize,
                            'isFlagged' => $isFlagged,
                            'isEncrypted' => $msg->isEncrypted() ? 1 : 0,
                            'alreadySentType' => $msg->alreadySentType(),
                            'SRSent' => $SRSent,
                            'NSRSent' => $NSRSent,
                            'softwareID' => $msg->softwareID()
                        );
                    }
                }
            }
            if (empty($total)) {
                $total = count($ar);
            }
            $app->response->headers->set('X-Total-Count', $total);
            $app->response->body(json_encode($ar, JSON_PRETTY_PRINT));
            return;
        });
// Message - Draft
        $app->get('/draft', function () use ($app, $log) {
            $get = $app->request()->get();
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            $total = 0;
            $ar = array();
            $searchTerm = null;
            $clientMsgID = 1;
            $clientMsgID_MR = 1;
            $searchParams = null;
            if (!empty($get["clientMsgID"])) {
                $clientMsgID = $app->mysqli->escape_string($get["clientMsgID"]);
                $searchParams .= " AND messages.ClientMsgID = ?";
            } else {
                $searchParams .= " AND ? ";
            }
            if (!empty($get["clientMsgID_MR"])) {
                $clientMsgID_MR = $app->mysqli->escape_string($get["clientMsgID_MR"]);
                $searchParams .= " AND messages.ClientMsgID REGEXP ?";
            } else {
                $searchParams .= " AND ? ";
            }
            if (!empty($get["messageIDList"]) && !empty($get["clientMsgIDList"])) {
                $messageIDList = $app->mysqli->escape_string($get["messageIDList"]);
                $searchParams .= " AND (messages.message_id IN (" . $messageIDList . ")";

                $clientMsgIDList = explode(',', $app->mysqli->escape_string($get["clientMsgIDList"]));
                $searchParams .= " OR messages.ClientMsgID IN ('" . implode("','", $clientMsgIDList) . "'))";
            } else {
                if (!empty($get["messageIDList"])) {
                    $messageIDList = $app->mysqli->escape_string($get["messageIDList"]);
                    $searchParams .= " AND messages.message_id IN (" . $messageIDList . ")";
                }

                if (!empty($get["clientMsgIDList"])) {
                    $clientMsgIDList = explode(',', $app->mysqli->escape_string($get["clientMsgIDList"]));
                    $searchParams .= " AND messages.ClientMsgID IN ('" . implode("','", $clientMsgIDList) . "')";
                }
            }

            if (!empty($get["turnOffSoftwareID"])) {
                $searchTerm = $app->mysqli->escape_string($get["turnOffSoftwareID"]);
                $validSearchParamsTurnOffSoftwareID = array("true", "false", "1", "0");
                if (!in_array($searchTerm, $validSearchParamsTurnOffSoftwareID)) {
                    throw new ValidationException("Invalid value supplied for `turnOffSoftwareID` parameter. valid inputs for turnOffSoftwareID param are " . implode(", ", $validSearchParamsTurnOffSoftwareID), 400);
                }
                $softwareID = 1;
                $searchParams .= " AND ? ";
            } else {
                $softwareID = !empty($app->APIToken->softwareID()) ? $app->APIToken->softwareID() : null;
                $searchParams .= " AND messages.softwareID = ?";
            }

            $draftQuery = "SELECT parties.party_id, parties.MessageID, messages.ThreadRoot, messages.Title, messages.AttSize, messages.BillTo ".
                    "FROM parties ".
                    "INNER JOIN messages ON parties.MessageID = messages.message_id ".
                    "$searchParams " .
                    "WHERE RecipientID = ? ".
                    "AND messages.Hold = '1' AND Deleted != '1' AND parties.isSender = '1' " .
                    "ORDER BY messages.message_id DESC;";
            if ($stmt = $app->mysqli->prepare($draftQuery)) {
                $stmt->bind_param('sssi', $clientMsgID, $clientMsgID_MR, $softwareID, $userID);
                if ($stmt->execute()) {
                    /* store result */
                    $stmt->store_result(); // its required to fix `Commands out of sync; you can't run this command now`, solution : http://stackoverflow.com/questions/614671/commands-out-of-sync-you-cant-run-this-command-now
                    $stmt->bind_result($party_id, $MessageID, $ThreadRoot, $Title, $AttSize,$BillTo);
                    while ($stmt->fetch()) {
                        $msg = new \Docsmit\Message($app->mysqli, $MessageID);
                        $docsmOwner = new \Docsmit\User($app->mysqli, $msg->ownerID());
                        $files = new \Docsmit\File($app->mysqli);
                        $attachments = $files->checkAttachmentExist($app->mysqli, $MessageID);
                        $query2="SELECT p.RecipientID, Unread, isSender, p.MailAddressID, u.email,
                                   ma.FirstName, ma.LastName, ma.Organization, ma.DeliveryStatus,
                                   ma.ProductionStatus, ma.SendType
                               FROM parties p
                               LEFT JOIN users u ON u.UserID = p.RecipientID
                               LEFT JOIN mailAddresses ma ON p.MailAddressID = ma.MailAddressID
                               WHERE p.MessageID = $MessageID ORDER BY ma.LastName, ma.FirstName, ma.Organization ASC
                               ";
                        $partiesResult = $app->mysqli->query($query2);
                        $to = Array();
                        $from = Array();
                        while (list($RecipientID, $Unread, $isSender, $MailAddressID,
                        $Email, $FirstName, $LastName, $Organization, $DeliveryStatus,
                        $ProductionStatus, $SendType) = $partiesResult->fetch_row())
                        {
                            if ($isSender == "1") {
                                $from[] = array(
                                    "email" => $Email,
                                    "unread" => $Unread
                                );
                            } else {
                                $to[] = array(
                                    'email' => $Email,
                                    'unread' => $Unread,
                                    //'SignedFor' => $SignedFor,
                                    'mailName' => ($FirstName ? $FirstName . " " : "") . $LastName,
                                    'mailCompany' => $Organization,
                                    'mailDeliveryStatus' => $DeliveryStatus,
                                    'mailProdStatus' => $ProductionStatus,
                                    'sendType' => $SendType
                                );
                            }
                        }

                        $ar[] = array(
                            'messageID' => $MessageID + 0,
                            'from' => $from,
                            'fromList' => $from,
                            'title' => $Title,
                            'to' => $to,
                            'attSize' => $AttSize,
                            'attachments' => $attachments,
                            'clientMsgID' => $msg->clientMsgID(),
                            'softwareID' => $msg->softwareID(),
                            'billTo' => $msg->billTo()
                        );
                    }
                }
            }
            if (empty($total)) {
                $total = count($ar);
            }
            $app->response->headers->set('X-Total-Count', $total);
            $app->response->body(json_encode($ar, JSON_PRETTY_PRINT));
            return;
        });
// Message - Sent
        $app->get('/sent', function () use ($app, $log) {
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;

            $get = $app->request()->get();
            // below code is temparary purpose
            //pr($get);
            $searchParams = null;
            $searchSenderParams = null;
            $searchRecipientParams = null;
            $commanQuery = "AND messages.OwnerID = parties.RecipientID";
            if (isset($get["unsigned"])) {
                //echo $get["unsigned"];exit;
                $searchTerm = $get["unsigned"];
                $validSearchParamsUnsigned = array("true", "false", "1", "0");
                if (!in_array($searchTerm, $validSearchParamsUnsigned)) {
                    throw new ValidationException("Invalid value supplied for `unsigned` parameter. valid inputs for unsigned param are " . implode(", ", $validSearchParamsUnsigned), 400);
                }

                if ($searchTerm == "true" || $searchTerm == 1)
                    $searchParams = " AND (parties.SignedFor IS NULL OR parties.SignedFor = '0000-00-00 00:00:00') ";
                elseif ($searchTerm == "false" || $searchTerm == 0)
                    $searchParams = " AND (parties.SignedFor IS NOT NULL OR parties.SignedFor <> '0000-00-00 00:00:00') ";
            }

            $clientMsgID = 1;
            if (!empty($get["clientMsgID"])) {
                $clientMsgID = $app->mysqli->escape_string($get["clientMsgID"]);
                $searchParams .= " AND messages.ClientMsgID = ?";
            } else {
                $searchParams .= " AND ? ";
            }

            $clientMsgID_MR = 1;
            if (!empty($get["clientMsgID_MR"])) {
                $clientMsgID_MR = $app->mysqli->escape_string($get["clientMsgID_MR"]);
                $searchParams .= " AND messages.ClientMsgID REGEXP ?";
            } else {
                $searchParams .= " AND ? ";
            }

            if (!empty($get["messageIDList"]) && !empty($get["clientMsgIDList"])) {
                $messageIDList = $app->mysqli->escape_string($get["messageIDList"]);
                $searchParams .= " AND (messages.message_id IN (" . $messageIDList . ")";

                $clientMsgIDList = explode(',', $app->mysqli->escape_string($get["clientMsgIDList"]));
                $searchParams .= " OR messages.ClientMsgID IN ('" . implode("','", $clientMsgIDList) . "'))";
            } else {
                if (!empty($get["messageIDList"])) {
                    $messageIDList = $app->mysqli->escape_string($get["messageIDList"]);
                    $searchParams .= " AND messages.message_id IN (" . $messageIDList . ")";
                }

                if (!empty($get["clientMsgIDList"])) {
                    $clientMsgIDList = explode(',', $app->mysqli->escape_string($get["clientMsgIDList"]));
                    $searchParams .= " AND messages.ClientMsgID IN ('" . implode("','", $clientMsgIDList) . "')";
                }
            }
            if (!empty($get["turnOffSoftwareID"])) {
                $searchTerm = $get["turnOffSoftwareID"];
                $validSearchParamsTurnOffSoftwareID = array("true", "false", "1", "0");
                if (!in_array($searchTerm, $validSearchParamsTurnOffSoftwareID)) {
                    throw new ValidationException("Invalid value supplied for `turnOffSoftwareID` parameter. Valid inputs for unsigned param are " . implode(", ", $validSearchParamsTurnOffSoftwareID), 400);
                }
//                $softwareID = 1;
//                $searchParams .= " AND ? ";
            } else {
                $softwareID = !empty($app->APIToken->softwareID()) ? $app->APIToken->softwareID() : null;
                $searchParams .= " AND messages.softwareID = $softwareID";
            }
            if (!empty($get["sender"])) {
                $sender = $app->mysqli->escape_string($get["sender"]);
                $searchSenderParams .= " AND users.email = '$sender' AND isSender = 1";
                $commanQuery = "";
            }
            if (!empty($get["recipient"])) {
                $recipient = $app->mysqli->escape_string($get["recipient"]);
                $searchRecipientParams .= " AND users.email = '$recipient' AND isSender = 0";
                $commanQuery = "";
            }

            $user = $app->user;
            $total = 0;
            $ar = array();
            $query = "
                      SELECT message_id, OwnerID, Title, messages.SRSent,
                         AttSize, SRCertType, NSRCertType, Bookmarked
                      FROM messages 
                      INNER JOIN parties ON messages.message_id = parties.MessageID
                      INNER JOIN users ON parties.RecipientID = users.UserID 
                      WHERE messages.OwnerID = ? AND Hold = '0' AND Deleted != '1' $searchSenderParams $searchRecipientParams 
                          $commanQuery $searchParams
                      ORDER BY messages.SRSent
        ";
            // below line is for debugging of tests.php
            if (!empty($get["debug"])) {
                echo $query;
                exit;
            }

            if ($stmt = $app->mysqli->prepare($query)) {
                $stmt->bind_param('sss', $userID, $clientMsgID, $clientMsgID_MR);
                if ($stmt->execute()) {
                    /* store result */
                    $stmt->store_result(); // its required to fix `Commands out of sync; you can't run this command now`, solution : http://stackoverflow.com/questions/614671/commands-out-of-sync-you-cant-run-this-command-now
                    $stmt->bind_result($MessageID, $OwnerID, $Title, $SRSent, $AttSize, $SRCertType, $NSRCertType, $Bookmarked);
                    while ($stmt->fetch()) {
                        $msg = new \Docsmit\Message($app->mysqli, $MessageID);
                        $docsmOwner = new \Docsmit\User($app->mysqli, $OwnerID);
                        $query2 = "SELECT ma.mailAddressID, p.RecipientID, Unread, SignedFor, p.MailAddressID, u.email,
                                   ma.FirstName, ma.LastName, ma.Organization,ma.Address1,ma.Address2,
                                   ma.City, ma.State, ma.PostalCode,ma.Envelope, ma.SendType, ma.Sided,
                                   ma.MailClass,ma.SASE, ma.SASESheets,ma.Binding,ma.Cover,
                                   ma.DeliveryStatus, ma.DStatusAO,ma.DeliveryLocation,
                                   ma.ProductionStatus, ma.PStatusAO, ma.USPSTrackingID, ma.Color
                               FROM parties p
                               LEFT JOIN users u ON u.UserID = p.RecipientID
                               LEFT JOIN mailAddresses ma ON p.MailAddressID = ma.MailAddressID
                               WHERE p.MessageID = $MessageID AND p.isSender = '0' 
                               ORDER BY ma.LastName, ma.FirstName, ma.Organization ASC";
                        $recipientsResult = $app->mysqli->query($query2);
                        $totalRecipientsCount = $recipientsResult->num_rows;
//                        $totalUnsignedCount = $msg->getTotalUnsignedMsgCount($MessageID);
//                        $sesEmail = new \Docsmit\SESEmail($app->mysqli);
//                        $bouncedMsg = $sesEmail->getBouncedMsgString($app->mysqli, $MessageID);
                        $i = 0;
                        $to = Array();

                        while (list($MailAddressID, $RecipientID, $Unread, $SignedFor, $MailAddressID, $Email,
                                $FirstName, $LastName, $Organization, $Address1,$Address2,
                                $City, $State, $PostalCode, $Envelope, $SendType, $Sided,
                                $MailClass, $SASE, $SASESheets, $Binding, $Cover,
                                $DeliveryStatus,$DStatusAO, $DeliveryLocation,
                                $ProductionStatus, $PStatusAO, $USPSTrackingID, $Color) = $recipientsResult->fetch_row())
                        {
                            $to[] = array(
                                'MAID' => $MailAddressID, 
//                                'Email' => $Email,
//                                'Unread' => $Unread,
//                                'SignedFor' => $SignedFor,
//                                'Name' => ($FirstName ? $FirstName." " : "") . $LastName,
                                'firstName' => $LastName,
                                'lastName' => $FirstName,
                                'organization' => $Organization,
                                'address1' => $Address1,
                                'address2' => $Address2,
                                'city' => $City,
                                'state' => $State,
                                'postalCode' => $PostalCode,
                                'sendType' =>$SendType,
                                'envelope' =>$Envelope,
                                'sided' => $Sided,
//                                'MailClass' => $MailClass,
                                'SASE' => $SASE,
                                'SASESheets' => $SASESheets,
                                'Color' => $Color,
//                                'Binding' => $Binding,
//                                'Cover' => $Cover,
                                'DeliveryStatus' => $DeliveryStatus,
                                'DStatusAO' => $DStatusAO,
                                'DeliveryLocation' => $DeliveryLocation,
                                'ProductionStatus' =>$ProductionStatus,
                                'PStatusAO' => $PStatusAO,
                                'USPSTrackingID' => $USPSTrackingID,
                            );
                        }

//                        $query3 = "SELECT Status FROM transactions WHERE MsgID = $MessageID AND PayorID=$userID ORDER BY DateTime DESC  limit 1;";
//                        $trResult = $app->mysqli->query($query3);
//                        list($status) = $trResult->fetch_row();
//                        if (isset($status)) {
//                            switch ($status) {
//                                case 'pending':
//                                    $status = 'p';
//                                    break;
//                                case PAYPAL_PENDING:
//                                    $status = 'p';
//                                    break;
//                                case PAYPAL_COMPLETED:
//                                    $status = 'c';
//                                    break;
//                                case PAYPAL_FAILED:
//                                    $status = 'f';
//                                    break;
//                                case PAYPAL_VOIDED:
//                                    $status = 'f';
//                                    break;
//                                case 'aborted':
//                                    $status = 'a';
//                                    break;
//                                default:
//                                    $status = '';
//                                    break;
//                            }
//                        } else
//                            $status = '';

                        $isFlagged = 0;
                        $isSend = $msg->isSender($app->mysqli, $userID);
//                        if ($isSend) {
//                            $isFlagged = $msg->hasFlagsFromRecipients() ? 1 : 0;
//                        }
//                        $bouncedResult = array('total' => $bouncedMsg['totalBouncedMsgCount'],
//                            'initialBounce' => $bouncedMsg['totalInitailBouncedMsgCount'],
//                            'reminderBounce' => $bouncedMsg['totalReminderBouncedMsgCount']);
                        $ar[] = array(
                            'messageID' => $MessageID + 0,
                            'from' => $docsmOwner->email(),
                            'title' => $Title,
                            'to' => $to,
//                            'attachments' => ($msg->zipID() ? 1 : 0),
//                            'threadRoot' => $ThreadRoot,
//                            'certified' => $msg->isCertified() ? 1 : 0,
                            'attSize' => $AttSize,
//                            'sent' => ($msg->isSent()) ? $msg->Sent() : '0000-00-00 00:00:00',
//                            'transaction' => $status,
                            'bookmarked' => (int) $Bookmarked,
                            'isFlagged' => $isFlagged,
//                            'isDeliverAfter' => $msg->isDeliverAfter() ? 1 : 0,
                            'isSent' => $msg->isSent() ? 1 : 0,
//                            'isBlastMessage' => $msg->isBlastMessage() ? 1 : 0,
                            'totalRecipientsCount' => $totalRecipientsCount,
//                            'totalUnsignedCount' => $totalUnsignedCount,
//                            'isEncrypted' => $msg->isEncrypted() ? 1 : 0,
//                            'alreadySentType' => $msg->alreadySentType(),
                            'accepted' => $SRSent,
//                            'NSRSent' => $NSRSent,
//                            'SRDeliverAfter' => $SRDeliverAfter,
//                            'NSRDeliverAfter' => $NSRDeliverAfter,
                            'clientMsgID' => $msg->clientMsgID(),
                            'softwareID' => $msg->softwareID(),
//                            'isBounced' => ($bouncedMsg['totalBouncedMsgCount'] > 0) ? true : false,
//                            'bouncedMsgCount' => $bouncedResult,
                            'billTo'=>$msg->billTo()
                        );
                    }
                }
            }
            if (empty($total)) {
                $total = count($ar);
            }
            $app->response->headers->set('X-Total-Count', $total);
            $app->response->body(json_encode($ar, JSON_PRETTY_PRINT));
            return;
        });

// Message - Sent message by messageID
        $app->get('/:messageID', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID)
                throw new ValidationException("Problem with one of the request parameters. " . $app->request->getResourceUri(), 400);

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;

            if ($msg->messageID() == -1)
                throw new ValidationException("Message $messageID not located.", 403);

            if (!$msg->isParty($app->mysqli, $userID))
                throw new ValidationException("User does not have authority to perform this action on message $messageID.", 403);

            $owner = new \Docsmit\User($app->mysqli, $msg->ownerID());
            $query2 = "SELECT ma.mailAddressID, ma.firstName, ma.lastName, ma.organization, ma.sendType, ma.USPSTrackingID, 
                           ma.productionStatus, ma.PStatusAO, ma.deliveryStatus, ma.DStatusAO
                       FROM parties p, mailAddresses ma
                       WHERE MessageID = $messageID AND 
                           p.MailAddressID = ma.MailAddressID AND
                           isSender = '0'";
            $recipientsResult = $app->mysqli->query($query2);

            $to = Array();
            while ($recipRow = $recipientsResult->fetch_assoc()) {
//                $recipRow ['unread'] = (int) 0; // deprecated
//                $recipRow ['email'] = "deprecated@deprecated.com";
                $to[] = $recipRow;
            }

            $tags = new \Docsmit\Tags($app->mysqli, $messageID, $userID);
            $files = new \Docsmit\File($app->mysqli);
            $ar = array(
                'messageID' => $messageID + 0,
                'from' => $owner->email(),
                'title' => $msg->title(),
                'to' => $to,
                'hold' => $msg->hold(),
                'totalRecipientsCount' => $recipientsResult->num_rows,
                'billTo' => $msg->BillTo(),
                'attachments' => $files->checkAttachmentExist($app->mysqli, $messageID),
                'attSize' => (int) $msg->attsize(),
                'taglist' => $tags->taglist(),
            );

            $app->response->headers->set('X-Total-Count', count($ar));
            $app->response->body(json_encode($ar, JSON_PRETTY_PRINT));
            return;
        });

// Message - Delete message by messageID
        $app->delete('/:messageID/delete', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);

            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if($msg->messageID() == -1)
                throw new ValidationException("Could not find message.", 404);

            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;

            if (!$msg->isSender($app->mysqli, $userID) || !$msg->hold() || $msg->ownerID() != $userID)
                throw new ValidationException("User does not have authority to perform this action on this message.", 403);

            if(!$msg->cleanUpFiles())
                throw new ValidationException("Something went wrong while cleanup files message", 500);

            $deleted = $msg->setDeleted();
            if (!$deleted)
                throw new ValidationException("Something went wrong while deleting message", 500);

            $ar["deleted"]= $deleted;
            $app->response->body(json_encode($ar, JSON_PRETTY_PRINT));
            return;
        });
// Message - new
        $app->post('/new', function () use ($app, $log) {
            $user = $app->user;
            $userID = !empty($user->userID()) ? $user->userID() : null;
            $userEmail = !empty($user->email()) ? $user->email() : null;
            $body = $app->request()->getBody();
            DEBLOG (print_r($body, true));
            //// Processing
            $errors = $app->validateMessage($body);
            if (!empty($errors))
                throw new ValidationException("New message validation error. " . print_r($errors, true), 400, $errors);

            $body = (object) $body;

            $msgBody = null;
            if (isset($body->bodyHTML)) {
                $msgBody = $app->mysqli->escape_string($body->bodyHTML);
                $msgBody = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $msgBody);
            } elseif (isset($body->bodyText)) {
                $msgBody = htmlentities(nl2br($body->bodyText));
            }

            $clientMsgID = isset($body->clientMsgID) ? $app->mysqli->escape_string($body->clientMsgID) : null;
//            $title = isset($body->title) ? $app->mysqli->escape_string($body->title) : null;
            $title = isset($body->title) ? htmlentities($body->title) : null;
            $signatureRequired = isset($body->signatureRequired) && $body->signatureRequired == false ? "NSR" : "SR";
            $rtnName = isset($body->rtnName) ? $app->mysqli->escape_string($body->rtnName) : null;
            $rtnOrganization = isset($body->rtnOrganization) ? $app->mysqli->escape_string($body->rtnOrganization) : null;
            $rtnAddress1 = isset($body->rtnAddress1) ? $app->mysqli->escape_string($body->rtnAddress1) : null;
            $rtnAddress2 = isset($body->rtnAddress2) ? $app->mysqli->escape_string($body->rtnAddress2) : null;
            $rtnCity = isset($body->rtnCity) ? $app->mysqli->escape_string($body->rtnCity) : null;
            $rtnState = isset($body->rtnState) ? abbreviateState($app->mysqli->escape_string(strtoupper($body->rtnState))) : null;
            $rtnZip = isset($body->rtnZip) ? $app->mysqli->escape_string($body->rtnZip) : null;
            $billTo = isset($body->billTo) ? $app->mysqli->escape_string($body->billTo) : null;

            if (!isset($body->rtnState) || !isValidState($rtnState)) {
                throw new ValidationException("Invalid rtnState. Please use valid state abbreviation.", 400);
            }
            if (!isset($body->rtnZip) || !isValidPostalCode($body->rtnZip)) {
                throw new ValidationException("Invalid rtnZip. Please use valid zip code. i.e. 12345 or 12345-5434", 400);
            }
            if (empty($body->rtnName) && empty($body->rtnOrganization)) {
                throw new ValidationException("Needs either rtnName or rtnOrganization.", 400);
            }
            if (empty($body->rtnAddress1)) {
                throw new ValidationException("rtnAddress1 is required.", 400);
            }

            //// Processing
            $messageID = \Docsmit\Message::insert($app->mysqli, $userID);
            if ($messageID == -1) {
                ERRLOG("Problem inserting new message, userID = $userID");
                throw new ValidationException("Problem inserting new message.", 500);
            }

            $output["messageID"] = $messageID;
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if (!$msg->isParty($app->mysqli, $userID) || $msg->ownerID() != $userID) {
                SECLOG("Unauthorized access, userID = $userID");
                throw new ValidationException("User does not have authority to perform this action on this message.", 403);
            }

            $softwareID = !empty($app->APIToken->softwareID()) ? $app->APIToken->softwareID() : null;
            $msg->setSoftwareID($softwareID);

            if (!empty($clientMsgID)) {
                $msg->setClientMsgID($clientMsgID);
            }
            if (!empty($title)) {
                $msg->setTitle($app->mysqli, $title);
            }
            if (!empty($msgBody)) {
                $msg->setMailMsg($app->mysqli, $msgBody);
            }
            if (!empty($billTo)) {
                $msg->setBillTo($billTo);
            }

            $msg->setRtnAddress($rtnName, $rtnOrganization, $rtnAddress1, $rtnAddress2, $rtnCity, $rtnState, $rtnZip);

            $handlePartyResult = \Docsmit\APIWork::handleParty($app->mysqli,$body, $userID,$messageID);
            if (!empty($handlePartyResult["errors"]))
                throw new ValidationException("Error occurred adding recipients.", 400, $handlePartyResult["errors"]);

//            if (!empty($body->unclaimedNoticeTimeout)) {
//                $emailRecipients = $msg->emailRecipients();
//                if(empty($emailRecipients))
//                    throw new ValidationException("This message doesn't contain emailRecipients.", 403);
//
//                $unclaimedNoticeTimeout = $body->unclaimedNoticeTimeout;
//                $isValidDate = (bool) strtotime($unclaimedNoticeTimeout);
//                if (!$isValidDate) {
//                    $errors["unclaimedNoticeTimeout"] = $body->unclaimedNoticeTimeout;
//                    throw new ValidationException("Invalid date for unclaimedNoticeTimeout.", 400, $errors);
//                }
//                $db_format_timeout = date("Y-m-d H:i:s", strtotime($unclaimedNoticeTimeout));
//                $utc_unclaimedNoticeTimeout = convertDateToUTC($db_format_timeout, $user->timezone(), "Y-m-d H:i:s");
//                $party = new \Docsmit\Party($app->mysqli);
//                // update unclaimedNoticeTimeout for all parties for this message including sender
//                $party->setTimeout($messageID, $userID, $utc_unclaimedNoticeTimeout);
//            }


            if (!empty($body->deliverAfter)) {
                $deliverAfter = $body->deliverAfter;
                $isValidDate = (bool) strtotime($deliverAfter);
                if (!$isValidDate) {
                    SECLOG("Invalid date for deliverAfter = $deliverAfter " . var_export($body, true));
                    $errors["deliverAfter"] = $body->deliverAfter;
                    throw new ValidationException("Invalid date for deliverAfter.", 400, $errors);
                }
                $dbFormatDeliverAfter = date("Y-m-d H:i:s", strtotime($body->deliverAfter));
                $utcDeliverAfter = convertDateToUTC($dbFormatDeliverAfter, $user->timezone(), "Y-m-d H:i:s");
                $msg->setDeliverAfter($utcDeliverAfter, $signatureRequired);
            }

            if (isset($body->excludeMessageBody)) {
                $excludeMessageBody = $body->excludeMessageBody === true || $body->excludeMessageBody == 1 ? 1 : 0;
                $msg->setExcludeMessageBody($app->mysqli, $excludeMessageBody);
            }

            if (isset($body->blastMessage)) {
                $blastMessage = $body->blastMessage === true || $body->blastMessage == 1 ? 1 : 0;
                $msg->setBlastMessage($blastMessage);
            }

            if (!empty($body->callbackURIs)) {
                $callbackErrors = array();
                foreach ($body->callbackURIs as $callbackName => $callbackURI) {
                    // http://www.bitrepository.com/how-to-extract-domain-name-from-an-e-mail-address-string.html
                    $emailDomain = substr(strrchr($userEmail, "@"), 1);
                    //http://stackoverflow.com/questions/16027102/get-domain-name-from-full-url
                    $URIDomain = getDomainFromURL($callbackURI);

                    if ($emailDomain != $URIDomain) {
                        $callbackErrors[$callbackName] = array(
                            "callbackName" => $callbackName,
                            "callbackURI" => $callbackURI,
                            "emailDomain" => $emailDomain,
                            "URIDomain" => $URIDomain,
                            "errorMessage" => "Tried to set callback URI with domain different than email domain."
                        );
                    }
                }

                if (!empty($callbackErrors))
                    throw new ValidationException("Callback domain must be the same as the user's email domain:", 400, $callbackErrors);

                $callbackURIs = json_encode($body->callbackURIs);
                $msg->setCallbackURIs($callbackURIs);
            }
            $app->status(201);
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });

// Message - Update message content by messageID
        $app->put('/:messageID/content', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);

            if (false === $messageID) {
                throw new ValidationException("Problem with messageID parameter.", 400);
            }

            $body = $app->request()->getBody();
            $errors = $app->validateMessage($body);

            if (!empty($errors))
                throw new ValidationException("Message validation error.", 400, $errors);

            $user = $app->user;
            $userID = !empty($user->userID()) ? $user->userID() : null;

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if (!$msg->isParty($app->mysqli, $userID) || $msg->ownerID() != $userID) {
                SECLOG("Unauthorized access, userID = $userID");
                throw new ValidationException("User does not have authority to perform this action on this message.", 403);
            }

            $msgBody = null;
            if (isset($body['bodyHTML'])) {
                $msgBody = $app->mysqli->escape_string($body['bodyHTML']);
                $msgBody = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $msgBody);
            } elseif (isset($body['bodyText'])) {
                $msgBody = htmlentities(nl2br($body['bodyText']));
            }

            $title = isset($body['title']) ? $app->mysqli->escape_string($body['title']) : null;
            if (!empty($title)) {
                $msg->setTitle($app->mysqli, $title);
            }
            if (!empty($body)) {
                $msg->setMailMsg($app->mysqli, $msgBody);
            }

            $output = array();
            $app->status(202);
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - messageID - party using comma separated emails, email address or physical mail address
        $app->post('/:messageID/party', function ($messageID) use ($app, $log) {
            if (false === filter_var($messageID, FILTER_VALIDATE_INT)) {
                throw new ValidationException("Invalid messageID.", 400);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;

            if (!$msg->isSender($app->mysqli, $userID)) {
                throw new ValidationException("User does not have authority to perform this action on this message.", 403);
            }
            $body = (object) $app->request()->getBody();
            $output = \Docsmit\APIWork::handleParty($app->mysqli, $body, $userID, $messageID);

            if (empty($output))
                throw new ValidationException("Problem with parameter. please pass valid params: recipients,toList,userIDs,email,physicalParties etc.", 400);

            if (!empty($output["errors"]))
                throw new ValidationException("Error occured.", 400, $output["errors"]);

            $app->status(201);
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - messageID - send
        $app->post('/:messageID/send', function ($messageID) use ($app, $log) {

DEBLOG("API: Inside POST /send for messageID $messageID");
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Invalid messageID parameter.", 400);
            }
            $body = $app->request()->getBody();
            $user = $app->user;
            $APIToken = $app->APIToken;
            $userID = !empty($user->userID()) ? $user->userID() : null;
            $userSettings = new \Docsmit\UserSettings($app->mysqli, $userID);
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
DEBLOG("API: About to validatePresend for messageID $messageID");
            $presendFailErrors = $msg->validatePresend();
            if (count($presendFailErrors) > 0) {
                DEBLOG('ValidatePresend' . " messageID:$messageID, userID:$userID");
                throw new ValidationException("Message cannot be sent yet.", 403, $presendFailErrors);
            }

            if (!$msg->hold()) {
                DEBLOG('Message is not hold.' . " messageID:$messageID, userID:$userID");
                throw new ValidationException("Message cannot be sent as it is not a draft message", 403);
            }

            if ($userSettings->requireBillTo() == 1 && empty($msg->billTo())) {
                DEBLOG('Bill To is required in user settings.  Cannot send.  MsgID and UserID: ' . " messageID:$messageID. ", ". userID:$userID");
                throw new ValidationException("Your User Settings requires a BillTo to send.", 403);
            }
            if (!$msg->isSender($app->mysqli, $userID)) {
                throw new ValidationException("You do not have authority to send this message.", 403);
            }
            if ($msg->isSent())
                throw new ValidationException("Message has already been sent.", 403);

//            if (empty($msg->title())) {
//                throw new ValidationException("Message title is required to send.", 403);
//            }
            if ($msg->numRecipients($app->mysqli) == 0) {
                throw new ValidationException("At least one recipient is required to send.", 403);
            }
//            if (isset($body['months'])) {
//                $months = filter_var($body['months'], FILTER_VALIDATE_INT);
//                if (false === $months) {
//                    throw new ValidationException("Invalid value supplied for 'months' parameter.", 400);
//                }
//                $validInputMonths = array(0, 12, 36, 84);
//                if (!in_array($months, $validInputMonths)) {
//                    throw new ValidationException("Invalid value supplied for `months` parameter. Valid inputs are " . implode(", ", $validInputMonths), 403);
//                }
//                $months = $body['months'];
//            }
//            if (isset($body["signatureRequired"])) {
//                $signatureRequired = $body["signatureRequired"];
//                $validSignatureRequiredInputs = array("true", "false", "1", "0");
//                if (!in_array($signatureRequired, $validSignatureRequiredInputs)) {
//                    throw new ValidationException("Invalid value supplied for signatureRequired parameter. valid inputs for signer_conf_mail params are " . implode(", ", $validSignatureRequiredInputs), 403);
//                }
//                $sendType = $body["signatureRequired"] === true || $body["signatureRequired"] == 1 ? "SR" : "NSR";
//            }

            $pm = new \Docsmit\PricingModel($app->mysqli, $userID, $messageID);
            $userSettings = new \Docsmit\UserSettings($app->mysqli, $app->user->userID());
            $cert = 1;
            $months = $userSettings->defaultExtendMonth();
            $sendType = "SR";
            $pm->setupConsume($cert, $months, $sendType, true);
            $netPrice = $pm->getNetPrice(true);
            $creditBalance = $userSettings->creditBalance();
            $output = array();
            if ($creditBalance < $netPrice && !$userSettings->allowNegativeBalance() && $netPrice != 0) {
                $output["message"] = "Your credit balance of $creditBalance is insufficient to send this mailing. Please buy credits to send.";
                $output["shortfall"] = $netPrice - $creditBalance;
                //$app->status(402);
                ////////////send an email to owner letting him know of this user's first DL.
                $docsmOwner = new \Docsmit\User($app->mysqli, $msg->ownerID());
                $dm = new \Docsmit\Docsmail;
                $dm->to = $docsmOwner->email();
                $dm->subject = "Credit Balance Too Low to Send";
                $dm->template = array(	
                    'shortfall' =>  number_format($netPrice - $creditBalance, 2),
                    'toLocal' => $user->localname(),
                    'source' => 'APIInsufficientFunds.html',
                    'title' => $msg->title(),
                    'creditBalance' => $creditBalance,
                    'messageID' => $messageID
                );
                if (!$dm->send()) {
                    ERRLOG("Problem notifying " . $docsmOwner->email() . " of credit shortfall!<BR> mail->getMessage() " . $dm->error);
                }
                throw new ValidationException("Your credit balance of $creditBalance is insufficient to send this message. Please buy credits to send.", 402, $output);
            }

            $tr = new \Docsmit\Transaction($app->mysqli);
            $tr->insert($userID, $messageID);
            if (!$tr->consume($cert, $months)) {
                DEBLOG("Transaction consume failed. messageID:$messageID, userID:$userID");
                throw new ValidationException("Something went wrong while creating transaction. Message $messageID not sent.", 403);
            }
            $tr->setBillTo($msg->billTo());
            $tr->setStatus(TRANSACTION_COMPLETED);
            $tr->setSoftwareID($APIToken->softwareID());
            DEBLOG('API: about to execute message send');
//            $msg->buildZip($app->mysqli);
            $sendErrors = $msg->send($app->mysqli, $sendType);
            if (!empty($sendErrors)) {
                ERRLOG("API: errors in /send for message $messageID. " . print_r($sendErrors, true));
                throw new ValidationException("Sending error(s).  Please email us at sendfail@docsmit.com for a refund and refer to mailing ".
                   $msg->messageID(), 403);
            }
////TODO 0: MUST CHECK FOR ERRORS IN RETURN VALUE!!!

//            //Must extend and certify after sending, because sending sets Expiration
            $msg->certifyAndExtend($cert, $months, $userID);
            DEBLOG("API: Message has been sent successfully. MessageID : $messageID");
            $userSettings = new \Docsmit\UserSettings($app->mysqli, $app->user->userID());  //refresh because balance will have changed
            $output["creditBalance"] = $userSettings->creditBalance();
            $output["messageID"] = $messageID;
            $output["message"] = "Message $messageID has been sent successfully.";
            $output["clientMsgID"] = $msg->clientMsgID();
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });

// Message - documentation by messageID
        $app->get('/:messageID/documentation', function ($messageID) use ($app, $log) {

            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);

            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if (!$msg->isParty($app->mysqli, $app->user->userID())) {
                throw new ValidationException("Not authorized to access this record. Unauthorized access attempts are violations of the Terms of Service and are recorded.", 403);
            }

            if (!$msg->isCertified()) {
                throw new ValidationException("Not certified!. Unauthorized access attempts are violations of the Terms of Service and are recorded.", 403);
            }

            require_once('./Common/HTML2text.php');
            require_once('./Common/pdf.php');

            $pdf = new PDF();
            $msg->generatePDFDocumentation($app->user->userID(), $pdf);
        });

// Message - documentation by messageID
        $app->get('/:messageID/POD', function ($messageID) use ($app, $log) {

            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);

            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if (!$msg->isParty($app->mysqli, $app->user->userID())) {
                throw new ValidationException("Not authorized to access this record. Unauthorized access attempts are violations of the Terms of Service and are recorded.", 403);
            }

            if (!$msg->isCertified()) {
                throw new ValidationException("Not certified!. Unauthorized access attempts are violations of the Terms of Service and are recorded.", 403);
            }

            require_once('./Common/HTML2text.php');
            require_once('./Common/pdf.php');

            $pdf = new PDF();
            $msg->genPOD($app->user->userID(), $pdf);
        });

// Message - Sent message by messageID - sent details
        $app->get('/:messageID/sent', function ($messageID) use ($app, $log) {

            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);

            if (false === $messageID) {
                throw new ValidationException("Invalid message", 403);
            }

            $messageID = $app->mysqli->escape_string($messageID);
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isParty($app->mysqli, $userID))
                throw new ValidationException("Unauthorized access.", 400);

            $user = $app->user;
            $total = 0;
            $ar = array();
            $query = "SELECT message_id "
                    . "FROM messages "
                    . "WHERE message_id='$messageID' AND OwnerID = '$userID' AND Hold = '0' AND Deleted != '1' ";

            if ($sentResult = $app->mysqli->query($query)) {
                while (list($ReleaseID) = $sentResult->fetch_row()) {
                    $msg = new \Docsmit\Message($app->mysqli, $ReleaseID);
                    $ar[] = array(
                        'messageID' => $ReleaseID + 0,
                        'sent' => ($msg->isSent()) ? $msg->Sent() : '0000-00-00 00:00:00',
                        'isSent' => $msg->isSent() ? 1 : 0,
                    );
                }

                $total = count($ar);
                $app->response->headers->set('X-Total-Count', $total);
                echo json_encode($ar, JSON_PRETTY_PRINT);
                return;
            }
        });
// Message - parties or recipients by messageID
        $app->get('/:messageID/:parties', function ($messageID) use ($app, $log) {

            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);

            if (false === $messageID) {
                throw new ValidationException("Invalid message", 400);
            }

            $messageID = $app->mysqli->escape_string($messageID);
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }

            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isParty($app->mysqli, $userID)) {
                throw new ValidationException("Unauthorized access.", 403);
            }
            $total = 0;
            $msgDetail = $msg->msgPartiesDetail2($app->mysqli, $messageID, $userID);
            $total = count($msgDetail['physicalParties']);
            $app->response->headers->set('X-Total-Count', $total);
            echo json_encode($msgDetail, JSON_PRETTY_PRINT);
            return;
        })->conditions(array('parties' => '(recipients|parties)'));

// Message - attachments by messageID
        $app->get('/:messageID/attachments', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isParty($app->mysqli, $userID))
                throw new ValidationException("User does not have authority to perform this action on this message.", 403);
            $total = 0;
            $ar = array();
            $attachmentList = $msg->attachmentsList($messageID);
            if (!empty($attachmentList)) {
                foreach ($attachmentList as $attachment) {
                    $fileData = array(
                        //'id' => $attachment['FileID'] + 0, //was fileID
                        'fileID' => $attachment['FileID'] + 0,
                        'fileName' => $attachment['OrigFileName'],
                        'size' => $attachment['Size'] + 0,
                        'attachmentID' => $attachment['attachment_id']);
                    $ar[] = $fileData;
                }
            }
            $total = count($ar);
            $app->response->headers->set('X-Total-Count', $total);
            $app->response->body(json_encode($ar, JSON_PRETTY_PRINT));
            return;
        });

// Message - upload file by messageID
        $app->post('/:messageID/upload', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);

            if (false === $messageID) {
                throw new ValidationException("Problem with the messageID parameter.", 400);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;

            if (!$msg->isSender($app->mysqli, $userID)) {
                throw new ValidationException("User does not have authority to perform this action on this message.", 403);
            }
            if (empty($_FILES))
                throw new ValidationException("File is missing !!", 400);
            $output = array();
            foreach ($_FILES as $key => $file) {
                $filename = $file["name"];
                $size = $file["size"];
                $filenameArray = explode(".", $filename);
                $extension = strtolower(end($filenameArray));
                $allowedExts = array("pdf");
                if (!in_array($extension, $allowedExts)) {
                    $output["errors"][$filename] = "Please upload valid file.";
                    continue;
                }
                //checkEmailAttachmentsFileSizeLimit
                $totalSize = \Docsmit\File::getTotalAttachmentsFileSize($app->mysqli, $userID, $messageID);
                $requiredSize = (int) $totalSize + $size;
                $totalAttachmentFilesMaxLimit = MAX_UPLOAD_ATTACHMENT_FILESIZE * MEGABYTE; // 300 mb
                if ($requiredSize > $totalAttachmentFilesMaxLimit) {
                    $output["errors"][$filename] = "Your total attachments for email file size limit is reached up to " . MAX_UPLOAD_ATTACHMENT_FILESIZE . " MB. You are not allowed to upload more than " . MAX_UPLOAD_ATTACHMENT_FILESIZE . " MB";
                    continue;
                }

                $LFNBase = substr($filename, 0, strrpos($filename, "."));
                $LFNextension = strrchr($filename, "."); //inclusive of the '.'
                $LFNmiddle = uniqid();
                $LFN = $LFNBase . "(" . $LFNmiddle . ")" . $LFNextension;

                $PDFPageInfo = \Docsmit\File::getPDFInfo($file['tmp_name']);
         // check PDF version
                $user = new \Docsmit\User($app->mysqli, $userID);
               if (!isset($PDFPageInfo["PDF version"])) {
                    $filename = $user->email() . "_" . $LFNBase . "(" . date("YmdHis") . ")_undefind_version" . $LFNextension;
                    $upload_dir_reject_file = SECUREDIR . 'rejections/' . $filename;
                    move_uploaded_file($file['tmp_name'], $upload_dir_reject_file);
                    $output["errors"][$filename] = $user->email() . " : Can not find PDF version.";
                    continue;
                } else if (isset($PDFPageInfo["PDF version"]) && floatval($PDFPageInfo["PDF version"]) > \Docsmit\MailAddress::PDF_VERSION_SUPPORT_LIMIT) {
                    //$user = new \Docsmit\User($mysqli, $userID);
                    //for get user email
                    $filename = $user->email() . "_" . $LFNBase . "(" . date("YmdHis") . ")_unsupported_version" . $LFNextension;
                    $upload_dir_reject_file = SECUREDIR . 'rejections/' . $filename;
                    move_uploaded_file($file['tmp_name'], $upload_dir_reject_file);
                    $output["errors"][$filename] = $user->email() . " : This file is PDF version " . $PDFPageInfo["PDF version"] . " which is not supported. Please use version " . \Docsmit\MailAddress::PDF_VERSION_SUPPORT_LIMIT . " or lower.";
                    continue;
                }

                $isFileModified = false;
                if ($app->request->get('size') != NULL)
                    $isPDFAllLetterSize = true;
                else 
                    $isPDFAllLetterSize = \Docsmit\DocsPDF::isPDFAllLetterSize($file['tmp_name']);
                if (!$isPDFAllLetterSize) {
                    $tempFile = new \Docsmit\TempFile(".pdf", "C");

                    $cmd = "gs  -sDEVICE=pdfwrite  -sOutputFile=" . $tempFile->location() . " -dBATCH -dNOPAUSE -q " .
                            "-dDEVICEHEIGHTPOINTS=792 -dDEVICEWIDTHPOINTS=612 " .
                            "-dPDFFitPage " .
//            "-dFIXEDMEDIA " .
                            $file['tmp_name'];
                    exec($cmd, $output);

                    $file['tmp_name'] = $tempFile->location();
                    $LFN = "mod-" . $LFN;
                    $file['name'] = "mod-" . $file['name'];

                    $isPDFAllLetterSize = \Docsmit\DocsPDF::isPDFAllLetterSize($file['tmp_name']);

                    if (!$isPDFAllLetterSize) {
                        $html = "Hi Mark,<br/>"
                                . "<p>System has failed to resize an uploaded file.</p>"
                                . "<table>"
                                . "<tr><th>Customer Name : </th><td>" . $user->email() . "</td></tr>"
                                . "<tr><th>Uploaded File Name : </th><td>" . $file["name"] . "</td></tr>"
                                . "</table>"
                                . "<br/>Regards,<br/>Docsmit";
                        \Docsmit\Docsmail::emailAdmin($html, "* Resizing Failed *");
                        $errString = " Customer : " . $user->email() . " has uploaded file named " . $file["name"] . ". System failed to resize this file.";
                        \Docsmit\Docsmail::textAdmin($errString);
                        $output["errors"][$filename] = 'We failed to resize the file to the right size.';
                        continue;
                    } else {
                        $isFileModified = true;
                    }
                }
                $newFileID = \Docsmit\File::moveAndInsert($app->mysqli, $file, $LFN, $userID, $messageID);
                if ($newFileID == -1) {
                    ERRLOG('File $origFileName was not uploaded successfully!');
                    throw new ValidationException("File $filename was not uploaded successfully !!", 400);
                    $output["errors"][$filename] = "File $filename was not uploaded successfully !!";
                    continue;
                }
                //add new file as message attachment
                $msg->addAttachment($app->mysqli, $newFileID, $userID);
                $attachment = new \Docsmit\File($app->mysqli, $newFileID);
                $output["fileData"][] = $fileData = array(
                    'fileID' => (int) ($attachment->fileID()),
                    'fileName' => $attachment->origFileName(),
                    'size' => $attachment->size(),
                    'uploadedTime' => $attachment->uploadedTime());
            }
            
            if(!empty($output["errors"]))
            {
                throw new ValidationException("Error occurred in message " . $msg->messageID() . " : " . print_r($output["errors"], true), 415,$output["errors"]);
            }
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });

// Message - messageID - download file by fileID
        $app->get('/:messageID/downloadFile/:fileID', function ($messageID, $fileID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);

            if (false === $messageID) {
                throw new ValidationException("Invalid message !!", 403);
            }

            $fileID = filter_var($fileID, FILTER_VALIDATE_INT);

            if (false === $fileID) {
                throw new ValidationException("Invalid file !!", 403);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;

            if (!$msg->isParty($app->mysqli, $userID)) {
                throw new ValidationException("Unauthorized party !!", 400);
            }

            $file = new \Docsmit\File($app->mysqli, $fileID);

            if (!$file->canUserAccess($app->mysqli, $userID))
                throw new ValidationException("Authorization to fileID = $fileID denied for UserID = $userID !!", 400);

            if (!S3 and ! $file->exists())
                throw new ValidationException("FileID $FileID not located in local storage!!", 404);

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            $file->readfile($messageID, $userID);
            exit;
        });
// Message - set Send now deliver after time
        $app->post('/:messageID/msgSendNow', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with the messageID parameter. ", 400);
            }
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isParty($app->mysqli, $userID) || !$msg->isDeliverAfter()) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID " . var_export($_POST, true));
                throw new ValidationException("Unauthorized access !!", 403);
            }
            $deliverAfterType = $msg->deliverAfterType();
            $msg->buildZip($app->mysqli);
            $msg->send($app->mysqli, $deliverAfterType);
            $TransactionID = $msg->getDeliverAfterTransactionID($msg->ownerID());
            if ($TransactionID) {
                $tr = new \Docsmit\Transaction($app->mysqli, $TransactionID);
                $msg->certifyAndExtend($tr->certs(), $tr->months(), $msg->ownerID());
                $tr->setDeliverAfterTransaction(0);
                $msg->setDeliverAfter("0000-00-00 00:00:00", $deliverAfterType);
            } else {
                throw new ValidationException("TransactionID not found. !!", 404);
            }
            $output["TransactionID"] = $TransactionID;
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });

// Message - Generate tracking ID
        $app->post('/:messageID/trackingID', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isParty($app->mysqli, $userID)) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID " . var_export($_POST, true));
                throw new ValidationException("Unauthorized access !!", 403);
            }
            $party = new \Docsmit\Party($app->mysqli);
            $guid = GUID();
            $output = array();
            if (!$party->setGUID($guid, $messageID, $userID))
            throw new ValidationException("Something went wrong while generating Tracking ID.", 400);

            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });

// Message - Unschedule
        $app->post('/:messageID/unscheduled', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isSender($app->mysqli, $userID)) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID " . var_export($_POST, true));
                throw new ValidationException("Unauthorized access !!", 403);
            }
            if (!$msg->isDeliverAfter()) {
                SECLOG("msgUnschedule: Message is not set as DeliverAfter, userID = $userID " . var_export($_POST, true));
                throw new ValidationException("This Message is not set as deliver after.", 403);
            }
            $TransactionID = $msg->getDeliverAfterTransactionID($userID);
            $RefundTransactionID = null;
            if ($TransactionID) {
                $txn = new \Docsmit\Transaction($app->mysqli, $TransactionID);
                $txn->setDeliverAfterTransaction(0);
                $txn->refund();
                $RefundTransactionID = $txn->TransactionID();
                SECLOG("Refund by admin for transactionID:$TransactionID, Refunded, RefundTransactionID : $RefundTransactionID, userID = $userID, msgID = $messageID ");
            } else {
                SECLOG("msgUnschedule, Could not find transaction id : $TransactionID");
                throw new ValidationException("TransactionID not found.", 404);
            }
            $output["RefundTransactionID"] = $RefundTransactionID;
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - Mail party RreEmail
        $app->post('/:messageID/rreEmail/:mailAddressID', function ($messageID, $mailAddressID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            $mailAddressID = filter_var($mailAddressID, FILTER_VALIDATE_INT);
            if (false === $messageID || false === $mailAddressID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isSender($app->mysqli, $userID)) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID " . var_export($_POST, true));
                throw new ValidationException("Unauthorized access !!", 403);
            }

            $MA = new \Docsmit\MailAddress($app->mysqli, $mailAddressID);
            if (!$MA->isOK()) {
                throw new ValidationException("MailAddressID not found.", 404);
            }

            if (empty($MA->USPSTrackingID())) {
                throw new ValidationException("USPSTrackingID is required.", 400);
            }

            $output = $MA->rreEmail($app->user->email());

            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });

// Message - get tags by messaageID - CONSOLIDATED INTO GET /OPTIONS
        $app->get('/:messageID/tags', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isParty($app->mysqli, $userID))
                throw new ValidationException("Unauthorized access !!", 403);

            $tags = new \Docsmit\Tags($app->mysqli, $messageID, $userID);
            $taglist = $tags->taglist();
            $output["tags"] = $taglist;
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - Bookmark by messageID - CONSOLIDATED INTO GET /OPTIONS
        $app->get('/:messageID/bookmark', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);

            if (false === $messageID) {
                throw new ValidationException("Invalid message !!", 403);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;

            if (!$msg->isParty($app->mysqli, $userID)) {
                throw new ValidationException("Unauthorized party !!", 400);
            }

            if (!$msg->isSent()) {
                throw new ValidationException("This message is not send yet.", 403);
            }
            $party = new \Docsmit\Party($app->mysqli);
            $party->findByData($app->mysqli, $messageID, $userID);
            $current_status = $party->Bookmarked();
            $current_status_str = $current_status == 0 ? "Unbookmark" : "Bookmark";
            $output["bookmark"] = $current_status == 0 ? "false" : "true";
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - Set deliverAfter - CONSOLIDATED INTO PUT /OPTIONS
            $app->post('/:messageID/deliverAfter', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            $body = $app->request()->getBody();
            if (!isset($body['deliverAfter']) || empty($body["deliverAfter"]))
                throw new ValidationException("deliverAfter param is requied and must have valid datetime.", 403);

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isParty($app->mysqli, $userID)) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID");
                throw new ValidationException("Unauthorized access !!", 403);
            }

            if (!$msg->hold()) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID");
                throw new ValidationException("You are not allowed to set deliver after for this message as its not draft message.", 403);
            }

            $signatureRequired = isset($body['signatureRequired']) && $body['signatureRequired'] == false ? "NSR" : "SR";

            $utcDeliverAfter = "0000-00-00 00:00:00";
            $deliverAfter = $body["deliverAfter"];
            $isValidDateDeliverAfter = (bool) strtotime($deliverAfter);
            if (!$isValidDateDeliverAfter) {
                SECLOG("Invalid date for deliverAfter. deliverAfter = $deliverAfter " . var_export($body, true));
                $errors["deliverAfter"] = $body["deliverAfter"];
                throw new ValidationException("Problem with one of the request parameters.", 400, $errors);
            } else {
                $dbFormatDeliverAfter = date("Y-m-d H:i:s", strtotime($body["deliverAfter"]));
                $utcDeliverAfter = convertDateToUTC($dbFormatDeliverAfter, $app->user->timezone(), "Y-m-d H:i:s");
            }
            // update schedule time for this message
            $msg->setDeliverAfter($utcDeliverAfter, $signatureRequired);
            //$output["message"] = "Deliver after has been set successfully to $utcDeliverAfter";
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });
// Message - Set Unclaimed notice - CONSOLIDATED INTO PUT /OPTIONS
        $app->post('/:messageID/setUnclaimedNoticeTimeout', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            $body = $app->request()->getBody();
            if (!isset($body["unclaimedNoticeTimeout"]) || empty($body["unclaimedNoticeTimeout"])) {
                SECLOG("Invalid parameters unclaimedNoticeTimeout: " . var_export($body, true));
                throw new ValidationException("Problem with one of the request parameters. Invalid parameter unclaimedNoticeTimeout", 400);
            }
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isParty($app->mysqli, $userID) || $msg->ownerID() != $userID) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID " . var_export($_POST, true));
                throw new ValidationException("Unauthorized access !!", 403);
            }
            $unclaimedNoticeTimeout = $body["unclaimedNoticeTimeout"];
            if (!empty($body["unclaimedNoticeTimeout"])) {
                $isValidDate = (bool) strtotime($unclaimedNoticeTimeout);
                if (!$isValidDate) {
                    SECLOG("Invalid date for unclaimedNoticeTimeout. unclaimedNoticeTimeout = $unclaimedNoticeTimeout " . var_export($body, true));
                    throw new ValidationException("Problem with one of the request parameters.Invalid parameter unclaimedNoticeTimeout", 400);
                }
            }
            $db_format_timeout = date("Y-m-d H:i:s", strtotime($unclaimedNoticeTimeout));
            $sender = new \Docsmit\User($app->mysqli, $userID);
            $utc_unclaimedNoticeTimeout = convertDateToUTC($db_format_timeout, $sender->timezone(), "Y-m-d H:i:s");
            $party = new \Docsmit\Party($app->mysqli);
            // update unclaimedNoticeTimeout for all parties for this message including sender
            $party->setTimeout($messageID, $userID, $utc_unclaimedNoticeTimeout);
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Messages - Flagged by messageID - CONSOLIDATED INTO PUT /OPTIONS
        $app->post('/:messageID/flagged', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);

            if (false === $messageID) {
                throw new ValidationException("Invalid message !!", 403);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;

            if (!$msg->isParty($app->mysqli, $userID)) {
                throw new ValidationException("Unauthorized party !!", 400);
            }

            if (!$msg->isSent()) {
                throw new ValidationException("This message is not send yet.", 403);
            }

            $body = $app->request()->getBody();
            if (!isset($body["type"]))
                throw new ValidationException("Invalid parameters", 403);

            $type = mysqli_real_escape_string($app->mysqli, trim($body["type"]));
            $output = $msg->flagged($messageID, $userID, $type);

            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - Unflag - CONSOLIDATED INTO PUT /OPTIONS
        $app->post('/:messageID/unflag', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isRecipient($app->mysqli, $userID)) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID ");
                throw new ValidationException("Unauthorized access !!", 403);
            }
            $output = $msg->unflagged($messageID, $userID);
            //$output["success"] = $response["success"];
            //$output["message"] = $response["narr"];
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - Set bookmark - CONSOLIDATED INTO PUT /OPTIONS
        $app->post('/:messageID/bookmark', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isParty($app->mysqli, $userID)) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID");
                throw new ValidationException("Unauthorized access !!", 403);
            }
            $output = array();
            $party = new \Docsmit\Party($app->mysqli);
            $party->findByData($app->mysqli, $messageID, $userID);
            $current_status = $party->Bookmarked();
            $new_status = $current_status == 0 ? 1 : 0;
            //$message = $current_status == 0 ? "Message bookmarked successfully." : "Message un-bookmarked successfully.";
            $party->markBookmarked($app->mysqli, $messageID, $userID, $new_status);
            //$output["message"] = $message;

            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - add tags by messaageID - CONSOLIDATED INTO PUT /OPTIONS
        $app->post('/:messageID/tags', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            $body = $app->request()->getBody();
            if (!isset($body["taglist"])) {
                SECLOG("Invalid parameters taglist: " . var_export($body, true));
                throw new ValidationException("Problem with one of the request parameters. Invalid parameter taglist", 400);
            }
            $taglist = $body["taglist"];
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }

            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isParty($app->mysqli, $userID))
                throw new ValidationException("Unauthorized access !!", 403);

            \Docsmit\Tags::saveTags($app->mysqli, $taglist, $messageID, $userID);
            $output = array();
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - Add Party - CONSOLIDATED INTO POST /PARTY
        $app->post('/:messageID/addParty', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);

            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;

            if (!$msg->isSender($app->mysqli, $userID)) {
                throw new ValidationException("User does not have authority to perform this action on this message.", 403);
            }
            $body = (object) $app->request()->getBody();
            if (!isset($body->email))
                throw new ValidationException("Problem with one of the request parameters.", 400);

            $email = $body->email;

            $email = filter_var($email, FILTER_VALIDATE_EMAIL);

            if (false === $email) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            //$isSender = isset($body->isSender) ? 1 : 0;
            $isSender = 0;

            $output = $msg->addRecipient($email, $isSender, $userID);

            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        })->name("addParty");
// Message - Add new party by userId (s) - CONSOLIDATED INTO POST /PARTY
        $app->post('/:messageID/userId', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isSender($app->mysqli, $userID)) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID");
                throw new ValidationException("Unauthorized access !!", 403);
            }

            if ($msg->isSent())
                throw new ValidationException("This message is already been sent.", 403);

            $body = $app->request()->getBody();
            if (!isset($body['userIDs']))
                throw new ValidationException("Problem with one of the request parameters. userIDs param is required.", 400);

            $userIDs = $app->mysqli->escape_string($body['userIDs']);
            $userIDsArr = explode(',', $userIDs);

            if (empty($userIDsArr))
                throw new ValidationException("Atleast one user id is required for userIDs param.", 400);

            $result_arr = array();
            foreach ($userIDsArr as $contactId) {
                $c = $contactId;
                $contactId = filter_var($contactId, FILTER_VALIDATE_INT);
                if (false === $contactId) {
                    $result_arr[]["errors"][] = "`$c` is invalid user id.";
                    continue;
                }
                $newParty = new \Docsmit\User($app->mysqli, $contactId);
                if ($newParty->isEmpty()) {
                    $result_arr[]["errors"][] = "`" . $contactId . "`, This user id does not exist. ";
                    continue;
                }

                $email = $newParty->email();
                $name = $newParty->name();
                $company = $newParty->company();
                $result = array();
                if ($msg->isParty($app->mysqli, $contactId)) {
                    $result = array(
                        'partyID' => 0,
                        "userID" => $contactId,
                        'success' => "N",
                        'narr' => "'$email' is already a party.",
                    );
                } else {
                    $docsmRecipientID = $msg->addParty($app->mysqli, $contactId, $userID, "T");
                    $result = array(
                        "partyID" => $docsmRecipientID,
                        "userID" => $contactId,
                        "narr" => '',
                        "success" => "Y",
                        "name" => $name,
                        "company" => $company,
                        //"isSender" => 0
                    );
                }
                $result_arr[] = $result;
            }
            $app->response->body(json_encode($result_arr, JSON_PRETTY_PRINT));
            return;
        });
// Message - Add new party by emailID (s) - CONSOLIDATED INTO POST /PARTY
        $app->post('/:messageID/emails', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isSender($app->mysqli, $userID)) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID");
                throw new ValidationException("Unauthorized access !!", 403);
            }

            if ($msg->isSent())
                throw new ValidationException("This message is already been sent.", 403);

            $body = $app->request()->getBody();
            if (!isset($body['emailIDs']))
                throw new ValidationException("Problem with one of the request parameters. userIDs param is required.", 400);

            $emailIDs = $app->mysqli->escape_string($body['emailIDs']);
            $emailIDsArr = explode(',', $emailIDs);

            if (empty($emailIDsArr))
                throw new ValidationException("At least one email id is required for userIDs param.", 400);

            $result_arr = array();
            foreach ($emailIDsArr as $email) {
                $emailId = filter_var($email, FILTER_VALIDATE_EMAIL);
                if (false === $emailId) {
                    $result_arr[]["errors"][] = "`$email` is invalid email id.";
                    continue;
                }
                $newParty = new \Docsmit\User($app->mysqli);
                $newParty->findByEmail($app->mysqli, $email);
                $name = $newParty->name();
                $company = $newParty->company();
                $result = array();
                if ($newParty->isEmpty()) {
                    $msg = new \Docsmit\Message($app->mysqli, $messageID);
                    $newUserID = \Docsmit\User::insertProvisional($app->mysqli, $email);
                    $docsmRecipientID = $msg->addParty($app->mysqli, $newUserID, $userID, "T");
                    $result = array("partyID" => $docsmRecipientID,
                        "userID" => $newUserID,
                        "email" => $email,
                        "narr" => '',
                        "success" => "Y",
                        "name" => $name,
                        "company" => $company,
                        //"isSender" => 0
                    );
                } else {
                    $newUserID = $newParty->userID();
                    $msg = new \Docsmit\Message($app->mysqli, $messageID);
                    if ($msg->isParty($app->mysqli, $newParty->userID())) {
                        $result = array(
                            'partyID' => 0,
                            "userID" => $newUserID,
                            "email" => $email,
                            'success' => "N",
                            'narr' => "'$email' is already a party.");
                    } else {
                        $docsmRecipientID = $msg->addParty($app->mysqli, $newUserID, $userID, "T");
                        $result = array("partyID" => $docsmRecipientID,
                            "email" => $email,
                            "userID" => $newUserID,
                            "narr" => '',
                            "success" => "Y",
                            "name" => $name,
                            "company" => $company,
                            //"isSender" => 0
                        );
                    }
                }
                $result_arr[] = $result;
            }
            $app->response->body(json_encode($result_arr, JSON_PRETTY_PRINT));
            return;
        });
// Message - Get validateAddParty - DO NOT USE
        $app->get('/:messageID/validateAddParty/:email', function ($messageID, $email) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            $email = filter_var($email, FILTER_VALIDATE_EMAIL);
            if (false === $email) {
                throw new ValidationException("Problem with one of the request parameters. Invalid email address", 400);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isParty($app->mysqli, $userID)) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID ");
                throw new ValidationException("Unauthorized access !!", 403);
            }

            $party = new \Docsmit\User($app->mysqli);
            $party->findByEmail($app->mysqli, $email);
            if ($party->isEmpty()) {
                //$output["message"] = "'$email' will be added as a user.";
                $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
                return;
            } else {
                // already on the list?
                $partyUserID = $party->userID();
                if ($msg->isParty($app->mysqli, $partyUserID)) {
                    $output["message"] = "'$email' is already a party.";
                    $output["success"] = "N";
                    echo json_encode($output, JSON_PRETTY_PRINT);
                    return;
                }
                $partyData = array(
                    'userID' => $partyUserID,
                    'email' => $party->email() . "",
                    'name' => $party->name() . "",
                    'company' => $party->company() . ""
                );
                $app->response->body(json_encode($partyData, JSON_PRETTY_PRINT));
                return;
            }
        });

// Message - delete party
        $app->delete('/:messageID/party/:partyID', function ($messageID, $partyID) use ($app, $log) {
            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isSender($app->mysqli, $userID)) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID ");
                throw new ValidationException("Unauthorized access !!", 403);
            }
            if ($msg->isSent()) {
                throw new ValidationException("This message has already been sent.", 403);
            }
            if ($msg->partyIsParty($partyID)) {
                throw new ValidationException("Not a party to this message.", 403);
            }
            $deleted = $msg->delParty($mysqli, $partyID);
            $output = array(
                "partyID" => $partyID,
                "success" => $deleted
            );
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - emailReceipt
        $app->get('/:messageID/emailReceipt', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isSender($app->mysqli, $userID)) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID ");
                throw new ValidationException("Unauthorized access !!", 403);
            }
            if (!$msg->isSent()) {
                throw new ValidationException("This message is not send yet.", 403);
            }
            $attachment_html = $msg->attachmentsDescrHTML();
            $docsmOwner = new \Docsmit\User($app->mysqli, $msg->ownerID());
            $isEncrypted = $msg->isEncrypted();
            if ($isEncrypted)
                $msgBody = ENCRYPTED_MESSAGE_BODY_HEADING_TEXT;
            else
                $msgBody = $msg->mailMsg($app->mysqli);

            $ownerDocsmail = new \Docsmit\Docsmail;
            $ownerDocsmail->to = $app->user->email();
            $ownerDocsmail->subject = $docsmOwner->email() . " signed for your message";
            $ownerDocsmail->template = array(
                'source' => '1stview.html',
                'toLocal' => $app->user->localname(),
                'sender' => $docsmOwner->email(),
                'title' => $msg->title(),
                'msgBody' => $msgBody,
                'sent' => date_fmt($msg->sent(), $app->user->timezone(), D_DATETIME_TZ),
                'viewer' => $app->user->email(),
                'viewed' => date_fmt(SQLTime($app->mysqli), $app->user->timezone(), D_DATETIME_TZ),
                'attachment_html' => $attachment_html
            );
            if (!$ownerDocsmail->send()) {
                ERRLOG("Problem sending email receipt for $messageID!<BR> mail->getMessage() " . $ownerDocsmail->error);
            }
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - Update message options by messageID
        $app->put('/:messageID/options', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);

            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }

            $body = $app->request()->getBody();

            $user = $app->user;
            $userID = !empty($user->userID()) ? $user->userID() : null;

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            if (!$msg->isParty($app->mysqli, $userID) || $msg->ownerID() != $userID) {
                SECLOG("Unauthorized access, userID = $userID");
                throw new ValidationException("User does not have authority to perform this action on this message.", 403);
            }

            if (empty($body))
                throw new ValidationException("Atleast one message option is required to set message options.", 403);

            if (!empty($body["signatureRequired"])) {
                $validSignatureRequiredValues = array("true", "false", "1", "0");
                if (!in_array($body["signatureRequired"], $validSignatureRequiredValues)) {
                    throw new ValidationException("Invalid value supplied for `signatureRequired` parameter. valid inputs for signatureRequired param are " . implode(", ", $validSignatureRequiredValues), 403);
                }
            }
            $signatureRequired = isset($body["signatureRequired"]) && $body["signatureRequired"] == false ? "NSR" : "SR";

            $validMessageOptions = array("unclaimedNoticeTimeout", "deliverAfter", "excludeMessageBody",
                "blastMessage", "signatureRequired", "bookmark", "flagged", "tags", "type","unflag","billTo");
            $invalidMessageOptions = array_diff(array_keys($body), $validMessageOptions);
            $sp = count($invalidMessageOptions) <= 1 ? "is" : "are";
            if (!empty($invalidMessageOptions))
                throw new ValidationException(implode(",", $invalidMessageOptions) . " $sp invalid message options.", 403);

            $output = array();
            $utc_unclaimedNoticeTimeout = "0000-00-00 00:00:00";
            if (!empty($body["unclaimedNoticeTimeout"])) {
                $emailRecipients = $msg->emailRecipients();
                if(empty($emailRecipients))
                    throw new ValidationException("This message doesn't contain emailRecipients.", 403);

                $unclaimedNoticeTimeout = $body["unclaimedNoticeTimeout"];
                $isValidDate = (bool) strtotime($unclaimedNoticeTimeout);
                if (!$isValidDate) {
                    SECLOG("Invalid date for unclaimedNoticeTimeout. unclaimedNoticeTimeout = $unclaimedNoticeTimeout " . var_export($body, true));
                    $errors["unclaimedNoticeTimeout"] = $body["unclaimedNoticeTimeout"];
                    throw new ValidationException("Problem with one of the request parameters.", 400, $errors);
                } else {
                    $db_format_timeout = date("Y-m-d H:i:s", strtotime($unclaimedNoticeTimeout));
                    $utc_unclaimedNoticeTimeout = convertDateToUTC($db_format_timeout, $user->timezone(), "Y-m-d H:i:s");
                }

                $party = new \Docsmit\Party($app->mysqli);
                // update unclaimedNoticeTimeout for all parties for this message including sender
                $party->setTimeout($messageID, $userID, $utc_unclaimedNoticeTimeout);
                //$output["unclaimedNoticeTimeout"] = "`unclaimedNoticeTimeout` has been set to $utc_unclaimedNoticeTimeout successfully.";
            }

            if ($msg->hold()) {
                $utcDeliverAfter = "0000-00-00 00:00:00";
                if (!empty($body["deliverAfter"])) {
                    $deliverAfter = $body["deliverAfter"];
                    $isValidDateDeliverAfter = (bool) strtotime($deliverAfter);
                    if (!$isValidDateDeliverAfter) {
                        SECLOG("Invalid date for deliverAfter. deliverAfter = $deliverAfter " . var_export($body, true));
                        $errors["deliverAfter"] = $body["deliverAfter"];
                        throw new ValidationException("Problem with one of the request parameters.", 400, $errors);
                    } else {
                        $dbFormatDeliverAfter = date("Y-m-d H:i:s", strtotime($body["deliverAfter"]));
                        $utcDeliverAfter = convertDateToUTC($dbFormatDeliverAfter, $user->timezone(), "Y-m-d H:i:s");
                    }
                    // update schedule time for this message
                    $msg->setDeliverAfter($utcDeliverAfter, $signatureRequired);
                    //$output["deliverAfter"] = "`deliverAfter` has been set to $utcDeliverAfter successfully.";
                }
            }

            if (isset($body["excludeMessageBody"])) {
                $excludeMessageBody = $body["excludeMessageBody"] === true || $body["excludeMessageBody"] == 1 ? 1 : 0;
                $msg->setExcludeMessageBody($app->mysqli, $excludeMessageBody);
                //$output["excludeMessageBody"] = "`excludeMessageBody` has been set to $excludeMessageBody successfully.";
            }

            if (isset($body["blastMessage"])) {
                $blastMessage = $body["blastMessage"] === true || $body["blastMessage"] == 1 ? 1 : 0;
                $msg->setBlastMessage($blastMessage);
                //$output["blastMessage"] = "`blastMessage` has been set to $blastMessage successfully.";
            }

            if (isset($body["billTo"])) {
                $billTo = $body["billTo"];
                $msg->setBillTo($billTo);
                //$output["billTo"] = "`billTo` has been set to $billTo successfully.";
            }

            if (!empty($body["bookmark"])) {
                $party = new \Docsmit\Party($app->mysqli);
                $party->findByData($app->mysqli, $messageID, $userID);
                $current_status = $party->Bookmarked();
                $new_status = $current_status == 0 ? 1 : 0;
                $bookmarked = $party->markBookmarked($app->mysqli, $messageID, $userID, $new_status);
                //$output["bookmarked"] = $bookmarked;
            }

            if (!empty($body["flagged"])) {
                if (!$msg->isSent()) {
                    throw new ValidationException("This message is not send yet.", 403);
                }

                if (!isset($body["type"]))
                    throw new ValidationException("`type` parameter is required to flag the message.", 403);

                $type = mysqli_real_escape_string($app->mysqli, trim($body["type"]));
                $flaggedResult = $msg->flagged($messageID, $userID, $type);
                //$output["flagged"] = $flaggedResult;
            }

            if (!empty($body["unflag"])) {
                if (!$msg->isRecipient($app->mysqli, $userID)) {
                    SECLOG("Unauthorized access, userID = $userID, msgID = $messageID ");
                    throw new ValidationException("Unauthorized access !!", 403);
                }
                $flagged = new \Docsmit\Flagged($app->mysqli, $messageID, $userID);

                if (!$flagged->isFlagExist()) {
                    SECLOG("Flag does not exist !!, userID = $userID, msgID = $messageID ");
                    throw new ValidationException("Flag does not exist !!", 404);
                }
                $unflagged = $msg->unflagged($messageID, $userID);
                //$output["unflag"] = $unflagged;
            }

            if (!empty($body["tags"])) {
                $tags = $body["tags"];
                \Docsmit\Tags::saveTags($app->mysqli, $tags, $messageID, $userID);
            }

            $app->status(202);
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - GET message options by messageID
        $app->get('/:messageID/options', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);

            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }

            $get = $app->request()->get();

            $user = $app->user;
            $userID = !empty($user->userID()) ? $user->userID() : null;

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            if (!$msg->isParty($app->mysqli, $userID)) {
                SECLOG("Unauthorized access, userID = $userID");
                throw new ValidationException("User does not have authority to perform this action on this message.", 403);
            }

            $output = array();
            if ($msg->isSent()) {
                $party = new \Docsmit\Party($app->mysqli);
                $party->findByData($app->mysqli, $messageID, $userID);
                $output["unclaimedNoticeTimeout"] = $party->unclaimedNoticeTimeout();
                $output["bookmarked"] = $party->Bookmarked() ? "true" : "false";
            }

            if ($msg->hold()) {
                $output["deliverAfter"] = $msg->deliverAfter();
            }

            $output["excludeMessageBody"] = $msg->excludeMessageBody() ? "true" : "false";
            $output["blastMessage"] = $msg->isBlastMessage() ? "true" : "false";
            $output["billTo"] = $msg->billTo() ? $msg->billTo() : null;
            if (isset($get["tags"])) {
                $tags = new \Docsmit\Tags($app->mysqli, $messageID, $userID);
                if (!empty($tags->taglist()))
                    $output["tags"] = $tags->taglist();
            }

            if (isset($get["flags"])) {
                $output["isFlagged"] = $msg->hasFlagsFromRecipients() ? "true" : "false";
                $isSend = $msg->isSender($app->mysqli, $userID);
                $isReceive = $msg->isRecipient($app->mysqli, $userID);
                $flagged = new \Docsmit\Flagged($app->mysqli, $messageID, $userID);
                if($isReceive)
                $output["flaggedType"] = $flagged->type();
                if ($isSend) {
                    $output["msgflagList"] = $flagged->msgflagList($messageID);
                }
            }

            $app->status(202);
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - download zip
        $app->get('/:messageID/download/:zipID', function ($messageID, $zipID) use ($app, $log) {

            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            $zipID = filter_var($zipID, FILTER_VALIDATE_INT);

            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            if (false === $zipID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            if (!$msg->isParty($app->mysqli, $app->user->userID())) {
                throw new ValidationException("Not authorized to access this record. Unauthorized access attempts are violations of the Terms of Service and are recorded.", 403);
            }

            if (!$msg->isCertified()) {
                throw new ValidationException("Not certified!. Unauthorized access attempts are violations of the Terms of Service and are recorded.", 403);
            }

            $zip = new \Docsmit\Zip($app->mysqli, $zipID);
            $user = $app->user;
            $userID = !empty($user->userID()) ? $user->userID() : null;

            if (!$zip->canUserAccess($app->mysqli, $userID)) {
                throw new ValidationException("Authorization to zipID = $zipID denied for UserID = $userID .", 403);
            }

            if ($zip->userDLs($app->mysqli, $userID) == 0) {
                ////////////send an email to owner letting him know of this user's first DL.
                $docsmOwner = new \Docsmit\User($app->mysqli, $msg->ownerID());
                $dm = new \Docsmit\Docsmail;
                $dm->to = $docsmOwner->email();
                $dm->subject = "1st DL (by " . $user->email() . ")";
                $dm->template = array(
                    'source' => 'attachmentDelivered.html',
                    'title' => $msg->title(),
                    'sent' => date_fmt($msg->sent(), $docsmOwner->timezone(), D_DATETIME_TZ),
                    'viewer' => $user->email(),
                    'viewed' => date_fmt(SQLTime($app->mysqli), $docsmOwner->timezone(), D_DATETIME_TZ),
                    'downloaded' => date_fmt(SQLTime($app->mysqli), $docsmOwner->timezone(), D_DATETIME_TZ),
                    'recepient_name' => $docsmOwner->localname(),
                    'downloader' => $user->email(),
                    'sender' => $docsmOwner->email(),
                );
                if (!$dm->send()) {
                    ERRLOG("Problem notifying the owner of 1st download!<BR> mail->getMessage() " . $dm->error);
                    throw new ValidationException("Problem notifying the owner of 1st download!<BR> mail->getMessage() " . $dm->error, 403);
                }
            }

            if (!S3 and ! $zip->exists()) {
                ERRLOG("zipID $zipID not located in local storage!!");
                throw new ValidationException("zipID $zipID not located in local storage!! ", 403);
            }

            $zip->readfile();
            //$app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - transactions
        $app->get('/:messageID/transactions', function ($messageID) use ($app, $log) {

            $get = $app->request()->get();
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;

            if (false === filter_var($messageID, FILTER_VALIDATE_INT)) {
                throw new ValidationException("Invalid messageID.", 400);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }

            if (!$msg->isParty($app->mysqli, $userID)) {
                throw new ValidationException("Unauthorized access.", 403);
            }

            $searchParams = " AND t.MsgID = ?";

            $status = TRANSACTION_COMPLETED;
            $ar = array();
            if (isset($get['recipientID'])) {
                $recipientID = $get['recipientID'];
                if (false === filter_var($recipientID, FILTER_VALIDATE_INT)) {
                    throw new ValidationException("Invalid recipientID.", 400);
                }
                if ($stmt = $app->mysqli->prepare("SELECT TransactionID, DateTime,CoR, discount, price, Months,
                    Certs, MsgID, t.SRCertType, t.NSRCertType, m.SRSent, m.NSRSent, t.BillTo
                FROM transactions t
                JOIN messages m on m.message_id = t.MsgID
                WHERE Status = '$status' and PayorID = ?
                    and t.MsgID in (SELECT MessageID from parties where RecipientID = ?)
                    $searchParams
                GROUP BY t.TransactionID
                ORDER BY t.TransactionID;")) {
                    $stmt->bind_param('iii', $userID, $recipientID, $messageID); // Bind "$email" to parameter.
                }
            } elseif (isset($get['from']) or isset($get['to'])) {
                if (isset($get['to']) and isset($get['from']) and $get['to'] != '' and $get['from'] != '') {
                    $to = $get['to'];
                    $from = $get['from'];
                    if (!validateDate($to,"Y-m-d"))
                        throw new ValidationException("Invalid `to` date.", 400);

                    if (!validateDate($from,"Y-m-d"))
                        throw new ValidationException("Invalid `from` date.", 400);

                    $interval = "t.DateTime between cast(? as date) and cast(? as date)";
                } elseif (isset($get['to']) and $get['to'] != '') {
                    $to = $get['to'];
                    if (!validateDate($to,"Y-m-d"))
                        throw new ValidationException("Invalid `to` date.", 400);
                    $interval = "t.DateTime <= cast(? as date)";
                } elseif (isset($get['from']) and $get['from'] != '') {
                    $from = $get['from'];
                    if (!validateDate($from,"Y-m-d"))
                        throw new ValidationException("Invalid `from` date.", 400);

                    $interval = "t.DateTime >= cast(? as date)";
                }

                $query = "SELECT TransactionID, DateTime,CoR, discount, price, Months, Certs, MsgID, t.SRCertType, t.NSRCertType,
                m.SRSent, m.NSRSent, t.BillTo
                FROM transactions t
                JOIN messages m on m.message_id = t.MsgID
                WHERE Status = '$status' and PayorID = ?
                    and $interval $searchParams
                GROUP BY t.TransactionID
                ORDER BY t.TransactionID;";

                if ($stmt = $app->mysqli->prepare($query)) {
                    if (isset($get['to']) and isset($get['from']) and $get['to'] != '' and $get['from'] != '') {
                        $stmt->bind_param('issi', $userID, $from, $to, $messageID);
                    } elseif (isset($get['to']) and $get['to'] != '') {
                        $stmt->bind_param('isi', $userID, $to, $messageID);
                    } elseif (isset($get['from']) and $get['from'] != '') {
                        $stmt->bind_param('isi', $userID, $from, $messageID);
                    }
                }
            } else {
                if ($stmt = $app->mysqli->prepare("SELECT TransactionID,DateTime, CoR, discount, price, Months, Certs, MsgID,
                    t.SRCertType, t.NSRCertType, m.SRSent, m.NSRSent, t.BillTo
                    FROM transactions t
                    left JOIN messages m on m.message_id = t.MsgID
                    WHERE Status = '$status' and PayorID = ?
                    $searchParams
                    GROUP BY t.TransactionID
                    ORDER BY t.DateTime;")) {
                    $stmt->bind_param('ii', $userID, $messageID); // Bind "$email" to parameter.
                }
            }

            $recipients = $msg->emailRecipients();

            if ($stmt->execute()) {
                $stmt->bind_result($TransactionID, $DateTime, $CoR, $discount, $price,
                        $Months, $Certs, $MsgID, $SRCertType, $NSRCertType, $SRSent, $NSRSent,$BillTo);
                while ($stmt->fetch()) {
                    $CertType = $SRCertType;
                    if ($NSRSent != '0000-00-00 00:00:00')
                        $CertType = $NSRCertType;

                    $ar[] = array(
                        'TransactionID' => $TransactionID,
                        'DateTime' => $DateTime,
                        'CoR' => $CoR,
                        'discount' => (float) $discount,
                        'price' => (float) $price,
                        'Months' => $Months,
                        'Certs' => $Certs,
                        'MsgID' => $MsgID,
                        'CertType' => $CertType,
                        'BillTo' => $BillTo,
                        'recipients' => $recipients);
                }
            }

            $app->response->body(json_encode($ar, JSON_PRETTY_PRINT));
            return;
        });
// Message - Get the price and the details
        $app->get('/:messageID/:priceAPIName', function ($messageID, $priceAPIName) use ($app, $log) {
            if (false === filter_var($messageID, FILTER_VALIDATE_INT)) {
                throw new ValidationException("Invalid MessageID.", 400);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isParty($app->mysqli, $userID)) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID ");
                throw new ValidationException("Unauthorized access!", 403);
            }

            $get = $app->request()->get();

//            if (!isset($get['certify']))
//                throw new ValidationException("`certify` param is required.", 400);

//            if (!isset($get['months']))
//                throw new ValidationException("`months` param is required.", 400);

//            if (!isset($get['sendType']))
//                throw new ValidationException("`sendType` param is required.", 400);

            $certify = isset($get['certify']) ? $get['certify'] : 1;
            $months = isset($get['months']) ?  filter_var($get['months'], FILTER_VALIDATE_INT) : 12;
            $sendType = isset($get['sendType']) ? $app->mysqli->escape_string($get["sendType"]) : 'SR';
            $validInputCertify = array("true", "false", "1", "0");
            if (!in_array($certify, $validInputCertify)) {
                throw new ValidationException("Invalid value supplied for `certify` parameter. valid inputs for certify param are " . implode(", ", $validInputCertify), 403);
            }

  //          $months = filter_var($get['months'], FILTER_VALIDATE_INT);
            if (false === $months) {
                throw new ValidationException("Invalid value supplied to `months` parameter.", 400);
            }

            $validInputMonths = array(12, 36, 84);
            if (!in_array($months, $validInputMonths)) {
                throw new ValidationException("Invalid value supplied for `months` parameter. valid inputs for months param are " . implode(", ", $validInputMonths), 403);
            }

//            $sendType = $app->mysqli->escape_string($get["sendType"]);
            $validSendTypes = array("SR", "NSR");
            if (!in_array($sendType, $validSendTypes)) {
                throw new ValidationException("Invalid value supplied for `sendType` parameter. valid inputs for sendType param are " . implode(", ", $validSendTypes), 403);
            }

            $pm = new \Docsmit\PricingModel($app->mysqli, $userID, $messageID);
            $pm->setupConsume($certify, $months, $sendType, true);
            $output["netPrice"] = $pm->getNetPrice(true);
            $output["details"] = $pm->description();

            if ($priceAPIName == "priceDetails")
                unset($output["details"]);

            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        })->conditions(array('priceAPIName' => '(priceCheck|priceDetails)'));

// Message - Check Email Attachments FileSize Limit
        $app->post('/:messageID/:sizeLimitAPIName', function ($messageID, $sizeLimitAPIName) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }
            $body = $app->request()->getBody();
            if (empty($body["size"]))
                throw new ValidationException("`size` param is requied and must have valid size.", 403);

            $size = filter_var($body["size"], FILTER_VALIDATE_INT);
            if (false === $size) {
                throw new ValidationException("Problem with one of the request parameters. Invalid value supplied to size param.", 400);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isParty($app->mysqli, $userID)) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID");
                throw new ValidationException("Unauthorized access !!", 403);
            }
            $output = array();
            if ($sizeLimitAPIName == "checkEmailAttachmentsFileSizeLimit") {
                $totalSize = \Docsmit\File::getTotalAttachmentsFileSize($app->mysqli, $userID, $messageID);
                $requiredSize = (int) $totalSize + $size;
                $totalAttachmentFilesMaxLimit = MAX_UPLOAD_ATTACHMENT_FILESIZE * MEGABYTE; // 300 mb
                $output = array('size' => $size, 'totalSize' => $totalSize, 'requiredSize' => $requiredSize, 'totalAttachmentFilesMaxLimit' => $totalAttachmentFilesMaxLimit);
            } elseif ($sizeLimitAPIName == "checkBullpenFileSizeLimit") {
                $totalSize = \Docsmit\File::getTotalBullpenFileSize($app->mysqli, $userID);
                $requiredSize = (int) $totalSize + $size;
                $bullpenFilesMaxLimit = MAX_UPLOAD_BULLPEN_FILESIZE * MEGABYTE; // 100 mb
                $output = array('size' => $size, 'totalSize' => $totalSize, 'requiredSize' => $requiredSize, 'bullpenFilesMaxLimit' => $bullpenFilesMaxLimit);
            }

            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        })->conditions(array('sizeLimitAPIName' => '(checkEmailAttachmentsFileSizeLimit|checkBullpenFileSizeLimit)'));
// Message - msgPreview
        $app->get('/:messageID/msgPreview', function ($messageID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);
            if (false === $messageID) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1) {
                throw new ValidationException("MessageID not found.", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if (!$msg->isParty($app->mysqli, $userID)) {
                SECLOG("Unauthorized access, userID = $userID, msgID = $messageID ");
                throw new ValidationException("Unauthorized access !!", 403);
            }

            $from = new \Docsmit\User($app->mysqli, $msg->ownerID());
            if ($msg->notViewedYet($app->mysqli, $userID) && !$msg->isSender($app->mysqli, $userID)) {
                $output["messageID"] = $messageID;
                $output["sent"] = $msg->sent();
                $output["from"] = $from->email();
                $output["msg"] = "<BR>*** Not Viewed Yet ***";
            } else {
                $output["messageID"] = $messageID;
                $output["sent"] = $msg->sent();
                $output["from"] = $from->email();
                $output["msg"] = $msg->mailMsg($app->mysqli);
            }

            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - Update mail party (Physical mail party)
        $app->put('/:messageID/party/:partyID', function ($messageID, $partyID) use ($app, $log) {

            if (false === filter_var($messageID, FILTER_VALIDATE_INT)) {
                throw new ValidationException("Invalid messageID:" . $messageID, 400);
            }

            if (false === filter_var($partyID, FILTER_VALIDATE_INT)) {
                throw new ValidationException("Invalid partyID: " . $partyID, 400);
            }

            $fields = array(
                "firstName",
                "lastName",
                "organization",
                "address1",
                "address2",
                "address3",
                "city",
                "state",
                "postalCode",
                "countryNonUS",
            );

            $body = (object) $app->request()->getBody();

            if (!isset($body->firstName) &&
                    !isset($body->lastName) &&
                    !isset($body->organization) &&
                    !isset($body->address1) &&
                    !isset($body->address2) &&
                    !isset($body->address3) &&
                    !isset($body->city) &&
                    !isset($body->state) &&
                    !isset($body->postalCode) &&
                    !isset($body->countryNonUS)
            ) {
                throw new ValidationException("At least one field is required from these fields. (" . implode(", ", $fields) . ")", 400);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            if ($msg->messageID() == -1)
                throw new ValidationException("Message not found: " . $messageID, 400);

            $party = new \Docsmit\Party($app->mysqli, $partyID);
            if (!$party->isUSPSParty()) {
                SECLOG("Not a mail party: " . $partyID);
                throw new ValidationException("Not a mail party: " . $partyID, 400);
            }

            $mailAddressID = $party->mailAddressID();
            $mailAddress = new \Docsmit\MailAddress($app->mysqli, $mailAddressID);
            $output = array();
            if (!$mailAddress->isOK()) {
                SECLOG("mail party address not found: " . $mailAddressID);
                throw new ValidationException("Mail party address not found: " . $mailAddressID, 400);
            }

            if (!empty($body->firstName))
                $mailAddress->setFirstName($body->firstName);
            if (!empty($body->lastName))
                $mailAddress->setLastName($body->lastName);
            if (!empty($body->organization))
                $mailAddress->setOrganization($body->organization);
            if (!empty($body->address1))
                $mailAddress->setAddress1($body->address1);
            if (!empty($body->address2))
                $mailAddress->setAddress2($body->address2);
            if (!empty($body->address3))
                $mailAddress->setAddress3($body->address3);
            if (!empty($body->city))
                $mailAddress->setCity($body->city);

            $body->state = abbreviateState($body->state);
            if (!empty($body->state)) {
                if (!isValidState($body->state))
                    throw new ValidationException("Invalid state. Please use valid state abbrev. e.g. 'AL','AK','CA','CT','DE','FL','GA','HI','LA'", 400);
                $mailAddress->setState($body->state);
            }
            if (!empty($body->postalCode)) {
                if (!isValidPostalCode($body->postalCode))
                    throw new ValidationException("Invalid postalCode. Please use valid postalCode. i.e. 12345 or 12345-5434", 400);
                $mailAddress->setPostalCode($body->postalCode);
            }
            if (!empty($body->countryNonUS))
                $mailAddress->setCountryNonUS($body->countryNonUS);
            if (!empty($body->endorsement))
                $mailAddress->setEndorsement($body->endorsement);

            $app->status(202);
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - messageID - delete file by fileID
        $app->delete('/:messageID/file/:fileID', function ($messageID, $fileID) use ($app, $log) {
            $messageID = filter_var($messageID, FILTER_VALIDATE_INT);

            if (false === $messageID) {
                throw new ValidationException("Invalid message !!", 403);
            }

            $fileID = filter_var($fileID, FILTER_VALIDATE_INT);

            if (false === $fileID) {
                throw new ValidationException("Invalid file !!", 403);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;

            if (!$msg->isParty($app->mysqli, $userID)) {
                throw new ValidationException("Unauthorized party !!", 400);
            }

            if ($msg->isSent()) {
                throw new ValidationException("This message has already been sent.", 403);
            }

            $file = new \Docsmit\File($app->mysqli, $fileID);
            if (!$file->canUserAccess($app->mysqli, $userID))
                throw new ValidationException("Authorization to fileID = $fileID denied for UserID = $userID !!", 400);

            if (!S3 and ! $file->exists())
                throw new ValidationException("FileID $fileID not located in local storage!!", 404);

            if ($file->Deleted())
                throw new ValidationException("This file is already been deleted !!", 400);

            $file->setDeleted();

            if (!$file->Deleted())
                throw new ValidationException("Error while deleting file!!", 400);

            $app->status(202);
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });
// Message - messageID - update return address
        $app->put('/:messageID/returnAddr', function ($messageID) use ($app, $log) {

            if (false === filter_var($messageID, FILTER_VALIDATE_INT)) {
                throw new ValidationException("Invalid messageID:" . $messageID, 400);
            }

            $msg = new \Docsmit\Message($app->mysqli, $messageID);
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;

            if (!$msg->isParty($app->mysqli, $userID)) {
                throw new ValidationException("Unauthorized party !!", 400);
            }

            if ($msg->isSent()) {
                throw new ValidationException("This message has already been sent.", 403);
            }
            $body = $app->request()->getBody();
            $body = (object) $body;
            $output = array();
            $rtnName = isset($body->rtnName) ? $app->mysqli->escape_string($body->rtnName) : null;
            $rtnOrganization = isset($body->rtnOrganization) ? $app->mysqli->escape_string($body->rtnOrganization) : null;
            $rtnAddress1 = isset($body->rtnAddress1) ? $app->mysqli->escape_string($body->rtnAddress1) : null;
            $rtnAddress2 = isset($body->rtnAddress2) ? $app->mysqli->escape_string($body->rtnAddress2) : null;
            $rtnCity = isset($body->rtnCity) ? $app->mysqli->escape_string($body->rtnCity) : null;
            $rtnState = isset($body->rtnState) ? $app->mysqli->escape_string(strtoupper($body->rtnState)) : null;
            $rtnZip = isset($body->rtnZip) ? $app->mysqli->escape_string($body->rtnZip) : null;
            if (!empty($rtnState)) {
                if (!isValidState(strtoupper($rtnState)))
                    throw new ValidationException("Invalid state. Please use valid state code. i.e. 'AL','AK','CA','CT','DE','FL','GA','HI','LA'", 400);
            }
            if (!empty($body->rtnZip)) {
                if (!isValidPostalCode($body->rtnZip))
                    throw new ValidationException("Invalid postalCode. Please use valid postalCode. i.e. 12345 or 12345-5434", 400);
            }
            if ((!empty($rtnName) || !empty($rtnOrganization)) && !empty($rtnAddress1)) {
                $msg->setRtnAddress($rtnName, $rtnOrganization, $rtnAddress1, $rtnAddress2, $rtnCity, $rtnState, $rtnZip);
            } else {
                throw new ValidationException("`rtnName` or `rtnOrganization` and `rtnAddress1` are required to set return address.", 400);
            }

            $app->status(202);
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });

    }); // messages API group - End

// Group for account API
    $app->group('/account', function () use ($app, $log) {

        $app->get('/info', function () use ($app, $log) {
            $output["userID"] = $app->user->userID();
            $output["name"] = $app->user->name();
            $output["email"] = $app->user->email();
            $output["company"] = $app->user->company();
            $output["address1"] = $app->user->address1();
            $output["address2"] = $app->user->address2();
            $output["city"] = $app->user->city();
            $output["state"] = $app->user->state();
            $output["zip"] = $app->user->zip();
//            $output["softwareIDs"] = $app->user->softwareIDs();
//            $output["suspended"] = $app->user->suspended();
            $timezone_as_number = null;
            foreach (timezoneList() as $t) {
                if($app->user->timezone() == $t['zone'])
                    $timezone_as_number = $t['diff_from_GMT'];
            }
            $output["timezone"] = $app->user->timezone();
            $output["timezoneAsNumber"] = $timezone_as_number;
            $output["attachmentFilesMaxLimit"] = MAX_UPLOAD_ATTACHMENT_FILESIZE * MEGABYTE; // 300 mb;
//            $output["bullpenFilesMaxLimit"] = MAX_UPLOAD_BULLPEN_FILESIZE * MEGABYTE; // 100 mb;
            $userSettings = new \Docsmit\UserSettings($app->mysqli, $app->user->userID());
            $output["creditBalance"] = $userSettings->creditBalance();
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });

        $app->put('/address', function () use ($app, $log) {

            $body = (object) $app->request()->getBody();
            $user = $app->user;
            //$userID = !empty($user->userID()) ? $user->userID() : null;

            if (empty($body))
                throw new ValidationException("Please pass address data.", 403);

            $missing = array();
            if (!isset($body->address1)) $missing[] = "address1";
            if (!isset($body->address2)) $missing[] = "address2";
            if (!isset($body->city)) $missing[] = "city";
            if (!isset($body->state)) $missing[] = "state";
            if (!isset($body->zip)) $missing[] = "zip";

            if (count($missing) !=0)
                throw new ValidationException("Missing in user address: ". implode(", ", $missing) . ".", 403);

            $name = isset($body->name) ? $body->name : null;
            $company = isset($body->company) ? $body->company : null;
            $address1 = $body->address1;
            $address2 = $body->address2;
            $city = $body->city;
            $state = abbreviateState($body->state);
            $zip = $body->zip;

            $output = array();
            if(!isValidState($state)){
                throw new ValidationException("Invalid state given for user address.", 403);
            }

            if(!isValidPostalCode($zip)){
                throw new ValidationException("Invalid zip code given for user address.", 403);
            }

            if(!empty($address1) || !empty($address2) || !empty($city) || !empty($state) || !empty($zip))
                $user->setAddress($address1,$address2,$city, $state, $zip);

            if ($name != null)
                $user->setName($app->mysqli,$name);

            if ($company != null)
                $user->setCompany($app->mysqli,$company);

            $output["name"] = $user->name();
            $output["company"] = $user->company();
            $output["address1"] = $user->address1();
            $output["address2"] = $user->address2();
            $output["city"] = $user->city();
            $output["state"] = $user->state();
            $output["zip"] = $user->zip();

            $app->status(202);
            $app->response->body(json_encode($output, JSON_PRETTY_PRINT));
            return;
        });

    }); // Group for account API - End

// Group for contacts API
    $app->group('/contacts', function () use ($app, $log) {

// Get contacts
        $app->get('/', function () use ($app, $log) {
            //pr($app->user,1);
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            $contacts = array();
            $total = 0;
            $recentContact = new \Docsmit\RecentContact($app->mysqli);
            $recentContacts = $recentContact->getUserRecentcontacts($app->mysqli, $userID);
            if (!empty($recentContacts)) {
                foreach ($recentContacts as $contact) {
                    $contactInfo = array('userID' => $contact['userid'],
                        'name' => $contact['name'],
                        'company' => $contact['company'],
                        'email' => $contact['email'],
                        'address1' => $contact['address1'],
                        'taglist' => $contact['TagList'],
                            //'taglist' => !empty($row[5]) ? explode(",",$row[5]) : $row[5]
                    );
                    array_push($contacts, $contactInfo); // Add user.
                }

                if (empty($total)) {
                    $total = count($contacts);
                }
            }
            $app->response->headers->set('X-Total-Count', $total);
            echo json_encode($contacts, JSON_PRETTY_PRINT);
            return;
        });
// Get contact using contactID
        $app->get('/:contactID', function ($contactID) use ($app, $log) {
            //pr($app->user,1);
            $user = $app->user;
            $userID = !empty($user->userID()) ? $user->userID() : null;
            $contactID = filter_var($contactID, FILTER_VALIDATE_INT);

            if (false === $contactID) {
                throw new ValidationException("Invalid contact ID", 400);
            }

            $cUser = new \Docsmit\User($app->mysqli, $contactID);
            if ($cUser->isEmpty()) {
                throw new ValidationException("contact not found !!", 404);
            }
            if (!$user->isContactOwner($contactID)) {
                throw new ValidationException("Unauthorized access !!", 400);
            }
            $rContact = new \Docsmit\RecentContact($app->mysqli, $userID, $contactID);
            $contactInfo = array('userID' => $cUser->userID(),
                'name' => $cUser->name(),
                'company' => $cUser->company(),
                'email' => $cUser->email(),
                'address1' => $cUser->address1(),
                'taglist' => $rContact->taglist(),
            );

            $total = count($contactInfo);
            $app->response->headers->set('X-Total-Count', $total);
            echo json_encode($contactInfo, JSON_PRETTY_PRINT);
            return;
        });
// Update contact with contactID
        $app->post('/:contactID', function ($contactID) use ($app, $log) {
            $contactID = filter_var($contactID, FILTER_VALIDATE_INT);

            if (false === $contactID) {
                throw new ValidationException("Invalid contactID", 400);
            }

            $body = $app->request()->getBody();
            $errors = $app->validateContact($body);
            if (!empty($errors))
                throw new ValidationException("Invalid data", 400, $errors);

            $contactUser = new \Docsmit\User($app->mysqli, $contactID);

            if ($contactUser->isEmpty())
                throw new ValidationException("Contact not found !!", 404);

            $user = $app->user;
            $userID = !empty($user->userID()) ? $user->userID() : null;

            if (!$user->isContactOwner($contactID)) {
                SECLOG("Unauthorized access, user_id : $userID, contact id: $contactID");
                throw new ValidationException("Unauthorized access !!", 400);
            }

            $taglist = $app->mysqli->escape_string($body['taglist']);
            $contactTags = new \Docsmit\RecentContact($app->mysqli, $userID, $contactID);
            $contactTags->saveContactTags($taglist);
            //$output["narr"] = "Contact details has been updated successfully.";
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });
// Delete contact with contactID
        $app->delete('/:contactID', function ($contactID) use ($app, $log) {

            $contactID = filter_var($contactID, FILTER_VALIDATE_INT);

            if (false === $contactID) {
                throw new ValidationException("Invalid contactID", 403);
            }

            $contactUser = new \Docsmit\User($app->mysqli, $contactID);
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if ($contactUser->isEmpty())
                throw new ValidationException("Contact user not found", 404);

            $deleted = \Docsmit\User::deleteRecentContact($app->mysqli, $userID, $contactID);

            $output["deleted"] = $deleted;
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });
    }); // contacts API group - End
// Group for handshakes API
    $app->group('/handshakes', function () use ($app, $log) {

//Get - Handshakes
        $app->get('/', function () use ($app, $log) {
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            $offeredList = \Docsmit\Handshake::offeredList($app->mysqli, $userID);
            $offereds = array();
            foreach ($offeredList as $k => $v) {
                $offered = array('handshake_id' => $v['id'],
                    'receiver' => $v['receiver_email'],
                    'receiver_is_provisional' => $v['receiver_is_provisional'],
                    'sender_accepted' => $v['sender_accepted_time'],
                    'sent' => $v['send_time']);
                array_push($offereds, $offered); // Add user.
            }

            $receivedList = \Docsmit\Handshake::receivedList($app->mysqli, $userID);
            //pr($receivedList,1);
            $receiveds = array();
            $received_email = null;
            foreach ($receivedList as $k => $v) {
                $received = array('handshake_id' => $v['id'],
                    'sender' => $v['sender_email'],
                    'sender_accepted' => $v['sender_accepted_time'],
                    'sent' => $v['send_time'],
                    'receiver_accepted_time' => $v['receiver_accepted_time']);
                array_push($receiveds, $received); // Add user.
            }

            $acceptedList = \Docsmit\Handshake::acceptedList($app->mysqli, $userID);
            $accepteds = array();
            $accepted_email = null;
            foreach ($acceptedList as $k => $v) {
                $isSender = $userID == $v['sender_id'] ? 1 : 0;
                $accepted = array('handshake_id' => $v['id'],
                    'receiver' => $isSender ? $v['receiver_email'] : $v['sender_email'],
                    'sender_accepted' => $v['sender_accepted_time'],
                    'sent' => $v['send_time']);
                array_push($accepteds, $accepted); // Add user.
            }

            $lists = array(
                'offereds' => $offereds,
                'receiveds' => $receiveds,
                'accepteds' => $accepteds
            );
            echo json_encode($lists, JSON_PRETTY_PRINT);
            return;
        });
//Offer a Handshake
        $app->post('/handshakes/offer', function () use ($app, $log) {
            $body = $app->request()->getBody();
            if (!isset($body['handshake_email'])) {
                throw new ValidationException("Problem with one of the request parameters. Invalid parameter shandshake email", 400);
            }

            if (!isset($body['message'])) {
                throw new ValidationException("Problem with one of the request parameters. Invalid parameter message", 400);
            }

            $handshake_email = $body['handshake_email'];
            $message = $body['message'];
            $email = filter_var($handshake_email, FILTER_VALIDATE_EMAIL);

            if (false === $email) {
                throw new ValidationException("Problem with one of the request parameters.", 400);
            }

            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;

            $sender = new \Docsmit\User($app->mysqli, $userID);
            $sender_email = $sender->email();
            $sender_localname = $sender->localname();
            $receiver_email = $handshake_email;
            $receiver = new \Docsmit\User($app->mysqli);
            $receiver->findByEmail($app->mysqli, $receiver_email);
            $handshakeID = '';
            if ($receiver->userID() <= 0) {
                SECLOG("User Not Registered, Create new User account in system and send Handshake email. receiver=handshake emial : $receiver_email");
                // User Not Registered, Create new User account in system and send Handshake email.
                $receiverID = \Docsmit\User::insertProvisional($app->mysqli, $receiver_email);
                $newUser = new \Docsmit\User($app->mysqli, $receiverID);
                $key = $newUser->activation();
                $handshakeID = \Docsmit\Handshake::insert($app->mysqli, $userID, $receiverID, $message);
                $accept_link = SECUREURL . "/activate_provisional_form.php?email=" . $receiver_email .
                        "&amp;key=" . $key . "&amp;hs=" . $handshakeID . "&amp;location=" . SECUREURL . "/contacts.php";

                // send handshake mail to user
                $recipMail = new \Docsmit\Docsmail;
                $recipMail->to = $receiver_email;
                $recipMail->subject = "Handshake Invitation";
                $recipMail->template = array(
                    'source' => 'handshake_provisional_user.html',
                    'receiver_localname' => $newUser->localname(),
                    'sender_email' => $sender_email,
                    'message' => $message,
                    'accept_link' => $accept_link
                );
                $recipMail->send();
                SECLOG("Handshake sent successfully to provisional user (receiver : $receiver_email, handshakeID : $handshakeID");

                // Send email to Sender
                \Docsmit\Handshake::senderHandshakeRequest($handshakeID, $sender_email, $receiver_email, $sender_localname, $message);
                SECLOG("Send email to Sender, user is provisional : $sender_email, Receiver : $receiver_email, Handshake : $handshakeID  ");

                $output = array(
                    "offered_handshake" => array(
                        "handshake_id" => $handshakeID,
                        "receiver" => $receiver_email,
                        "receiver_is_provisional" => 1,
                    )
                );
            } else {
                // User Already Registered with System
                $receiverID = $receiver->userID();
                SECLOG("Offer handshake : User Already Registered with System, Receiver : $receiver_email");
                $handshake_already_sent = \Docsmit\Handshake::checkHandshake($app->mysqli, $userID, $receiverID);
                SECLOG("Check handshake already sent to user : $handshake_already_sent = $receiver_email");
                if ($handshake_already_sent == 0) {
                    $handshakeID = \Docsmit\Handshake::insert($app->mysqli, $userID, $receiverID, $message);
                    $accept_link = SECUREURL . '/accept_handshake.php?hs=' . $handshakeID;
                    $recipMail = new \Docsmit\Docsmail;
                    $recipMail->to = $receiver_email;
                    $recipMail->subject = "Handshake Invitation";
                    $recipMail->template = array(
                        'source' => 'handshakeRequest-Receiver.html',
                        'receiver_localname' => $receiver->localname(),
                        'sender_email' => $sender_email,
                        'message' => $message,
                        'accept_link' => $accept_link
                    );
                    $recipMail->send();
                    SECLOG("Handshake sent successfully to user already registered with system, (receiver : $receiver_email, handshakeID : $handshakeID");
                    $output = array(
                        "offered_handshake" => array(
                            "handshake_id" => $handshakeID,
                            "receiver" => $receiver_email,
                            "receiver_is_provisional" => 0,
                        )
                    );
                    // Send email to Sender
                    Docsmit\Handshake::senderHandshakeRequest($handshakeID, $sender_email, $receiver_email, $sender_localname, $message);
                    SECLOG("Send email to Sender - user already registered : $sender_email, Receiver : $receiver_email, Handshake : $handshakeID  ");
                } else {
                    throw new ValidationException("Handshake already sent to specified email address.", 403);
                }
            }
            \Docsmit\User::addRecentContact($app->mysqli, $userID, $receiverID);
            SECLOG("Contact added to recent contacts, user_id : $userID, receiver : $receiverID");
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });
//Remind Handshake
        $app->post('/handshakes/:handshakeID/reminder', function ($handshakeID) use ($app, $log) {
            $handshakeID = filter_var($handshakeID, FILTER_VALIDATE_INT);

            if (false === $handshakeID) {
                throw new ValidationException("Problem with one of the request parameters.Invalid HandshakeID", 400);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            $sender = new \Docsmit\User($app->mysqli, $userID);
            $sender_email = $sender->email();
            $sender_localname = $sender->localname();
            //get handshake details
            $handshake = new \Docsmit\Handshake($app->mysqli, $handshakeID);
            if (empty($handshake->handshake_id())) {
                throw new ValidationException("HandshakeID not found", 404);
            }
            if ($handshake->sender_id() != $userID) {
                throw new ValidationException("Unauthorized access !!", 403);
            }
            $receiver_id = $handshake->receiver_id();
            $message = $handshake->message();
            //get receiver details by email
            $receiver_details = new \Docsmit\User($app->mysqli, $receiver_id);
            $key = $receiver_details->activation();
            $receiver_email = $receiver_details->email();
            $accept_link = SECUREURL . "/activate_provisional_form.php?email=" . $receiver_email .
                    "&amp;key=" . $key . "&amp;hs=" . $handshake->handshake_id();
            //send reminder mail to receiver
            $recipMail = new \Docsmit\Docsmail;
            $recipMail->to = $receiver_email;
            $recipMail->subject = "Handshake Invitation reminder";
            $recipMail->template = array(
                'source' => 'handshake_reminder.html',
                'receiver_localname' => $receiver_details->localname(),
                'sender_email' => $sender_email,
                'message' => $message,
                'accept_link' => $accept_link
            );
            $recipMail->send();
            DEBLOG("Handshake reminder has been sent, sender : $sender_email, hanshake id : $handshakeID, receiver : $receiver_email");
            $output["handshakeID"] = $handshakeID;
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });
//Remind Resend
        $app->post('/handshakes/:handshakeID/resend', function ($handshakeID) use ($app, $log) {
            $handshakeID = filter_var($handshakeID, FILTER_VALIDATE_INT);

            if (false === $handshakeID) {
                throw new ValidationException("Problem with one of the request parameter.Invalid HandshakeID", 403);
            }
            //get handshake details
            $handshake = new \Docsmit\Handshake($app->mysqli, $handshakeID);
            if (empty($handshake->handshake_id())) {
                throw new ValidationException("HandshakeID not found", 404);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            if ($handshake->sender_id() != $userID) {
                throw new ValidationException("Unauthorized access !!", 403);
            }
            $receiver_id = $handshake->receiver_id();
            $sender_id = $handshake->sender_id();
            $message = $handshake->message();

            $receiver = new \Docsmit\User($app->mysqli, $receiver_id);
            $receiver_email = $receiver->email();
            $receiver_localname = $receiver->localname();

            $sender = new \Docsmit\User($app->mysqli, $sender_id);
            $sender_email = $sender->email();
            $sender_localname = $sender->localname();
            //If receiver is provisional user
            if ($receiver->isProvisional()) {
                SECLOG("Resend handshake mail to provisional user. receiver=handshake emial : $receiver_email");
                $key = $receiver->activation();
                $accept_link = SECUREURL . "/activate_provisional_form.php?email=" . $receiver_email .
                        "&amp;key=" . $key . "&amp;hs=" . $handshakeID . "&amp;location=" . SECUREURL . "/contacts.php";
                // send handshake mail to provisional user
                $recipMail = new \Docsmit\Docsmail;
                $recipMail->to = $receiver_email;
                $recipMail->subject = "Handshake Invitation";
                $recipMail->template = array(
                    'source' => 'handshake_provisional_user.html',
                    'receiver_localname' => $receiver_localname,
                    'sender_email' => $sender_email,
                    'message' => $message,
                    'accept_link' => $accept_link
                );
                $recipMail->send();
                SECLOG("Handshake resent successfully to provisional user (receiver : $receiver_email, handshake_id : $handshakeID");
            } else {
                // If user is regualr user - not provisional
                $accept_link = SECUREURL . '/accept_handshake.php?hs=' . $handshakeID;
                $recipMail = new \Docsmit\Docsmail;
                $recipMail->to = $receiver_email;
                $recipMail->subject = "Handshake Invitation";
                $recipMail->template = array(
                    'source' => 'handshakeRequest-Receiver.html',
                    'receiver_localname' => $receiver_localname,
                    'sender_email' => $sender_email,
                    'message' => $message,
                    'accept_link' => $accept_link
                );
                $recipMail->send();
                SECLOG("Handshake resent successfully to non-provisional user (receiver : $receiver_email, handshakeID : $handshakeID");

            }
            $output["handshakeID"]= $handshakeID;
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });
//accept Handshake
        $app->get('/handshakes/:handshakeID/acceptHandshake', function ($handshakeID) use ($app, $log) {
            $handshakeID = filter_var($handshakeID, FILTER_VALIDATE_INT);

            if (false === $handshakeID) {
                throw new ValidationException("Problem with one of the request parameter.Invalid HandshakeID", 403);
            }
            $userID = !empty($app->user->userID()) ? $app->user->userID() : null;
            $handshake = new \Docsmit\Handshake($app->mysqli, $handshakeID);
            if (empty($handshake->handshake_id())) {
                throw new ValidationException("HandshakeID not found", 404);
            }
            if ($handshake->sender_id() != $userID && $handshake->receiver_id() != $userID) {
                throw new ValidationException("Unauthorized access !!", 403);
            }
            $message = "";
            $accepted = $handshake->accept($userID);
            if ($accepted) {
                $message = "You accepted the handshake.";
                if ($handshake->bothAccepted()) {
                    $handshake->notify_accepted();
                    $message = "Handshake confirmed!";
                }
            }
            $output["message"] = $message;
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });
    }); // handshakes API group - End
// Group for testing API
    $app->group('/testing', function () use ($app, $log) {

//callback signedFor with messageID
        $app->post('/signedFor/:messageID', function ($messageID) use ($app, $log) {
            $body = $app->request()->getBody();
            $dateTime = isset($body['dateTime']) ? $body['dateTime'] : '';
            $event = isset($body['event']) ? $body['event'] : '';
            $email = isset($body['email']) ? $body['email'] : '';
            $clientMsgID = isset($body['clientMsgID']) ? $body['clientMsgID'] : '';
            $result = "Event : " . $event . " Message - " . $messageID . " was signed for on " . $dateTime . " by " . $email;
            DEBLOG("signedFor callbackURI Response :" . $result);
            $output["result"] = $result;
            //$output["message"] = "signedFor callbackURI called successfully.";
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });
//callback reminded with messageID
        $app->post('/reminderSent/:messageID', function ($messageID) use ($app, $log) {
            $body = $app->request()->getBody();
            $dateTime = isset($body['dateTime']) ? $body['dateTime'] : '';
            $event = isset($body['event']) ? $body['event'] : '';
            $email = isset($body['email']) ? $body['email'] : '';
            $clientMsgID = isset($body['clientMsgID']) ? $body['clientMsgID'] : '';
            $nextReminder = isset($body['nextReminder']) ? $body['nextReminder'] : '';
            $result = "Event : " . $event . " Message - " . $messageID . " reminded for on " . $dateTime . " by " . $email . " with clientMsgID:" . $clientMsgID . " Next Reminder :" . $nextReminder;
            DEBLOG("reminded callbackURI Response :" . $result);
            $output["result"] = $result;
            //$output["message"] = "reminded callbackURI called successfully.";
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });
//callback delivered with messageID
        $app->post('/delivered/:messageID', function ($messageID) use ($app, $log) {
            $body = $app->request()->getBody();
            $email = isset($body['email']) ? $body['email'] : '';
            $dateTime = isset($body['dateTime']) ? $body['dateTime'] : '';
            $event = isset($body['event']) ? $body['event'] : '';
            $clientMsgID = isset($body['clientMsgID']) ? $body['clientMsgID'] : '-';
            $result = "Event : " . $event . " Message - " . $messageID . " - " . $email . " received your email on " . $dateTime . " with ClientMSGID " . $clientMsgID;
            DEBLOG("delivered callbackURI Response : " . $result);
            $output["result"] = $result;
            //$output["message"] = "delivered callbackURI called successfully.";
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });
//callback bounced with messageID
        $app->post('/bounced/:messageID', function ($messageID) use ($app, $log) {
            $body = $app->request()->getBody();
            $email = isset($body['email']) ? $body['email'] : '';
            $dateTime = isset($body['dateTime']) ? $body['dateTime'] : '';
            $reason = isset($body['reason']) ? $body['reason'] : '';
            $event = isset($body['event']) ? $body['event'] : '';
            $clientMsgID = isset($body['clientMsgID']) ? $body['clientMsgID'] : '-';
            $result = "Event : " . $event . " Message - " . $messageID . " - " . $email . " bounced on " . $dateTime . " with below Details." .
                    " diagnosticCode : " . $reason['diagnosticCode'] . ",action: " . $reason['action'] . ",bounceType: " . $reason['bounceType'] . ",bounceSubType" . $reason['bounceSubType'];
            DEBLOG("bounced callbackURI Response : " . $result);
            $output["result"] = $result;
            //$output["message"] = "bounced callbackURI called successfully.";
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });
//callback complained with messageID
        $app->post('/complained/:messageID', function ($messageID) use ($app, $log) {
            $body = $app->request()->getBody();
            $email = isset($body['email']) ? $body['email'] : '';
            $dateTime = isset($body['dateTime']) ? $body['dateTime'] : '';
            $reason = isset($body['reason']) ? $body['reason'] : '';
            $event = isset($body['event']) ? $body['event'] : '';
            $result = "Event : " . $event . " Message - " . $messageID . " - " . $email . " complained on " . $dateTime . " with below Details." .
                    " complaintFeedbackType :" . $reason['complaintFeedbackType'];
            DEBLOG("complained callbackURI Response : " . $result);
            $output["result"] = $result;
            //$output["message"] = "complained callbackURI called successfully.";
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });
//callback flagged with messageID
        $app->post('/flagged/:messageID', function ($messageID) use ($app, $log) {
            $body = $app->request()->getBody();
            $email = isset($body['email']) ? $body['email'] : '';
            $dateTime = isset($body['dateTime']) ? $body['dateTime'] : '';
            $reason = isset($body['reason']) ? $body['reason'] : '';
            $event = isset($body['event']) ? $body['event'] : '';
            $result = "Event : " . $event . " Message - " . $messageID . " - " . $email . " flagged on " . $dateTime . " with Reason " . $reason;
            DEBLOG("flagged callbackURI Response : " . $result);
            $output["result"] = $result;
            //$output["message"] = "flagged callbackURI called successfully.";
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });
//callback unclaimed with messageID
        $app->post('/unclaimed/:messageID', function ($messageID) use ($app, $log) {
            $body = $app->request()->getBody();
            $dateTime = isset($body['dateTime']) ? $body['dateTime'] : '';
            $event = isset($body['event']) ? $body['event'] : '';
            $email = isset($body['email']) ? $body['email'] : '';
            $clientMsgID = isset($body['clientMsgID']) ? $body['clientMsgID'] : '';
            $result = "Event : " . $event . " Message - " . $messageID . " unclaimed for on " . $dateTime . " by " . $email . " with clientMsgID:" . $clientMsgID;
            DEBLOG("Unclaimed callbackURI Response :" . $result);
            $output["result"] = $result;
            //$output["message"] = "Unclaimed callbackURI called successfully.";
            echo json_encode($output, JSON_PRETTY_PRINT);
            return;
        });
    }); // testing API group - End
}
); // end api v1
// Public human readable home page
$app->get(
        '/', function () use ($app, $log) {
    echo "<h1>Hello, this can be the public App Interface</h1>";
}
);

// JSON friendly errors
// NOTE: debug must be false
// or default error template will be printed
$app->error(function (\Exception $e) use ($app, $log) {

    $mediaType = $app->request->getMediaType();

    $isAPI = (bool) preg_match('|^/api/v.*$|', $app->request->getPath());

    // Standard exception data
    $error = array(
        'code' => $e->getCode(),
        'message' => $e->getMessage(),
        'file' => $e->getFile(),
        'line' => $e->getLine(),
    );

    // Graceful error data for production mode
    if (!in_array(
                    get_class($e), array('API\\Exception', 'API\\Exception\ValidationException')
            ) && 'production' === $app->config('mode')) {
        die("prod mode");
        $error['message'] = 'There was an internal error';
        unset($error['file'], $error['line']);
    }

    // Custom error data (e.g. Validations)
    if (method_exists($e, 'getData')) {
        $errors = $e->getData();
    }

    if (!empty($errors)) {
        $error['errors'] = $errors;
    }

    $log->error($e->getMessage());
    if ('application/json' === $mediaType || true === $isAPI) {
        $app->response->headers->set(
                'Content-Type', 'application/json'
        );

        $app->status($e->getCode());
        $app->response->body(json_encode($error, JSON_PRETTY_PRINT));
        return;
    } else {
        echo '<html>
        <head><title>Error</title></head>
        <body><h1>Error: ' . $error['code'] . '</h1><p>'
        . $error['message']
        . '</p></body></html>';
    }
});

/// Custom 404 error
$app->notFound(function () use ($app) {

    $mediaType = $app->request->getMediaType();

    $isAPI = (bool) preg_match('|^/api/v.*$|', $app->request->getPath());


    if ('application/json' === $mediaType || true === $isAPI) {

        $app->response->headers->set('Content-Type', 'application/json');

        echo json_encode(
                array(
            'code' => 404,
            'message' => 'Not found'
                ), JSON_PRETTY_PRINT
        );
    } else {
        echo '<html>
        <head><title>404 Page Not Found</title></head>
        <body><h1>404 Page Not Found</h1><p>The page you are
        looking for could not be found.</p></body></html>';
    }
});

$app->run();
