<?php

/**
 * Common configuration
 */
$config['db'] = array(
    'driver' => 'mysql',
    'host' => 'localhost',
    'username' => 'robert',
    'password' => 'April2014!',
    'dbname' => 'stage_robert',
);


$config['db']['dsn'] = sprintf(
        '%s:host=%s;dbname=%s', $config['db']['driver'], $config['db']['host'], $config['db']['dbname']
);
$_ENV['SLIM_MODE'] =    "development";
//echo $config['db']['dsn'];exit;
$config['app']['mode'] = $_ENV['SLIM_MODE'];

// Cache TTL in seconds
$config['app']['cache.ttl'] = 60;

// Max requests per hour
$config['app']['rate.limit'] = 100000;

// Setup monolog
$config['app']['log.writer'] = new \Flynsarmy\SlimMonolog\Log\MonologWriter(array(
    'handlers' => array(
        new \Monolog\Handler\StreamHandler(
                realpath(__DIR__ . '/../logs')
                . '/' . $_ENV['SLIM_MODE'] . '_' . date('Y-m-d') . '.log'
        ),
    ),
        ));
$config['app']['public.routes']["/v1/token"]= "POST";
$config['app']['public.routes']["/v1/messages/home"]= "GET";
$config['app']['public.routes']["/v1/"]= "GET";
$config['app']['public.routes']["/"]= "GET";
