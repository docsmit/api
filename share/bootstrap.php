<?php
require_once dirname(__FILE__) . '/../vendor/autoload.php';

// docsmit dependencies - start
$docsmit_path = dirname(__FILE__) ."/../../";
//echo "$docsmit_path";
include $docsmit_path . 'config.php';
include $docsmit_path . 'security_functions.php';
include_once $docsmit_path . 'utils/util.php';
include_once $docsmit_path . 'Common/vendor/autoload.php';
// docsmit dependencies - end

use Slim\Slim;
use API\Application;
use API\Middleware\TokenOverBasicAuth;
use Flynsarmy\SlimMonolog\Log\MonologWriter;

// Init application mode
if (empty($_ENV['SLIM_MODE'])) {
    $_ENV['SLIM_MODE'] = (getenv('SLIM_MODE')) ? getenv('SLIM_MODE') : 'development';
}

// Init and load configuration
$config = array();

$configFile = dirname(__FILE__) . '/config/' . $_ENV['SLIM_MODE'] . '.php';

if (is_readable($configFile)) {
    require_once $configFile;
} else {
    require_once dirname(__FILE__) . '/config/default.php';
}


// Create Application
$app = new Application($config['app']);

// Define DB resource
 $app->container->singleton('mysqli', function () {
     return new mysqli(MYSQLHOST, MYSQLUSER, MYSQLPASSWORD, DATABASE);
 });
 
// Only invoked if mode is "production"
$app->configureMode('production', function () use ($app) {
    $app->config(array(
        'log.enable' => true,
        'log.level' => \Slim\Log::WARN,
        'debug' => false
    ));
});

// Only invoked if mode is "development"
$app->configureMode('development', function () use ($app) {
    $app->config(array(
        'log.enable' => true,
        'log.level' => \Slim\Log::DEBUG,
        'debug' => false
    ));
});

// Get log writer
$log = $app->getLog();

// Cache Middleware (inner)
$app->add(new API\Middleware\Cache('/v1'));

// Parses JSON body
$app->add(new \Slim\Middleware\ContentTypes());

// Manage Rate Limit
$app->add(new API\Middleware\RateLimit('/v1'));

// JSON Middleware
$app->add(new API\Middleware\JSON('/v1'));

// Auth Middleware (outer)
$app->add(new API\Middleware\TokenOverBasicAuth(array('root' => '/v1')));
